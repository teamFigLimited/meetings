$(function() {

    //  generic aria tab control
    $("li[role='tab']").click(function(){  
            $("li[role='tab']").attr("aria-selected","false"); //deselect all the tabs
            $("li[role='tab']").removeClass("active"); //deselect all the tabs, bootstrap
            $(this).attr("aria-selected","true");  // select this tab
            $(this).addClass("active");  // select this tab, bootstrap
            var tabpanid= $(this).attr("aria-controls"); //find out what tab panel this tab controls  
            var tabpan = $("#"+tabpanid);  
            $("div[role='tabpanel']").attr("aria-hidden","true"); //hide all the panels 
            tabpan.attr("aria-hidden","false");  // show our panel
    });

    //  family head option
    $('#is-family-head').on('change', function() {
        if($(this).val() === '1') {
            $('#family-head-id').val(0);
            $('#family-head-id').attr('disabled', true);
        } else {
            $('#family-head-id').attr('disabled', false);
        }
    });
    //  check on page load
    if($('#is-family-head').val() === '1') {
            $('#family-head-id').val(0);
            $('#family-head-id').attr('disabled', true);
    } else {
            $('#family-head-id').attr('disabled', false);
    }


    //  view an individual
    $('.view-individual').on('click', function() {
            var id = $(this).data('id');
            console.log(id);
            $('#view-individual').load('/individuals/get/'+id);
    });


    //  speaker select from cong
    $('.congregation-id-trigger-true').on('change', function() {
        $this = $(this);
        $.get(
            "/individuals/a_speakers/", { 
                'congregation_id' : $this.val() 
            },
            function(data) {
                var sel = $("#speaker-id");
                sel.empty();
                sel.append('<option value="">-- no selection --</option>');
                $.each(data, function(k,v) {
                    sel.append('<option value="' + k + '">' + v + '</option>');
                });
            }, 
            "json"
        );        
    });
    
    //  speaker outline from speaker
    $('.speaker-id-trigger-true').on('change', function() {
        $this = $(this);
        $.get(
            "/outlines/a_by_speaker/", { 
                'individual_id' : $this.val() 
            },
            function(data) {
                //console.log(data);
                var sel = $("#outline-id");
                sel.empty();
                sel.append('<option value="">-- no selection --</option>');
                $.each(data, function(k,v) {
                    sel.append('<option value="' + k + '">' + v + '</option>');
                });
            }, 
            "json"
        );        
    });
    
    
    //  on change of individual for history
    $('.individual-item-history').change(function(e) {
        var $this = $(this);
        if($this.val() !== '') {
            $('tbody').load("/meeting-midweeks/a-by-individual", {individualId: $this.val()});
        }
    });

});