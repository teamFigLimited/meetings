<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MeetingEvents Controller
 *
 * @property \App\Model\Table\MeetingEventsTable $MeetingEvents
 */
class MeetingEventsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('meetingEvents', $this->paginate($this->MeetingEvents));
        $this->set('_serialize', ['meetingEvents']);
    }

    /**
     * View method
     *
     * @param string|null $id Meeting Event id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $meetingEvent = $this->MeetingEvents->get($id, [
            'contain' => []
        ]);
        $this->set('meetingEvent', $meetingEvent);
        $this->set('_serialize', ['meetingEvent']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $meetingEvent = $this->MeetingEvents->newEntity();
        if ($this->request->is('post')) {
            $meetingEvent = $this->MeetingEvents->patchEntity($meetingEvent, $this->request->data);
            if ($this->MeetingEvents->save($meetingEvent)) {
                $this->Flash->success(__('The meeting event has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The meeting event could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('meetingEvent'));
        $this->set('_serialize', ['meetingEvent']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Meeting Event id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $meetingEvent = $this->MeetingEvents->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $meetingEvent = $this->MeetingEvents->patchEntity($meetingEvent, $this->request->data);
            if ($this->MeetingEvents->save($meetingEvent)) {
                $this->Flash->success(__('The meeting event has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The meeting event could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('meetingEvent'));
        $this->set('_serialize', ['meetingEvent']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Meeting Event id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $meetingEvent = $this->MeetingEvents->get($id);
        if ($this->MeetingEvents->delete($meetingEvent)) {
            $this->Flash->success(__('The meeting event has been deleted.'));
        } else {
            $this->Flash->error(__('The meeting event could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
