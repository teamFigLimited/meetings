<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MeetingWeekTypes Controller
 *
 * @property \App\Model\Table\MeetingWeekTypesTable $MeetingWeekTypes
 */
class MeetingWeekTypesController extends AppController
{

	public function isAuthorized($user)
	{
        if($user['is_admin'] == 1) {
            return true;
        }
        
		return parent::isAuthorized($user);
	}
	
        
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('meetingWeekTypes', $this->paginate($this->MeetingWeekTypes));
        $this->set('_serialize', ['meetingWeekTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Meeting Week Type id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $meetingWeekType = $this->MeetingWeekTypes->get($id, [
            'contain' => []
        ]);
        $this->set('meetingWeekType', $meetingWeekType);
        $this->set('_serialize', ['meetingWeekType']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $meetingWeekType = $this->MeetingWeekTypes->newEntity();
        if ($this->request->is('post')) {
            $meetingWeekType = $this->MeetingWeekTypes->patchEntity($meetingWeekType, $this->request->data);
            if ($this->MeetingWeekTypes->save($meetingWeekType)) {
                $this->Flash->success(__('The meeting week type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The meeting week type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('meetingWeekType'));
        $this->set('_serialize', ['meetingWeekType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Meeting Week Type id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $meetingWeekType = $this->MeetingWeekTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $meetingWeekType = $this->MeetingWeekTypes->patchEntity($meetingWeekType, $this->request->data);
            if ($this->MeetingWeekTypes->save($meetingWeekType)) {
                $this->Flash->success(__('The meeting week type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The meeting week type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('meetingWeekType'));
        $this->set('_serialize', ['meetingWeekType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Meeting Week Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $meetingWeekType = $this->MeetingWeekTypes->get($id);
        if ($this->MeetingWeekTypes->delete($meetingWeekType)) {
            $this->Flash->success(__('The meeting week type has been deleted.'));
        } else {
            $this->Flash->error(__('The meeting week type could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
