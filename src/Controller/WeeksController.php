<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Weeks Controller
 *
 * @property \App\Model\Table\WeeksTable $Weeks
 */
class WeeksController extends AppController
{

    public function isAuthorized($user)
    {
	//  only admins have full access to Weeks
	if($user['is_admin'] == 1) {
	    return true;
	}
	return parent::isAuthorized($user);
    }
	
    
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        
        //  check if a Month has been selected
        if(isset($this->request->query['month_id'])) {
            //  get selected month
            $monthId = $this->request->query['month_id'];
        } else {
            //  get current month
            $currentMonth = $this->Weeks->Month->find()->where(['date_month <= NOW()'])->order(['date_month' => 'DESC'])->first();
            //debug(json_encode($currentMonth, JSON_PRETTY_PRINT));
            if($currentMonth) {
                $monthId = $currentMonth->id;
            } else {
                $monthId = 1;
            }
        }
        
        $this->paginate = [
            'conditions' => ['Weeks.month_id' => $monthId],
            'contain' => ['Month', 'WeekMidweek.MinistryItem1Type', 'WeekMidweek.MinistryItem2Type', 'WeekMidweek.MinistryItem3Type', 'WeekWeekend']
        ];
        $this->set('weeks', $this->paginate($this->Weeks));
        $this->set('_serialize', ['weeks']);
        
        $months = $this->Weeks->Month->find('all');
        
        $this->set(compact('monthId', 'months'));
    }

    
    /**
     * View method
     *
     * @param string|null $id Week id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $week = $this->Weeks->get($id, [
            //'order' => ['WeekMinistryAssignments.sort' => 'ASC'],
            'contain' => ['Month', 'Meetings', 'WeekMidweek.MinistryItem1Type', 'WeekMidweek.MinistryItem2Type', 'WeekMidweek.MinistryItem3Type', 'WeekWeekend']
        ]);
        $this->set('week', $week);
        $this->set('_serialize', ['week']);
    }

    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $week = $this->Weeks->newEntity();
        if ($this->request->is(['put', 'post'])) {
            $week = $this->Weeks->patchEntity($week, $this->request->data);
            if ($this->Weeks->save($week)) {
                $this->Flash->success(__('The week has been saved.'));
                return $this->redirect(['controller' => 'weeks', 'action' => 'index', (isset($this->request->query['month_id'])) ? $this->request->query['month_id'] : null ]);
            } else {
                $this->Flash->error(__('The week could not be saved. Please, try again.'));
            }
        }
        $weeks = $this->Weeks->find();
        $months = $this->Weeks->Month->find('list');
        $this->set(compact('week', 'weeks', 'months'));
        $this->set('_serialize', ['week']);
    }

    
    /**
     * Edit method
     *
     * @param string|null $id Week id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $week = $this->Weeks->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $week = $this->Weeks->patchEntity($week, $this->request->data);
            if ($this->Weeks->save($week)) {
                $this->Flash->success(__('The week has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The week could not be saved. Please, try again.'));
            }
        }
        $months = $this->Weeks->Months->find('list', ['limit' => 200]);
        $this->set(compact('week', 'months'));
        $this->set('_serialize', ['week']);
    }

    
    /**
     * Delete method
     *
     * @param string|null $id Week id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $week = $this->Weeks->get($id);
        if ($this->Weeks->delete($week)) {
            $this->Flash->success(__('The week has been deleted.'));
        } else {
            $this->Flash->error(__('The week could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    
    public function activate($id = null)
    {
        if($this->Weeks->updateAll(['is_live' => 1], ['id' => $id])) {
            $this->Flash->success(__('The Week Header has been activated.'));
        } else {
            $this->Flash->success(__('The Week Header could not be activated yet.'));
        }
        
        return $this->redirect($this->referer());
    }


    public function deactivate($id = null)
    {
        if($this->Weeks->updateAll(['is_live' => 0], ['id' => $id])) {
            $this->Flash->success(__('The Week Header has been de-activated.'));
        } else {
            $this->Flash->success(__('The Week Header could not be de-activated.'));
        }
        
        return $this->redirect($this->referer());
    }
    
    
}
