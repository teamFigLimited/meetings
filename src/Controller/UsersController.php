<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    
	public function isAuthorized($user)
	{
		//  only user can edit herself
		if ($this->request->action === 'edit' && $user['id'] == (int)$this->request->params['pass'][0]) {
			return true;
		}

		return parent::isAuthorized($user);
	}
	
	
    public function beforeFilter(Event $event)
    {
		parent::beforeFilter($event);
		// Allow users to register and logout.
		// You should not add the "login" action to allow list. Doing so would
		// cause problems with normal functioning of AuthComponent.
		$this->Auth->allow(['add', 'logout']);
    }
	
	
 
	public function login()
	{
		if ($this->request->is('post')) {
			$user = $this->Auth->identify();
			if ($user) {
				$this->Auth->setUser($user);
				
				//  update last login time
				$u = $this->Users->find()->where(['Users.id' => $user['id']])->first();
				$u->last_login = date('Y-m-d H:i:s');
				$u->client_ip = $this->request->clientIp();
				$this->Users->save($u);
				
				return $this->redirect($this->Auth->redirectUrl());
			}
			$this->Flash->error(__('Invalid username or password, try again'));
		}
	}

	
	public function logout()
	{
        $this->request->session()->destroy();
		return $this->redirect($this->Auth->logout());
	}
	
	    
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('users', $this->paginate($this->Users));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Installations']
        ]);
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }
    
    
    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data, ['fieldList' => ['username', 'password', 'mobile', 'email']]);
						$user->client_ip = $this->request->clientIp();
            if ($this->Users->save($user)) {
                
                $this->Flash->success(__('You have been successfully registered.'));
                $this->Auth->setUser($user->toArray());
                return $this->redirect([
                    'controller' => 'Installations',
                    'action' => 'add'
                ]);
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
        
    }

    
    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data, ['fieldList' => ['username', 'password', 'mobile', 'email']]);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['controller' => 'installations', 'action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
        
    }

}
