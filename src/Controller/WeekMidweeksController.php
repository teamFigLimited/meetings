<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use JoshRibakoff_Note;

/**
 * WeekMidweeks Controller
 *
 * @property \App\Model\Table\WeekMidweeksTable $WeekMidweeks
 */
class WeekMidweeksController extends AppController
{

    public function isAuthorized($user)
    {
	if($user['is_admin'] == 1) {
	    return true;
	}
	return parent::isAuthorized($user);
    }


    public function import()
    {
	$weekId = $this->request->query('week_id');

        $weekMidweek = $this->WeekMidweeks->newEntity();

	if($this->request->is(['put', 'post'])) {
	    $note = new JoshRibakoff_Note;
	    $file = file_get_contents($this->request->data['import']['tmp_name']);
	    echo $file;
	    $note->setRTF($file);
	    $note->formatHTML(); // returns your RTF converted to HTML as best as possible.
	}

	$this->set(compact('weekId', 'weekMidweek'));
    }


    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Weeks']
        ];
        $this->set('weekMidweeks', $this->paginate($this->WeekMidweeks));
        $this->set('_serialize', ['weekMidweeks']);
    }

    /**
     * View method
     *
     * @param string|null $id Week Midweek id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $weekMidweek = $this->WeekMidweeks->get($id, [
            'contain' => ['Weeks']
        ]);
        $this->set('weekMidweek', $weekMidweek);
        $this->set('_serialize', ['weekMidweek']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if(isset($this->request->query['week_id'])) {
            $weekId = $this->request->query['week_id'];
        } else {
            $this->Flash->error(__('There needs to be a week number'));
            return $this->redirect($this->referer());
        }

        $weekMidweek = $this->WeekMidweeks->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['week_id'] = $weekId;
            $weekMidweek = $this->WeekMidweeks->patchEntity($weekMidweek, $this->request->data);

            //  TODO: Need to check whether this week / part combo can be added

            if ($this->WeekMidweeks->save($weekMidweek)) {
                $this->Flash->success(__('The midweek has been saved.'));
                return $this->redirect(['controller' => 'weeks', 'action' => 'view', $weekId]);
            } else {
                $this->Flash->error(__('The midweek could not be saved. Please, try again.'));
            }
        }
        $week = $this->WeekMidweeks->Week->get($weekId, ['contain' => ['Month']]);
        $this->set(compact('weekMidweek', 'weekId', 'week'));
        $this->set('_serialize', ['weekMidweek']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Week Midweek id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $weekMidweek = $this->WeekMidweeks->get($id, [
            'contain' => ['Week.Month']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $weekMidweek = $this->WeekMidweeks->patchEntity($weekMidweek, $this->request->data);
            if ($this->WeekMidweeks->save($weekMidweek)) {
                $this->Flash->success(__('The week midweek has been saved.'));
                return $this->redirect(['controller' => 'weeks', 'action' => 'view', $weekMidweek->week_id]);
            } else {
                $this->Flash->error(__('The week midweek could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('weekMidweek'));
        $this->set('_serialize', ['weekMidweek']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Week Midweek id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $weekMidweek = $this->WeekMidweeks->get($id);
        if ($this->WeekMidweeks->delete($weekMidweek)) {
            $this->Flash->success(__('The week midweek has been deleted.'));
        } else {
            $this->Flash->error(__('The week midweek could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }


    public function editTreasures($id = null)
    {
        $weekMidweek = $this->WeekMidweeks->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $weekMidweek = $this->WeekMidweeks->patchEntity($weekMidweek, $this->request->data);
            if ($this->WeekMidweeks->save($weekMidweek)) {
                $this->Flash->success(__('The week midweek has been saved.'));
                return $this->redirect(['controller' => 'weeks', 'action' => 'view', $weekMidweek->week_id]);
            } else {
                $this->Flash->error(__('The week midweek could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('weekMidweek'));
        $this->set('_serialize', ['weekMidweek']);
    }


    public function editMinistry($id = null)
    {
        $weekMidweek = $this->WeekMidweeks->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            //print_r($this->request->data);
            $weekMidweek = $this->WeekMidweeks->patchEntity($weekMidweek, $this->request->data);
            //debug(json_encode($weekMidweek, JSON_PRETTY_PRINT));
            //die();
            if ($this->WeekMidweeks->save($weekMidweek)) {
                $this->Flash->success(__('The week midweek has been saved.'));
                return $this->redirect(['controller' => 'weeks', 'action' => 'view', $weekMidweek->week_id]);
            } else {
                $this->Flash->error(__('The week midweek could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('weekMidweek'));
        $this->set('_serialize', ['weekMidweek']);
    }

    public function editLiving($id = null)
    {
        $weekMidweek = $this->WeekMidweeks->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            //print_r($this->request->data);
            $weekMidweek = $this->WeekMidweeks->patchEntity($weekMidweek, $this->request->data);
            //debug(json_encode($weekMidweek, JSON_PRETTY_PRINT));
            //die();
            if ($this->WeekMidweeks->save($weekMidweek)) {
                $this->Flash->success(__('The week midweek has been saved.'));
                return $this->redirect(['controller' => 'weeks', 'action' => 'view', $weekMidweek->week_id]);
            } else {
                $this->Flash->error(__('The week midweek could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('weekMidweek'));
        $this->set('_serialize', ['weekMidweek']);
    }


    public function activate($id = null)
    {
        if($this->WeekMidweeks->updateAll(['is_live' => 1], ['id' => $id])) {
            $this->Flash->success(__('The Midweek Section has been activated.'));
        } else {
            $this->Flash->success(__('The Midweek Section could not be activated yet.'));
        }

        return $this->redirect($this->referer());
    }


    public function deactivate($id = null)
    {
        if($this->WeekMidweeks->updateAll(['is_live' => 0], ['id' => $id])) {
            $this->Flash->success(__('The Midweek Section has been de-activated.'));
        } else {
            $this->Flash->success(__('The Midweek Section could not be de-activated.'));
        }

        return $this->redirect($this->referer());
    }

}
