<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Meetings Controller
 *
 * @property \App\Model\Table\MeetingsTable $Meetings
 */
class MeetingsController extends AppController
{

	public function isAuthorized($user)
	{
        
		if($this->request->action === 'index') {
			return true;
		}
		
		if($this->request->action === 'view') {
			return true;
		}
		
		if($this->request->action === 'edit') {
            $meeting = $this->Meetings->get($this->request->params['pass'][0]);
            if($meeting->installation_id === $this->request->session()->read('installation')->id) {
                return true;
            }
			return false;
		}
		
		if($this->request->action === 'update') {
			return true;
		}
		
		return parent::isAuthorized($user);
	}
    
        
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        //  check if a Month has been selected
        if(isset($this->request->query['month_id'])) {
            //  get selected month  
            $monthId = $this->request->query['month_id'];
        } else {
            //  get current month
            $currentMonth = $this->Meetings->Week->Month->find()->where(['date_month <= NOW()'])->order(['date_month' => 'DESC'])->first();
            //debug(json_encode($currentMonth, JSON_PRETTY_PRINT));
            
            if($currentMonth) {
                $monthId = $currentMonth->id;
            } else {
                $monthId = 1;
            }
        }
        $this->set('monthId', $monthId);
        
        $this->paginate = [
            'conditions' => ['Meetings.installation_id' => $this->request->session()->read('installation')->id, 'Week.month_id' => $monthId],
            'contain' => ['Installation', 'Week.WeekMidweek', 'Week.WeekWeekend', 'Week.Month', 'MeetingMidweek.Chairman', 'MeetingWeekend', 'MeetingWeekType', 'MeetingMidweek.OpeningPrayer', 'MeetingMidweek.TreasuresTalkIndividual', 'MeetingMidweek.TreasuresGemsIndividual', 'MeetingMidweek.TreasuresReadingIndividual', 'MeetingMidweek.MinistryPresentationIndividual', 'MeetingMidweek.MinistryItem1Type', 'MeetingMidweek.MinistryItem1Student', 'MeetingMidweek.MinistryItem1Assistant', 'MeetingMidweek.MinistryItem2Type', 'MeetingMidweek.MinistryItem2Student', 'MeetingMidweek.MinistryItem2Assistant', 'MeetingMidweek.MinistryItem3Type', 'MeetingMidweek.MinistryItem3Student', 'MeetingMidweek.MinistryItem3Assistant', 'MeetingMidweek.LivingItem1Individual', 'MeetingMidweek.LivingItem2Individual', 'MeetingMidweek.LivingCbsConductor', 'MeetingMidweek.LivingCbsReader', 'MeetingMidweek.ClosingPrayer', 'MeetingWeekend.Chairman', 'MeetingWeekend.Speaker', 'MeetingWeekend.SpeakerCongregation', 'MeetingWeekend.Reader', 'MeetingWeekend.Outline', 'MeetingOutgoingTalks.Speaker', 'MeetingOutgoingTalks.Outline', 'MeetingOutgoingTalks.Congregation']
        ];
        $this->set('meetings', $this->paginate($this->Meetings));
        $this->set('_serialize', ['meetings']);
        
        $months = $this->Meetings->Week->Month->find('all');
        $this->set(compact('months'));
    }

    /**
     * View method
     *
     * @param string|null $id Meeting id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $meeting = $this->Meetings->get($id, [
            'contain' => ['Installation', 'Week.Month', 'Week.WeekMidweek', 'Week.WeekWeekend', 'MeetingMidweek.Chairman', 'MeetingWeekend', 'MeetingWeekType', 'MeetingMidweek.OpeningPrayer', 'MeetingMidweek.TreasuresTalkIndividual', 'MeetingMidweek.TreasuresGemsIndividual', 'MeetingMidweek.TreasuresReadingIndividual', 'MeetingMidweek.MinistryPresentationIndividual', 'MeetingMidweek.MinistryItem1Type', 'MeetingMidweek.MinistryItem1Student', 'MeetingMidweek.MinistryItem1Assistant', 'MeetingMidweek.MinistryItem2Type', 'MeetingMidweek.MinistryItem2Student', 'MeetingMidweek.MinistryItem2Assistant', 'MeetingMidweek.MinistryItem3Type', 'MeetingMidweek.MinistryItem3Student', 'MeetingMidweek.MinistryItem3Assistant', 'MeetingMidweek.LivingItem1Individual', 'MeetingMidweek.LivingItem2Individual', 'MeetingMidweek.LivingCbsConductor', 'MeetingMidweek.LivingCbsReader', 'MeetingMidweek.ClosingPrayer', 'MeetingWeekend.Chairman', 'MeetingWeekend.Speaker', 'MeetingWeekend.SpeakerCongregation', 'MeetingWeekend.Reader', 'MeetingWeekend.Outline', 'MeetingOutgoingTalks.Speaker', 'MeetingOutgoingTalks.Outline', 'MeetingOutgoingTalks.Congregation']
        ]);
        $this->set('meeting', $meeting);
        $this->set('_serialize', ['meeting']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $meeting = $this->Meetings->newEntity();
        if ($this->request->is('post')) {
            $meeting = $this->Meetings->patchEntity($meeting, $this->request->data);
            if ($this->Meetings->save($meeting)) {
                $this->Flash->success(__('The meeting has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
            }
        }
        $installations = $this->Meetings->Installations->find('list', ['limit' => 200]);
        $weeks = $this->Meetings->Weeks->find('list', ['limit' => 200]);
        $this->set(compact('meeting', 'installations', 'weeks'));
        $this->set('_serialize', ['meeting']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Meeting id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $meeting = $this->Meetings->get($id, [
            'contain' => ['Week']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $meeting = $this->Meetings->patchEntity($meeting, $this->request->data);
            if ($this->Meetings->save($meeting)) {
                $this->Flash->success(__('The meeting has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
            }
        }
        $meetingWeekTypes = $this->Meetings->MeetingWeekType->find('list', ['limit' => 200]);
        
        $this->set(compact('meeting', 'meetingWeekTypes'));
        $this->set('_serialize', ['meeting']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Meeting id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $meeting = $this->Meetings->get($id);
        if ($this->Meetings->delete($meeting)) {
            $this->Flash->success(__('The meeting has been deleted.'));
        } else {
            $this->Flash->error(__('The meeting could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    
    public function update() 
    {
        
        $installationId = $this->request->session()->read('installation')->id;

        // first check weeks -> meetings
        $weeks = $this->Meetings->Week->find()
            ->where(['Week.is_live' => 1])
            ->notMatching('Meetings', function($q) use($installationId) {
                return $q->where(
                    ['Meetings.installation_id' => $installationId]
                );
            });
                
        //  insert Weeks into Meetings
        $this->Meetings->updateMeetings($weeks, $this->request->session()->read('installation')->id);
        
        $meetings = $this->Meetings->find()
            ->where(['Meetings.installation_id' => $this->request->session()->read('installation')->id])
            ->contain(['MeetingMidweek'])
            ->where(['MeetingMidweek.id IS NULL'])
            ->contain(['Week.WeekMidweek'])
            ;
        $this->Meetings->updateMeetingMidweeksByMeeting($meetings);
        
        $meetings = $this->Meetings->find()
            ->where(['Meetings.installation_id' => $this->request->session()->read('installation')->id])
            ->contain(['MeetingWeekend'])
            ->where(['MeetingWeekend.id IS NULL'])
            ->contain(['Week.WeekWeekend'])
            ;
        $this->Meetings->updateMeetingWeekendsByMeeting($meetings);
        
        return $this->redirect($this->referer());

    }
    
}
