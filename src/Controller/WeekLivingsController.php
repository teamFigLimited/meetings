<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * WeekLivings Controller
 *
 * @property \App\Model\Table\WeekLivingsTable $WeekLivings
 */
class WeekLivingsController extends AppController
{

	public function isAuthorized($user)
	{
        if($user['is_admin'] == 1) {
            return true;
        }
        
		return parent::isAuthorized($user);
	}
	
        
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        
        if($this->request->session()->check('installation')) {
            $this->set('currentInstallation', $this->request->session()->read('installation'));        
        }
    }
    
    
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Weeks']
        ];
        $this->set('weekLivings', $this->paginate($this->WeekLivings));
        $this->set('_serialize', ['weekLivings']);
    }

    /**
     * View method
     *
     * @param string|null $id Week Living id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $weekLiving = $this->WeekLivings->get($id, [
            'contain' => ['Weeks']
        ]);
        $this->set('weekLiving', $weekLiving);
        $this->set('_serialize', ['weekLiving']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if(isset($this->request->query['week_id'])) {
            $weekId = $this->request->query['week_id'];
        } else {
            $this->Flash->error(__('There needs to be a week number'));
            return $this->redirect($this->referer());
        }
        
        $weekLiving = $this->WeekLivings->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['week_id'] = $weekId;
            $weekLiving = $this->WeekLivings->patchEntity($weekLiving, $this->request->data);
            if ($this->WeekLivings->save($weekLiving)) {
                $this->Flash->success(__('The week living has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The week living could not be saved. Please, try again.'));
            }
        }
        $weeks = $this->WeekLivings->Weeks->find('list', ['limit' => 200]);
        $this->set(compact('weekLiving', 'weeks'));
        $this->set('_serialize', ['weekLiving']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Week Living id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $weekLiving = $this->WeekLivings->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $weekLiving = $this->WeekLivings->patchEntity($weekLiving, $this->request->data);
            if ($this->WeekLivings->save($weekLiving)) {
                $this->Flash->success(__('The week living has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The week living could not be saved. Please, try again.'));
            }
        }
        $weeks = $this->WeekLivings->Weeks->find('list', ['limit' => 200]);
        $this->set(compact('weekLiving', 'weeks'));
        $this->set('_serialize', ['weekLiving']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Week Living id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $weekLiving = $this->WeekLivings->get($id);
        if ($this->WeekLivings->delete($weekLiving)) {
            $this->Flash->success(__('The week living has been deleted.'));
        } else {
            $this->Flash->error(__('The week living could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
