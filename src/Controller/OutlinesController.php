<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Outlines Controller
 *
 * @property \App\Model\Table\OutlinesTable $Outlines
 */
class OutlinesController extends AppController
{

	public function isAuthorized($user)
	{
        
        if($user['is_admin'] == 1) {
            return true;
        }
        
        
        if($this->request->action === 'aBySpeaker') {
            return true;
        }
        
        
		return parent::isAuthorized($user);
        
	}
	
            
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('outlines', $this->paginate($this->Outlines));
        $this->set('_serialize', ['outlines']);
    }

    /**
     * View method
     *
     * @param string|null $id Outline id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $outline = $this->Outlines->get($id, [
            'contain' => []
        ]);
        $this->set('outline', $outline);
        $this->set('_serialize', ['outline']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $outline = $this->Outlines->newEntity();
        if ($this->request->is('post')) {
            $outline = $this->Outlines->patchEntity($outline, $this->request->data);
            if ($this->Outlines->save($outline)) {
                $this->Flash->success(__('The outline has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The outline could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('outline'));
        $this->set('_serialize', ['outline']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Outline id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $outline = $this->Outlines->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $outline = $this->Outlines->patchEntity($outline, $this->request->data);
            if ($this->Outlines->save($outline)) {
                $this->Flash->success(__('The outline has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The outline could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('outline'));
        $this->set('_serialize', ['outline']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Outline id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $outline = $this->Outlines->get($id);
        if ($this->Outlines->delete($outline)) {
            $this->Flash->success(__('The outline has been deleted.'));
        } else {
            $this->Flash->error(__('The outline could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }    
    
    
    public function aBySpeaker()
    {
        
        $this->viewBuilder()->layout('ajax');
        
        $individualId = $this->request->query['individual_id'];
        
        $outlines = $this->Outlines->find('list')
            ->matching('Individuals', function($q) use($individualId) { 
                return $q->where(['Individuals.id' => $individualId]);
            })
            ;
            
        echo json_encode($outlines);
        die();

    }
    
}
