<?php
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\ORM\Behavior\TimestampBehavior;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public $helpers = ['Html', 'Form', 'Number'];

	public $currentUser;


    public $maxSongNumber = 155;

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
	        'authorize' => ['Controller'],
            'loginRedirect' => [
                'controller' => 'Installations',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Pages',
                'action' => 'display',
                'home'
            ],
            'flash' => [
                'params' => [
                    'class' => 'alert alert-danger alert-top'
                ]
            ]
        ]);
    }


	public function isAuthorized($user)
	{
		// Default deny
		return false;
	}


    public function beforeFilter(Event $event)
    {


        $this->Auth->allow(['display']);

        if(Configure::read('maintenance') && $this->request->params['controller'] != 'Pages') {
            return $this->redirect(['controller' => 'pages', 'action' => 'display', 'maintenance']);
        }

		$this->currentUser = $this->Auth->user();
		$this->set('currentUser', $this->currentUser);

        /*
         * load installation as if it extends the Users model
         */
        $this->loadModel('Installations');
        $installation = $this->Installations->find('all', ['conditions' => ['user_id' => $this->currentUser['id']], 'contain' => ['Congregation']])->first();
		if($installation) {
			//  set the installation id in Session
			$this->request->session()->write('installation', $installation);
            $this->set('currentInstallation', $installation);
//		} else {
//            echo $this->request->session()->read('installation')->name."y";
        }

        $this->set('www', Router::url('/'));

        $this->set('debug', Configure::read('debug'));

        $this->set('maxSongNumber', $this->maxSongNumber);

        //  file created by cron
//        $gitDescribe = file_get_contents(WWW_ROOT."gitDescribe.txt");
        $this->set('gitDescribe', '');

    }


    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }
}
