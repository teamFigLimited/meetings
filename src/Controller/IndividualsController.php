<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;

/**
 * Individuals Controller
 *
 * @property \App\Model\Table\IndividualsTable $Individuals
 */
class IndividualsController extends AppController {

    
    public function isAuthorized($user) {

        if ($this->request->action === 'index') {
            return true;
        }

        if ($this->request->action === 'view') {
            return true;
        }

        if ($this->request->action === 'add') {
            return true;
        }

        if ($this->request->action === 'edit') {
            $individual = $this->Individuals->find('all', ['conditions' => ['Congregation.installation_id' => $this->request->session()->read('installation')->id, 'Individuals.id' => (int) $this->request->params['pass'][0]], 'contain' => ['Congregation']])->count();

            if ($individual == 1)
                return true;

            return false;
        }

        if ($this->request->action === 'delete') {
            $individual = $this->Individuals->find('all', ['conditions' => ['Congregation.installation_id' => $this->request->session()->read('installation')->id, 'Individuals.id' => (int) $this->request->params['pass'][0]], 'contain' => ['Congregation']])->count();
            if ($individual == 1)
                return true;

            return false;
        }
        
        if($this->request->action === 'editStudy') {
            return true;
        }
        
        if ($this->request->action === 'aSpeakers') {
            return true;
        }

        if ($this->request->action === 'aOutlinesBySpeaker') {
            return true;
        }
        
        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index() {

        if (isset($this->request->query['congregation_id'])) {
            $congregationId = $this->request->query['congregation_id'];
        } else {
            $congregationId = $this->request->session()->read('installation')->congregation_id;
        }
        $this->set('congregationId', $congregationId);


        $congregations = $this->Individuals->Congregation->find()
            ->where(['installation_id' => $this->request->session()->read('installation')->id]);
        $this->set('congregations', $congregations);
        
        /* non-paginated */
        $individuals = $this->Individuals->find()
            ->order(['Individuals.congregation_id = '.$this->request->session()->read('installation')->congregation_id => 'DESC', 'Individuals.last_name' => 'ASC', 'Individuals.first_name' => 'ASC'])
            ->contain(['Congregation'])
            ->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->contain(['Gender', 'Appointment', 'FamilyHead'])
        ;
        if($congregationId > 0) {
            $individuals->where(['Congregation.id' => $congregationId]);
        }
        
        $this->set(compact('individuals'));
        
        $this->set('_serialize', ['individuals']);
    }

    /**
     * View method
     *
     * @param string|null $id Individual id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        
        
        $individual = $this->Individuals->get($id, [
            'contain' => ['Congregation', 'Gender', 'Appointment', 'FamilyHead', 'Privileges']
        ]);
        $this->set('individual', $individual);
        $this->set('_serialize', ['individual']);
        
        if($this->request->_ext === 'pdf') {
            
            //  friendsOfCake
            Configure::write('CakePdf', [
                //'className' => 'CakePdf.WkHtmlToPdf',
                'engine' => 'CakePdf.WkHtmlToPdf',
                //'binary' => '/usr/local/bin/wkhtmltopdf',
                'margin' => [
                    'bottom' => 10,
                    'left' => 10,
                    'right' => 10,
                    'top' => 10
                ],
                'orientation' => 'portrait',
                'download' => false
            ]);
            
            $this->render('view', 'wkhtml');
            
        }
        
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        //  TODO:check if cong set and if belongs to user

        $individual = $this->Individuals->newEntity();
        if ($this->request->is('post')) {

            if ($this->request->data['is_family_head'] == 1) {
                $this->request->data['family_head_id'] = 0;
            }
            //$this->request->data['installation_id'] = $this->request->session()->read('installation')->id;

            $individual = $this->Individuals->patchEntity($individual, $this->request->data);

            if ($this->Individuals->save($individual)) {
                $this->Flash->success(__('The individual has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The individual could not be saved. Please, try again.'));
            }
        }

        $congregations = $this->Individuals->Congregation->find('list', ['conditions' => ['installation_id' => $this->request->session()->read('installation')->id], 'limit' => 200]);
        $genders = $this->Individuals->Gender->find('list', ['limit' => 200]);
        $appointments = $this->Individuals->Appointment->find('list', ['limit' => 200]);
        $familyHeads = $this->Individuals->FamilyHead->find('list', ['conditions' => ['family_head_id' => 0, 'Congregation.installation_id' => $this->request->session()->read('installation')->id], 'order' => ['last_name' => 'ASC', 'first_name' => 'ASC'], 'contain' => ['Congregation']]);
        $privileges = $this->Individuals->Privileges->find('list', ['limit' => 200]);
        $this->set(compact('individual', 'congregations', 'genders', 'appointments', 'familyHeads', 'privileges'));
        $this->set('_serialize', ['individual']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Individual id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $individual = $this->Individuals->get($id, ['contain' => ['Privileges', 'Outlines', 'Studies']]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            if ($this->request->data['is_family_head'] == 1) {
                $this->request->data['family_head_id'] = 0;
            }

            $this->request->data['privileges']['_ids'] = array_merge(
                [], (is_array($this->request->data['privileges1']['_ids']) ? $this->request->data['privileges1']['_ids'] : []), (is_array($this->request->data['privileges2']['_ids']) ? $this->request->data['privileges2']['_ids'] : []), (is_array($this->request->data['privileges3']['_ids']) ? $this->request->data['privileges3']['_ids'] : []), (is_array($this->request->data['privileges4']['_ids']) ? $this->request->data['privileges4']['_ids'] : [])
            );

            $individual = $this->Individuals->patchEntity($individual, $this->request->data, ['associated' => ['Privileges', 'Outlines', 'Studies']]);

            //error_log($individual->outlines[0]);
            //print_r($individual);
            //throw new NotFoundException('Stopping here!');
            
            if ($this->Individuals->save($individual, ['associated' => ['Privileges', 'Outlines', 'Studies']])) {
                $this->Flash->success($individual->full_name.' '.__('has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {

                $x = $individual->errors();
                if ($x) {
                    debug($x);
                }

                debug(json_encode($individual, JSON_PRETTY_PRINT));
                die();

                $this->Flash->error(__('The individual could not be saved. Please, try again.'));
            }
        }
        $congregations = $this->Individuals->Congregation->find('list', ['limit' => 200]);
        $genders = $this->Individuals->Gender->find('list', ['limit' => 200]);
        $appointments = $this->Individuals->Appointment->find('list', ['limit' => 200]);
        $familyHeads = $this->Individuals->FamilyHead->find('list', ['conditions' => ['family_head_id' => 0], 'limit' => 200]);
        $outlines = $this->Individuals->Outlines->find('list');
        $studies = $this->Individuals->Studies->find('list');
        $this->set(compact('individual', 'congregations', 'genders', 'appointments', 'familyHeads', 'outlines', 'studies'));
        $this->set('_serialize', ['individual']);

        $individualPrivileges = $this->Individuals->Privileges->find('list')->matching('Individuals')->where(['IndividualsPrivileges.individual_id' => $individual->id])->toArray();
        $privileges[0] = $this->Individuals->Privileges->find('list')->toArray();
        $privileges[1] = $this->Individuals->Privileges->find('list')->where(['privilege_type_id' => 1])->toArray();
        $privileges[2] = $this->Individuals->Privileges->find('list')->where(['privilege_type_id' => 2])->toArray();
        $privileges[3] = $this->Individuals->Privileges->find('list')->where(['privilege_type_id' => 3])->toArray();
        $privileges[4] = $this->Individuals->Privileges->find('list')->where(['privilege_type_id' => 4])->toArray();
        $this->set(compact('privileges', 'individualPrivileges'));

    }

    
    /**
     * Delete method
     *
     * @param string|null $id Individual id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $individual = $this->Individuals->get($id);
        if ($this->Individuals->delete($individual)) {
            $this->Flash->success(__('The individual has been deleted.'));
        } else {
            $this->Flash->error(__('The individual could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    
    public function editStudy()
    {
        
        $individualStudyId = $this->request->query['individual_study_id'];
        
    }
    
    
    public function aSpeakers()
    {
        
        $this->viewBuilder()->layout('ajax');
        
        $speakers = $this->Individuals->find('list')
            ->where(['congregation_id' => $this->request->query['congregation_id']])
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 15])
            ;
        echo json_encode($speakers);
        die();

    }
    
}
