<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * WeekSongs Controller
 *
 * @property \App\Model\Table\WeekSongsTable $WeekSongs
 */
class WeekSongsController extends AppController
{

	public function isAuthorized($user)
	{
        if($user['is_admin'] == 1) {
            return true;
        }
        
		return parent::isAuthorized($user);
	}
	
        
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        
        if($this->request->session()->check('installation')) {
            $this->set('currentInstallation', $this->request->session()->read('installation'));        
        }
    }
    
    
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Weeks', 'Songs']
        ];
        $this->set('weekSongs', $this->paginate($this->WeekSongs));
        $this->set('_serialize', ['weekSongs']);
    }

    /**
     * View method
     *
     * @param string|null $id Week Song id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $weekSong = $this->WeekSongs->get($id, [
            'contain' => ['Weeks', 'Songs']
        ]);
        $this->set('weekSong', $weekSong);
        $this->set('_serialize', ['weekSong']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $weekSong = $this->WeekSongs->newEntity();
        if ($this->request->is('post')) {
            $weekSong = $this->WeekSongs->patchEntity($weekSong, $this->request->data);
            if ($this->WeekSongs->save($weekSong)) {
                $this->Flash->success(__('The week song has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The week song could not be saved. Please, try again.'));
            }
        }
        $weeks = $this->WeekSongs->Weeks->find('list', ['limit' => 200]);
        $songs = $this->WeekSongs->Songs->find('list', ['limit' => 200]);
        $this->set(compact('weekSong', 'weeks', 'songs'));
        $this->set('_serialize', ['weekSong']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Week Song id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $weekSong = $this->WeekSongs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $weekSong = $this->WeekSongs->patchEntity($weekSong, $this->request->data);
            if ($this->WeekSongs->save($weekSong)) {
                $this->Flash->success(__('The week song has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The week song could not be saved. Please, try again.'));
            }
        }
        $weeks = $this->WeekSongs->Weeks->find('list', ['limit' => 200]);
        $songs = $this->WeekSongs->Songs->find('list', ['limit' => 200]);
        $this->set(compact('weekSong', 'weeks', 'songs'));
        $this->set('_serialize', ['weekSong']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Week Song id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $weekSong = $this->WeekSongs->get($id);
        if ($this->WeekSongs->delete($weekSong)) {
            $this->Flash->success(__('The week song has been deleted.'));
        } else {
            $this->Flash->error(__('The week song could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
