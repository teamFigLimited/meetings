<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PrivilegeTypes Controller
 *
 * @property \App\Model\Table\PrivilegeTypesTable $PrivilegeTypes
 */
class PrivilegeTypesController extends AppController
{

    
	public function isAuthorized($user)
	{
		
		if($this->request->action === 'index') {
			return true;
		}
		
		if($this->request->action === 'add') {
			return true;
		}
		
		if($this->request->action === 'edit') {
			return true;
		}
		
		return parent::isAuthorized($user);
	}
	
		            
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('privilegeTypes', $this->paginate($this->PrivilegeTypes));
        $this->set('_serialize', ['privilegeTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Privilege Type id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $privilegeType = $this->PrivilegeTypes->get($id, [
            'contain' => []
        ]);
        $this->set('privilegeType', $privilegeType);
        $this->set('_serialize', ['privilegeType']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $privilegeType = $this->PrivilegeTypes->newEntity();
        if ($this->request->is('post')) {
            $privilegeType = $this->PrivilegeTypes->patchEntity($privilegeType, $this->request->data);
            if ($this->PrivilegeTypes->save($privilegeType)) {
                $this->Flash->success(__('The privilege type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The privilege type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('privilegeType'));
        $this->set('_serialize', ['privilegeType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Privilege Type id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $privilegeType = $this->PrivilegeTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $privilegeType = $this->PrivilegeTypes->patchEntity($privilegeType, $this->request->data);
            if ($this->PrivilegeTypes->save($privilegeType)) {
                $this->Flash->success(__('The privilege type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The privilege type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('privilegeType'));
        $this->set('_serialize', ['privilegeType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Privilege Type id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $privilegeType = $this->PrivilegeTypes->get($id);
        if ($this->PrivilegeTypes->delete($privilegeType)) {
            $this->Flash->success(__('The privilege type has been deleted.'));
        } else {
            $this->Flash->error(__('The privilege type could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
