<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MeetingWeekends Controller
 *
 * @property \App\Model\Table\MeetingWeekendsTable $MeetingWeekends
 */
class MeetingWeekendsController extends AppController
{

	public function isAuthorized($user)
	{
        
		if($this->request->action === 'edit') {
			return true;
		}
				
		return parent::isAuthorized($user);
	}

    
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Meetings', 'Chairmen', 'Speakers', 'Congregations', 'WtReaders']
        ];
        $this->set('meetingWeekends', $this->paginate($this->MeetingWeekends));
        $this->set('_serialize', ['meetingWeekends']);
    }

    /**
     * View method
     *
     * @param string|null $id Meeting Weekend id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $meetingWeekend = $this->MeetingWeekends->get($id, [
            'contain' => ['Meetings', 'Chairmen', 'Speakers', 'Congregations', 'WtReaders']
        ]);
        $this->set('meetingWeekend', $meetingWeekend);
        $this->set('_serialize', ['meetingWeekend']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $meetingWeekend = $this->MeetingWeekends->newEntity();
        if ($this->request->is('post')) {
            $meetingWeekend = $this->MeetingWeekends->patchEntity($meetingWeekend, $this->request->data);
            if ($this->MeetingWeekends->save($meetingWeekend)) {
                $this->Flash->success(__('The meeting weekend has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The meeting weekend could not be saved. Please, try again.'));
            }
        }
        $meetings = $this->MeetingWeekends->Meetings->find('list', ['limit' => 200]);
        $chairmen = $this->MeetingWeekends->Chairmen->find('list', ['limit' => 200]);
        $speakers = $this->MeetingWeekends->Speakers->find('list', ['limit' => 200]);
        $congregations = $this->MeetingWeekends->Congregations->find('list', ['limit' => 200]);
        $wtReaders = $this->MeetingWeekends->WtReaders->find('list', ['limit' => 200]);
        $this->set(compact('meetingWeekend', 'meetings', 'chairmen', 'speakers', 'congregations', 'wtReaders'));
        $this->set('_serialize', ['meetingWeekend']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Meeting Weekend id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $meetingWeekend = $this->MeetingWeekends->get($id, [
            'contain' => ['Meeting.Week.Month']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $meetingWeekend = $this->MeetingWeekends->patchEntity($meetingWeekend, $this->request->data);
            if ($this->MeetingWeekends->save($meetingWeekend)) {
                $this->Flash->success(__('The meeting weekend has been saved.'));
                return $this->redirect(['controller' => 'meetings', 'action' => 'view', $meetingWeekend->meeting->week_id]);
            } else {
                $this->Flash->error(__('The meeting weekend could not be saved. Please, try again.'));
            }
        }
        
        $songs = range(0, $this->maxSongNumber);
        unset($songs[0]);
        
        $chairmen = $this->MeetingWeekends->Chairman->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 14])
            ->toArray();
        
        $congregations = $this->MeetingWeekends->SpeakerCongregation->find('list')
            ->where(['SpeakerCongregation.installation_id' => $this->request->session()->read('installation')->id])
            ->order(['SpeakerCongregation.id = '.$this->request->session()->read('installation')->congregation_id => 'DESC', 'SpeakerCongregation.name' => 'ASC'])
            ;
        
        
        $speakers = $this->MeetingWeekends->Speaker->find('list')
            ->where(['Speaker.congregation_id' => $this->request->session()->read('installation')->congregation_id])
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 15])
            ->toArray();
        
        $readers = $this->MeetingWeekends->Speaker->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 17])
            ->toArray();
        
        //$readers = $this->MeetingWeekends->Reader->find('list', ['limit' => 200]);
        $this->set(compact('meetingWeekend', 'chairmen', 'speakers', 'congregations', 'readers', 'songs'));
        $this->set('_serialize', ['meetingWeekend']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Meeting Weekend id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $meetingWeekend = $this->MeetingWeekends->get($id);
        if ($this->MeetingWeekends->delete($meetingWeekend)) {
            $this->Flash->success(__('The meeting weekend has been deleted.'));
        } else {
            $this->Flash->error(__('The meeting weekend could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
