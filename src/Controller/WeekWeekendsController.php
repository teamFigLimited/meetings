<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * WeekWeekends Controller
 *
 * @property \App\Model\Table\WeekWeekendsTable $WeekWeekends
 */
class WeekWeekendsController extends AppController
{

	public function isAuthorized($user)
	{
        if($user['is_admin'] == 1) {
            return true;
        }
        
		return parent::isAuthorized($user);
	}
	
    
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Weeks']
        ];
        $this->set('weekWeekends', $this->paginate($this->WeekWeekends));
        $this->set('_serialize', ['weekWeekends']);
    }

    /**
     * View method
     *
     * @param string|null $id Week Weekend id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $weekWeekend = $this->WeekWeekends->get($id, [
            'contain' => ['Weeks']
        ]);
        $this->set('weekWeekend', $weekWeekend);
        $this->set('_serialize', ['weekWeekend']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        
        if(isset($this->request->query['week_id'])) {
            $weekId = $this->request->query['week_id'];
        } else {
            $this->Flash->error(__('There needs to be a week number'));
            return $this->redirect($this->referer());
        }
        
        $weekWeekend = $this->WeekWeekends->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['week_id'] = $weekId;
            $weekWeekend = $this->WeekWeekends->patchEntity($weekWeekend, $this->request->data);
            
            //  TODO: Need to check whether this week / part combo can be added
            
            if ($this->WeekWeekends->save($weekWeekend)) {
                $this->Flash->success(__('The week weekend has been saved.'));
                return $this->redirect(['controller' => 'weeks', 'action' => 'view', $this->request->data['week_id']]);
            } else {
                $this->Flash->error(__('The week weekend could not be saved. Please, try again.'));
            }
        }
        $week = $this->WeekWeekends->Week->get($weekId, ['contain' => ['Month']]);
        $this->set(compact('weekWeekend', 'week'));
        $this->set('_serialize', ['weekWeekend']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Week Weekend id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $weekWeekend = $this->WeekWeekends->get($id, [
            'contain' => ['Week.Month']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $weekWeekend = $this->WeekWeekends->patchEntity($weekWeekend, $this->request->data);
            if ($this->WeekWeekends->save($weekWeekend)) {
                $this->Flash->success(__('The week weekend has been saved.'));
                return $this->redirect(['controller' => 'weeks', 'action' => 'view', $weekWeekend->week_id]);
            } else {
                $this->Flash->error(__('The week weekend could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('weekWeekend'));
        $this->set('_serialize', ['weekWeekend']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Week Weekend id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $weekWeekend = $this->WeekWeekends->get($id);
        if ($this->WeekWeekends->delete($weekWeekend)) {
            $this->Flash->success(__('The week weekend has been deleted.'));
        } else {
            $this->Flash->error(__('The week weekend could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    
    public function activate($id = null)
    {
        if($this->WeekWeekends->updateAll(['is_live' => 1], ['id' => $id])) {
            $this->Flash->success(__('The Weekend Section has been activated.'));
        } else {
            $this->Flash->success(__('The Weekend Section could not be activated yet.'));
        }
        
        return $this->redirect($this->referer());
    }


    public function deactivate($id = null)
    {
        if($this->WeekWeekends->updateAll(['is_live' => 0], ['id' => $id])) {
            $this->Flash->success(__('The Weekend Section has been de-activated.'));
        } else {
            $this->Flash->success(__('The Weekend Section could not be de-activated.'));
        }
        
        return $this->redirect($this->referer());
    }
    
}
