<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Installations Controller
 *
 * @property \App\Model\Table\InstallationsTable $Installations
 */
class InstallationsController extends AppController
{

	public function isAuthorized($user)
	{
        
		if($this->request->action === 'add') {
            return true;
		}

		if($this->request->action === 'view') {
			//  check if this installation is owned by
			if($this->Installations->isOwnedBy((int)$this->request->params['pass'][0], $user['id'])) {
				return true;
			}
		}
		
		if($this->request->action === 'index') {
			return true;
		}
		
		if ($this->request->action === 'settings') {
			//  check if this installation is owned by
			if($this->Installations->isOwnedBy((int)$this->request->params['pass'][0], $user['id'])) {
				return true;
			}
		}
        
		if($this->request->action === 'reports') {
			return true;
		}
		
		return parent::isAuthorized($user);
	}
    
		    
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        //  if installation exists then redirect
        if($this->request->session()->check('installation')) {
            return $this->redirect(['controller' => 'installations', 'action' => 'view', $this->request->session()->read('installation')->id]);
        } else {
            return $this->redirect(['controller' => 'installations', 'action' => 'add']);
        }
        
        $this->paginate = [
            'conditions' => ['user_id' => $this->currentUser['id']],
            'contain' => ['Congregation']
        ];
        $this->set('installations', $this->paginate($this->Installations));
        $this->set('_serialize', ['installations']);
        
        $this->request->session()->delete('installation');
    }

    /**
     * View method
     *
     * @param string|null $id Installation id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $installation = $this->Installations->get($id, [
            'contain' => ['Congregation', 'Meetings']
        ]);
        
        $this->loadModel('Notices');
        $notices = $this->Notices->find()
            ->order(['created' => 'DESC']);
        
        $this->set(compact('installation', 'notices'));
        $this->set('_serialize', ['installation']);
        
        //$this->request->session()->write('installation', $installation);
        //$this->set('currentInstallation', $this->request->session()->read('installation'));
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $installation = $this->Installations->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['user_id'] = $this->currentUser['id'];
            $installation = $this->Installations->patchEntity($installation, $this->request->data, ['fieldList' => ['user_id', 'congregation_name']]);
            if ($this->Installations->save($installation)) {
                
				//  create a congregation
				$congregation = $this->Installations->Congregation->newEntity();
				$congregation = $this->Installations->Congregation->patchEntity($congregation, ['installation_id' => $installation->id, 'name' => $this->request->data['congregation_name']]);
				$this->Installations->Congregation->save($congregation);
				
                //  update the installation
                $installation = $this->Installations->patchEntity($installation, ['congregation_id' => $congregation->id], ['fieldList' => ['user_id', 'congregation_id']]);
                $this->Installations->save($installation);                
                
                $this->Flash->success(__('The installation has been saved.'));
                
                return $this->redirect(['action' => 'index']);
                
            } else {
                $x = $installation->errors();
                if($x) {
                    debug($x);
                }
                
                debug(json_encode($installation, JSON_PRETTY_PRINT));
                die();
                $this->Flash->error(__('The installation could not be saved. Please, try again.'));
                
            }
        }
        $congregations = $this->Installations->Congregation->find('list');
        $this->set(compact('installation', 'congregations'));
        $this->set('_serialize', ['installation']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Installation id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $installation = $this->Installations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $installation = $this->Installations->patchEntity($installation, $this->request->data);
            if ($this->Installations->save($installation)) {
                $this->Flash->success(__('The installation has been saved.'));
                return $this->redirect(['action' => 'view', $installation->id]);
            } else {
                $this->Flash->error(__('The installation could not be saved. Please, try again.'));
            }
        }
        $congregations = $this->Installations->Congregation->find('list', ['conditions' => ['installation_id' => $this->request->session()->read('installation')->id], 'limit' => 200]);
        $coordinators = $this->Installations->Coordinator->find('list')->where(['congregation_id' => $this->request->session()->read('installation')->congregation_id])->matching('Appointment')->where(['is_elder' => 1])->toArray();
        $secretaries = $this->Installations->Secretary->find('list')->where(['congregation_id' => $this->request->session()->read('installation')->congregation_id])->matching('Appointment')->where(['is_elder' => 1])->toArray();
        $serviceOverseers = $this->Installations->ServiceOverseer->find('list')->where(['congregation_id' => $this->request->session()->read('installation')->congregation_id])->matching('Appointment')->where(['is_elder' => 1])->toArray();
        $this->set(compact('installation', 'congregations', 'coordinators', 'secretaries', 'serviceOverseers'));
        $this->set('_serialize', ['installation']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Installation id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $installation = $this->Installations->get($id);
        if ($this->Installations->delete($installation)) {
            $this->Flash->success(__('The installation has been deleted.'));
        } else {
            $this->Flash->error(__('The installation could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    
    public function settings($id = null)
    {
        $installation = $this->Installations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $installation = $this->Installations->patchEntity($installation, $this->request->data, ['fieldList' => ['congregation_id', 'circuit_overseer_name', 'coordinator_id', 'secretary_id', 'service_overseer_id', 'clm_overseer_id', 'classes_number', 'time_midweek', 'time_weekend']]);
            if ($this->Installations->save($installation)) {
                $this->Flash->success(__('The Settings have been saved.'));
                return $this->redirect(['action' => 'view', $installation->id]);
            } else {
                $this->Flash->error(__('The Settings could not be saved. Please, try again.'));
            }
        }
        $congregations = $this->Installations->Congregation->find('list', ['conditions' => ['installation_id' => $this->request->session()->read('installation')->id], 'limit' => 200]);
        $coordinators = $this->Installations->Coordinator->find('list')->where(['congregation_id' => $this->request->session()->read('installation')->congregation_id])->matching('Appointment')->where(['is_elder' => 1])->toArray();
        $secretaries = $this->Installations->Secretary->find('list')->where(['congregation_id' => $this->request->session()->read('installation')->congregation_id])->matching('Appointment')->where(['is_elder' => 1])->toArray();
        $serviceOverseers = $this->Installations->ServiceOverseer->find('list')->where(['congregation_id' => $this->request->session()->read('installation')->congregation_id])->matching('Appointment')->where(['is_elder' => 1])->toArray();
        $clmOverseers = $this->Installations->ClmOverseer->find('list')->where(['congregation_id' => $this->request->session()->read('installation')->congregation_id])->matching('Appointment')->where(['is_elder' => 1])->toArray();
        $this->set(compact('installation', 'congregations', 'coordinators', 'secretaries', 'serviceOverseers', 'clmOverseers'));
        $this->set('_serialize', ['installation']);
    }
    
    
    public function reports($id = null)
    {
        $currentMonth = date('M');
        $nextMonth = date('M', strtotime('+1 Month'));
        $this->set(compact('currentMonth', 'nextMonth'));
    }
    
}
