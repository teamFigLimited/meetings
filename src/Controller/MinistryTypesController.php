<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MinistryTypes Controller
 *
 * @property \App\Model\Table\MinistryTypesTable $MinistryTypes
 */
class MinistryTypesController extends AppController
{
    
	public function isAuthorized($user)
	{
        //  only admins have full access to Weeks
        if($user['is_admin'] == 1) {
            return true;
        }
		return parent::isAuthorized($user);
	}
    

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('ministryTypes', $this->paginate($this->MinistryTypes));
        $this->set('_serialize', ['ministryTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Ministry Type id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ministryType = $this->MinistryTypes->get($id, [
            'contain' => []
        ]);
        $this->set('ministryType', $ministryType);
        $this->set('_serialize', ['ministryType']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ministryType = $this->MinistryTypes->newEntity();
        if ($this->request->is('post')) {
            $ministryType = $this->MinistryTypes->patchEntity($ministryType, $this->request->data);
            if ($this->MinistryTypes->save($ministryType)) {
                $this->Flash->success(__('The ministry type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ministry type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ministryType'));
        $this->set('_serialize', ['ministryType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ministry Type id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ministryType = $this->MinistryTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ministryType = $this->MinistryTypes->patchEntity($ministryType, $this->request->data);
            if ($this->MinistryTypes->save($ministryType)) {
                $this->Flash->success(__('The ministry type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ministry type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ministryType'));
        $this->set('_serialize', ['ministryType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ministry Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ministryType = $this->MinistryTypes->get($id);
        if ($this->MinistryTypes->delete($ministryType)) {
            $this->Flash->success(__('The ministry type has been deleted.'));
        } else {
            $this->Flash->error(__('The ministry type could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
