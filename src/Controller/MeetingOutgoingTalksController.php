<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MeetingOutgoingTalks Controller
 *
 * @property \App\Model\Table\MeetingOutgoingTalksTable $MeetingOutgoingTalks
 */
class MeetingOutgoingTalksController extends AppController
{

	public function isAuthorized($user)
	{
        
		if($this->request->action === 'index') {
			return true;
		}
				
		if($this->request->action === 'add') {
            //  need to check that meeting_id belongs to user
            
			return true;
		}
				
		if($this->request->action === 'edit') {
            //  need to check that meeting_id belongs to user
            
			return true;
		}
				
		return parent::isAuthorized($user);
        
	}
    
        
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Meetings', 'Speakers', 'Congregations', 'Outlines']
        ];
        $this->set('meetingOutgoingTalks', $this->paginate($this->MeetingOutgoingTalks));
        $this->set('_serialize', ['meetingOutgoingTalks']);
    }

    /**
     * View method
     *
     * @param string|null $id Meeting Outgoing Talk id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $meetingOutgoingTalk = $this->MeetingOutgoingTalks->get($id, [
            'contain' => ['Meetings', 'Speakers', 'Congregations', 'Outlines']
        ]);
        $this->set('meetingOutgoingTalk', $meetingOutgoingTalk);
        $this->set('_serialize', ['meetingOutgoingTalk']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $meetingOutgoingTalk = $this->MeetingOutgoingTalks->newEntity();
        if ($this->request->is('post')) {
            $meetingOutgoingTalk = $this->MeetingOutgoingTalks->patchEntity($meetingOutgoingTalk, $this->request->data);
            if ($this->MeetingOutgoingTalks->save($meetingOutgoingTalk)) {
                $this->Flash->success(__('The meeting outgoing talk has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The meeting outgoing talk could not be saved. Please, try again.'));
            }
        }
        $meeting = $this->request->query['meeting_id'];
        
        $speakers = $this->MeetingOutgoingTalks->Speaker->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 16])
            ->toArray();
        
        //$speakers = $this->MeetingOutgoingTalks->Speaker->find('list');
        $congregations = $this->MeetingOutgoingTalks->Congregation->find('list')
            ->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->where(['Congregation.id <>' => $this->request->session()->read('installation')->congregation_id])
            ->order(['Congregation.name' => 'ASC'])
            ;
        $outlines = $this->MeetingOutgoingTalks->Outline->find('list', ['limit' => 200]);
        $this->set(compact('meetingOutgoingTalk', 'meeting', 'speakers', 'congregations', 'outlines'));
        $this->set('_serialize', ['meetingOutgoingTalk']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Meeting Outgoing Talk id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $meetingOutgoingTalk = $this->MeetingOutgoingTalks->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $meetingOutgoingTalk = $this->MeetingOutgoingTalks->patchEntity($meetingOutgoingTalk, $this->request->data);
            if ($this->MeetingOutgoingTalks->save($meetingOutgoingTalk)) {
                $this->Flash->success(__('The meeting outgoing talk has been saved.'));
                return $this->redirect(['controller' => 'meetings', 'action' => 'view', $meetingOutgoingTalk->meeting_id]);
            } else {
                $this->Flash->error(__('The meeting outgoing talk could not be saved. Please, try again.'));
            }
        }
        $meeting = $this->request->query['meeting_id'];
        //$speakers = $this->MeetingOutgoingTalks->Speaker->find('list', ['limit' => 200]);
        $speakers = $this->MeetingOutgoingTalks->Speaker->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 16])
            ->toArray();
        
        //$congregations = $this->MeetingOutgoingTalks->Congregation->find('list', ['limit' => 200]);
        $congregations = $this->MeetingOutgoingTalks->Congregation->find('list')
            ->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->where(['Congregation.id <>' => $this->request->session()->read('installation')->congregation_id])
            ->order(['Congregation.name' => 'ASC'])
            ;
        $outlines = $this->MeetingOutgoingTalks->Outline->find('list', ['limit' => 200]);
        $this->set(compact('meetingOutgoingTalk', 'meeting', 'speakers', 'congregations', 'outlines'));
        $this->set('_serialize', ['meetingOutgoingTalk']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Meeting Outgoing Talk id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $meetingOutgoingTalk = $this->MeetingOutgoingTalks->get($id);
        if ($this->MeetingOutgoingTalks->delete($meetingOutgoingTalk)) {
            $this->Flash->success(__('The meeting outgoing talk has been deleted.'));
        } else {
            $this->Flash->error(__('The meeting outgoing talk could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
