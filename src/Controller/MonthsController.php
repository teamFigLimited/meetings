<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Core\Configure;

/**
 * Months Controller
 *
 * @property \App\Model\Table\MonthsTable $Months
 */
class MonthsController extends AppController
{

    
	public function isAuthorized($user)
	{
        if($user['is_admin'] == 1) {
            return true;
        }
     
        if($this->request->action === 'reports') {
            return true;
        }

        if($this->request->action === 'clmReport') {
            return true;
        }

		return parent::isAuthorized($user);
	}
	
    
    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }
    
    
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('months', $this->paginate($this->Months));
        $this->set('_serialize', ['months']);
    }

    /**
     * View method
     *
     * @param string|null $id Month id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $month = $this->Months->get($id, [
            'contain' => ['Weeks']
        ]);
        $this->set('month', $month);
        $this->set('_serialize', ['month']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $month = $this->Months->newEntity();
        if ($this->request->is('post')) {
            $month = $this->Months->patchEntity($month, $this->request->data);
            if ($this->Months->save($month)) {
                $this->Flash->success(__('The month has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The month could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('month'));
        $this->set('_serialize', ['month']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Month id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $month = $this->Months->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $month = $this->Months->patchEntity($month, $this->request->data);
            if ($this->Months->save($month)) {
                $this->Flash->success(__('The month has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The month could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('month'));
        $this->set('_serialize', ['month']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Month id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $month = $this->Months->get($id);
        if ($this->Months->delete($month)) {
            $this->Flash->success(__('The month has been deleted.'));
        } else {
            $this->Flash->error(__('The month could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    
    public function report($id = null)
    {
        $month = $this->Months->get($id);        
        $this->set('month', $month);
    }
    
    
    public function clmReport($id = null, $format = null)
    {
        $month = $this->Months->get(
            $id, [
                'contain' => [
                    'Weeks.Meetings' => ['conditions' => ['Meetings.installation_id' => $this->request->session()->read('installation')->id]],
                    'Weeks.Meetings.Installation',
                    'Weeks.Meetings.MeetingWeekType',
                    'Weeks.WeekMidweek',
                    'Weeks.Meetings.MeetingMidweek.Chairman',
                    'Weeks.Meetings.MeetingMidweek.OpeningPrayer',
                    'Weeks.Meetings.MeetingMidweek.TreasuresTalkIndividual',
                    'Weeks.Meetings.MeetingMidweek.TreasuresGemsIndividual',
                    'Weeks.Meetings.MeetingMidweek.TreasuresReadingIndividual',
                    'Weeks.Meetings.MeetingMidweek.TreasuresReadingStudy',
                    'Weeks.Meetings.MeetingMidweek.MinistryPresentationIndividual',
                    'Weeks.Meetings.MeetingMidweek.MinistryItem1Type',
                    'Weeks.Meetings.MeetingMidweek.MinistryItem1Student',
                    'Weeks.Meetings.MeetingMidweek.MinistryItem1Study',
                    'Weeks.Meetings.MeetingMidweek.MinistryItem1Assistant',
                    'Weeks.Meetings.MeetingMidweek.MinistryItem2Type',
                    'Weeks.Meetings.MeetingMidweek.MinistryItem2Student',
                    'Weeks.Meetings.MeetingMidweek.MinistryItem2Study',
                    'Weeks.Meetings.MeetingMidweek.MinistryItem2Assistant',
                    'Weeks.Meetings.MeetingMidweek.MinistryItem3Type',
                    'Weeks.Meetings.MeetingMidweek.MinistryItem3Student',
                    'Weeks.Meetings.MeetingMidweek.MinistryItem3Study',
                    'Weeks.Meetings.MeetingMidweek.MinistryItem3Assistant',
                    'Weeks.Meetings.MeetingMidweek.LivingItem1Individual',
                    'Weeks.Meetings.MeetingMidweek.LivingItem2Individual',
                    'Weeks.Meetings.MeetingMidweek.LivingCbsConductor',
                    'Weeks.Meetings.MeetingMidweek.LivingCbsReader',
                    'Weeks.Meetings.MeetingMidweek.ClosingPrayer',
                ]
            ]
        );
        $this->set('month', $month);
        
        
        if($this->request->_ext === 'pdf') {
            
            //  friendsOfCake
            Configure::write('CakePdf', [
                'className' => 'CakePdf.WkHtmlToPdf',
                'engine' => 'CakePdf.WkHtmlToPdf',
                'binary' => '/usr/bin/wkhtmltopdf',
                'margin' => [
                    'bottom' => 8,
                    'left' => 8,
                    'right' => 8,
                    'top' => 9
                ],
                'orientation' => 'portrait',
                'download' => false,
                'encoding' => 'UTF-8'
            ]);

            $this->render('clmReport_'.$format, 'wkhtml');
            
        }
        
        
    }
    
    
    public function reports()
    {
        
        //  init base date to work from
        $now = Time::now();
        
        //  change to first of month
        $now->day(1);
        
        //  change to last month
        $now->subMonths(1);

        //  get last month, this month and next 2
        $months = $this->Months->find(
            'all', 
            [
                'conditions' => [
                    'date_first >=' => $now->year.'-'.$now->month.'-'.$now->day
                ],
                'order' => ['date_first' => 'ASC'],
                'limit' => 4
            ]
        );
        $this->set(compact('months'));
    }
    
}
