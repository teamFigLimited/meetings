<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * MeetingDays Controller
 *
 * @property \App\Model\Table\MeetingDaysTable $MeetingDays
 */
class MeetingDaysController extends AppController
{

	public function isAuthorized($user)
	{
		
        if($user['is_admin'] == 1) {
            return true;
        }
		
		return parent::isAuthorized($user);
	}
	
        
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        
        if($this->request->session()->check('installation')) {
            $this->set('currentInstallation', $this->request->session()->read('installation'));        
        }
    }
    
        
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('meetingDays', $this->paginate($this->MeetingDays));
        $this->set('_serialize', ['meetingDays']);
    }

    /**
     * View method
     *
     * @param string|null $id Meeting Day id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $meetingDay = $this->MeetingDays->get($id, [
            'contain' => []
        ]);
        $this->set('meetingDay', $meetingDay);
        $this->set('_serialize', ['meetingDay']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $meetingDay = $this->MeetingDays->newEntity();
        if ($this->request->is('post')) {
            $meetingDay = $this->MeetingDays->patchEntity($meetingDay, $this->request->data);
            if ($this->MeetingDays->save($meetingDay)) {
                $this->Flash->success(__('The meeting day has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The meeting day could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('meetingDay'));
        $this->set('_serialize', ['meetingDay']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Meeting Day id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $meetingDay = $this->MeetingDays->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $meetingDay = $this->MeetingDays->patchEntity($meetingDay, $this->request->data);
            if ($this->MeetingDays->save($meetingDay)) {
                $this->Flash->success(__('The meeting day has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The meeting day could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('meetingDay'));
        $this->set('_serialize', ['meetingDay']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Meeting Day id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $meetingDay = $this->MeetingDays->get($id);
        if ($this->MeetingDays->delete($meetingDay)) {
            $this->Flash->success(__('The meeting day has been deleted.'));
        } else {
            $this->Flash->error(__('The meeting day could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
