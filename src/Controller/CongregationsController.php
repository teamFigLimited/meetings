<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Congregations Controller
 *
 * @property \App\Model\Table\CongregationsTable $Congregations
 */
class CongregationsController extends AppController
{

	public function isAuthorized($user)
	{
        
		if($this->request->action === 'index') {
			return true;
		}

		if($this->request->action === 'view') {
			//  check if this congregation is owned by
			if($this->Congregations->isOwnedBy((int)$this->request->params['pass'][0], $this->request->session()->read('installation')->id)) {
				return true;
			}
		}

		if($this->request->action === 'add') {
			return true;
		}
        
		if($this->request->action === 'reports') {
			return true;
		}
        
		if($this->request->action === 'clmStudents') {
			return true;
		}
        
    }
    
    
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'conditions' => ['installation_id' => $this->request->session()->read('installation')->id],
            'contain' => ['Installation']
        ];
        $this->set('congregations', $this->paginate($this->Congregations));
        $this->set('_serialize', ['congregations']);
    }

    /**
     * View method
     *
     * @param string|null $id Congregation id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $congregation = $this->Congregations->get($id, [
            'contain' => ['Installation', 'Individuals.Appointment', 'Individuals.FamilyHead']
        ]);
        $this->set('congregation', $congregation);
        $this->set('_serialize', ['congregation']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $congregation = $this->Congregations->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['installation_id'] = $this->request->session()->read('installation')->id;
            $congregation = $this->Congregations->patchEntity($congregation, $this->request->data);
            if ($this->Congregations->save($congregation)) {
                $this->Flash->success(__('The congregation has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The congregation could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('congregation'));
        $this->set('_serialize', ['congregation']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Congregation id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $congregation = $this->Congregations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $congregation = $this->Congregations->patchEntity($congregation, $this->request->data);
            if ($this->Congregations->save($congregation)) {
                $this->Flash->success(__('The congregation has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The congregation could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('congregation'));
        $this->set('_serialize', ['congregation']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Congregation id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $congregation = $this->Congregations->get($id);
        if ($this->Congregations->delete($congregation)) {
            $this->Flash->success(__('The congregation has been deleted.'));
        } else {
            $this->Flash->error(__('The congregation could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    
    public function reports()
    {
        
    }
    
    
    public function clmStudents() {
        $students = $this->Congregations->Individuals->find()
            ->contain(['Congregation'])
            ->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            /*
            ->contain(['Privileges' => function($q) {
                return $q
                    ->select(['Privileges.id'])
                    ->where(['Privileges.id' => 23]);
            }])
            */
            ->matching('Privileges', function($q) {
                return $q->where(['Privileges.id' => 23]);
            })
            ->contain(['Privileges'])
            ->group(['Individuals.id'])
            ->order(['Individuals.last_name' => 'ASC', 'Individuals.first_name' => 'ASC'])
            ;
            
        
        foreach($students as $student) {
            $privileges = [];
            foreach($student->privileges as $privilege) {
                $privileges[$privilege->id] = $privilege;
            }
            $student->privileges_abbrev = $privileges;
        }
        $this->set(compact('students'));
    }
    
}
