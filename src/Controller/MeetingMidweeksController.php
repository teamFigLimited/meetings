<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\Query;
use Cake\I18n\Time;
use Cake\Core\Configure;
use Cake\View\View;
use App\Controller\Request;

/**
 * MeetingMidweeks Controller
 *
 * @property \App\Model\Table\MeetingMidweeksTable $MeetingMidweeks
 */
class MeetingMidweeksController extends AppController
{

	public function isAuthorized($user)
	{

		if($this->request->action === 'edit') {
			return true;
		}

        if($this->request->action === 'reports') {
			return true;
		}

    if($this->request->action === 'clmWorksheet') {
			return true;
		}

		if($this->request->action === 'assignmentSlips') {
			return true;
		}

		if($this->request->action === 'editTreasuresTalk') {
            //  check this user is ok to edit
			return true;
		}

		if($this->request->action === 'editTreasuresGems') {
            //  check this user is ok to edit
			return true;
		}

		if($this->request->action === 'editTreasuresReading') {
            //  check this user is ok to edit
			return true;
		}

		if($this->request->action === 'editMinistryItem1') {
            //  check this user is ok to edit
			return true;
		}

		if($this->request->action === 'editMinistryItem2') {
            //  check this user is ok to edit
			return true;
		}

		if($this->request->action === 'editMinistryItem3') {
            //  check this user is ok to edit
			return true;
		}

		if($this->request->action === 'aByIndividual') {
            //  check this user is ok to edit
			return true;
		}

		return parent::isAuthorized($user);
	}


    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Meetings', 'Chairmen', 'OpeningPrayers', 'TreasuresTalkIndividuals', 'TreasuresGemsIndividuals', 'TreasuresReadingIndividuals', 'MinistryPresentationIndividuals', 'MinistryInitialStudents', 'MinistryInitialAssistants', 'MinistryReturnStudents', 'MinistryReturnAssistants', 'MinistryStudyStudents', 'MinistryStudyAssistants', 'LivingItem1Individuals', 'LivingItem2Individuals', 'LivingCbsConductors', 'LivingCbsReaders', 'ClosingPrayers']
        ];
        $this->set('meetingMidweeks', $this->paginate($this->MeetingMidweeks));
        $this->set('_serialize', ['meetingMidweeks']);
    }

    /**
     * View method
     *
     * @param string|null $id Meeting Midweek id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $meetingMidweek = $this->MeetingMidweeks->get($id, [
            'contain' => ['Meeting', 'Chairman', 'OpeningPrayer', 'TreasuresTalkIndividuals', 'TreasuresGemsIndividuals', 'TreasuresReadingIndividuals', 'MinistryPresentationIndividuals', 'MinistryInitialStudents', 'MinistryInitialAssistants', 'MinistryReturnStudents', 'MinistryReturnAssistants', 'MinistryStudyStudents', 'MinistryStudyAssistants', 'LivingItem1Individuals', 'LivingItem2Individuals', 'LivingCbsConductors', 'LivingCbsReaders', 'ClosingPrayers']
        ]);
        $this->set('meetingMidweek', $meetingMidweek);
        $this->set('_serialize', ['meetingMidweek']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $meetingMidweek = $this->MeetingMidweeks->newEntity();
        if ($this->request->is('post')) {
            $meetingMidweek = $this->MeetingMidweeks->patchEntity($meetingMidweek, $this->request->data);
            if ($this->MeetingMidweeks->save($meetingMidweek)) {
                $this->Flash->success(__('The meeting midweek has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The meeting midweek could not be saved. Please, try again.'));
            }
        }
        $meetings = $this->MeetingMidweeks->Meetings->find('list', ['limit' => 200]);
        $chairmen = $this->MeetingMidweeks->Chairmen->find('list', ['limit' => 200]);
        $openingPrayers = $this->MeetingMidweeks->OpeningPrayers->find('list', ['limit' => 200]);
        $treasuresTalkIndividuals = $this->MeetingMidweeks->TreasuresTalkIndividuals->find('list', ['limit' => 200]);
        $treasuresGemsIndividuals = $this->MeetingMidweeks->TreasuresGemsIndividuals->find('list', ['limit' => 200]);
        $treasuresReadingIndividuals = $this->MeetingMidweeks->TreasuresReadingIndividuals->find('list', ['limit' => 200]);
        $ministryPresentationIndividuals = $this->MeetingMidweeks->MinistryPresentationIndividuals->find('list', ['limit' => 200]);
        $ministryInitialStudents = $this->MeetingMidweeks->MinistryInitialStudents->find('list', ['limit' => 200]);
        $ministryInitialAssistants = $this->MeetingMidweeks->MinistryInitialAssistants->find('list', ['limit' => 200]);
        $ministryReturnStudents = $this->MeetingMidweeks->MinistryReturnStudents->find('list', ['limit' => 200]);
        $ministryReturnAssistants = $this->MeetingMidweeks->MinistryReturnAssistants->find('list', ['limit' => 200]);
        $ministryStudyStudents = $this->MeetingMidweeks->MinistryStudyStudents->find('list', ['limit' => 200]);
        $ministryStudyAssistants = $this->MeetingMidweeks->MinistryStudyAssistants->find('list', ['limit' => 200]);
        $livingItem1Individuals = $this->MeetingMidweeks->LivingItem1Individuals->find('list', ['limit' => 200]);
        $livingItem2Individuals = $this->MeetingMidweeks->LivingItem2Individuals->find('list', ['limit' => 200]);
        $livingCbsConductors = $this->MeetingMidweeks->LivingCbsConductors->find('list', ['limit' => 200]);
        $livingCbsReaders = $this->MeetingMidweeks->LivingCbsReaders->find('list', ['limit' => 200]);
        $closingPrayers = $this->MeetingMidweeks->ClosingPrayers->find('list', ['limit' => 200]);
        $this->set(compact('meetingMidweek', 'meetings', 'chairmen', 'openingPrayers', 'treasuresTalkIndividuals', 'treasuresGemsIndividuals', 'treasuresReadingIndividuals', 'ministryPresentationIndividuals', 'ministryInitialStudents', 'ministryInitialAssistants', 'ministryReturnStudents', 'ministryReturnAssistants', 'ministryStudyStudents', 'ministryStudyAssistants', 'livingItem1Individuals', 'livingItem2Individuals', 'livingCbsConductors', 'livingCbsReaders', 'closingPrayers'));
        $this->set('_serialize', ['meetingMidweek']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Meeting Midweek id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {

        $meetingMidweek = $this->MeetingMidweeks->get($id, [
            'contain' => ['TreasuresTalkIndividual', 'Meeting.Week.WeekMidweek', 'Meeting.Week.Month', 'Chairman.Privileges', 'MinistryItem1Type', 'MinistryItem2Type', 'MinistryItem3Type']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $meetingMidweek = $this->MeetingMidweeks->patchEntity($meetingMidweek, $this->request->data);
            if ($this->MeetingMidweeks->save($meetingMidweek)) {
                $this->Flash->success(__('The meeting midweek has been saved.'));
                return $this->redirect(['controller' => 'meetings', 'action' => 'view', $meetingMidweek->meeting->id]);
            } else {
                $this->Flash->error(__('The meeting midweek could not be saved. Please, try again.'));
            }
        }
        $songs = range(0, $this->maxSongNumber);
        unset($songs[0]);

        $chairmen = $this->MeetingMidweeks->Chairman->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 1])
            ->order(['Chairman.last_name' => 'ASC', 'Chairman.first_name' => 'ASC'])
            ->toArray();
        $openingPrayers = $this->MeetingMidweeks->OpeningPrayer->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 18])
            ->order(['OpeningPrayer.last_name' => 'ASC', 'OpeningPrayer.first_name' => 'ASC'])
            ->toArray();
        $treasuresTalkIndividuals = $this->MeetingMidweeks->TreasuresTalkIndividual->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 2])
            ->order(['TreasuresTalkIndividual.last_name' => 'ASC', 'TreasuresTalkIndividual.first_name' => 'ASC'])
            ->toArray();
        $treasuresGemsIndividuals = $this->MeetingMidweeks->TreasuresGemsIndividual->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 3])
            ->order(['TreasuresGemsIndividual.last_name' => 'ASC', 'TreasuresGemsIndividual.first_name' => 'ASC'])
            ->toArray();

        $treasuresReadingIndividuals = $this->MeetingMidweeks->TreasuresReadingIndividual->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 4])
            ->order(['TreasuresReadingIndividual.last_name' => 'ASC', 'TreasuresReadingIndividual.first_name' => 'ASC'])
            ->toArray()
            ;
        /*
        $treasuresReadingIndividuals = $this->MeetingMidweeks->TreasuresReadingIndividual->find()
            ->distinct(['TreasuresReadingIndividual.id'])
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 4])

            ->contain(['LastTreasuresReading' => [
                'queryBuilder' => function(Query $q) {
                    return $q
                        ->select(['id', 'week_id', 'treasures_reading_individual_id'])
                        ->order(['week_id' => 'DESC'])
                        //->limit(1)
                        ;
                }
            ]])

            ->order(['LastTreasuresReading.week_id' => 'ASC'])
            ->toArray()
            ;
        */

        $ministryPresentationIndividuals = $this->MeetingMidweeks->MinistryPresentationIndividual->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 5])
            ->order(['MinistryPresentationIndividual.last_name' => 'ASC', 'MinistryPresentationIndividual.first_name' => 'ASC'])
            ->toArray();
        $ministryItem1Students = $this->MeetingMidweeks->MinistryItem1Student->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 6])
            ->order(['MinistryItem1Student.last_name' => 'ASC', 'MinistryItem1Student.first_name' => 'ASC'])
            ->toArray();
        $ministryItem1Assistants = $this->MeetingMidweeks->MinistryItem1Assistant->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 9])
            ->order(['MinistryItem1Assistant.last_name' => 'ASC', 'MinistryItem1Assistant.first_name' => 'ASC'])
            ->toArray();
        $ministryItem2Students = $this->MeetingMidweeks->MinistryItem2Student->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 7])
            ->order(['MinistryItem2Student.last_name' => 'ASC', 'MinistryItem2Student.first_name' => 'ASC'])
            ->toArray();
        $ministryItem2Assistants = $this->MeetingMidweeks->MinistryItem2Assistant->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 9])
            ->order(['MinistryItem2Assistant.last_name' => 'ASC', 'MinistryItem2Assistant.first_name' => 'ASC'])
            ->toArray();
        $ministryItem3Students = $this->MeetingMidweeks->MinistryItem3Student->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 8])
            ->order(['MinistryItem3Student.last_name' => 'ASC', 'MinistryItem3Student.first_name' => 'ASC'])
            ->toArray();
        $ministryItem3Assistants = $this->MeetingMidweeks->MinistryItem3Assistant->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 9])
            ->order(['MinistryItem3Assistant.last_name' => 'ASC', 'MinistryItem3Assistant.first_name' => 'ASC'])
            ->toArray();
        $livingItem1Individuals = $this->MeetingMidweeks->LivingItem1Individual->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 11])
            ->order(['LivingItem1Individual.last_name' => 'ASC', 'LivingItem1Individual.first_name' => 'ASC'])
            ->toArray();
        $livingItem2Individuals = $this->MeetingMidweeks->LivingItem2Individual->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 11])
            ->order(['LivingItem2Individual.last_name' => 'ASC', 'LivingItem2Individual.first_name' => 'ASC'])
            ->toArray();
        $livingCbsConductors = $this->MeetingMidweeks->LivingCbsConductor->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 12])
            ->order(['LivingCbsConductor.last_name' => 'ASC', 'LivingCbsConductor.first_name' => 'ASC'])
            ->toArray();
        $livingCbsReaders = $this->MeetingMidweeks->LivingCbsReader->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 13])
            ->order(['LivingCbsReader.last_name' => 'ASC', 'LivingCbsReader.first_name' => 'ASC'])
            ->toArray();
        $closingPrayers = $this->MeetingMidweeks->ClosingPrayer->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 18])
            ->order(['ClosingPrayer.last_name' => 'ASC', 'ClosingPrayer.first_name' => 'ASC'])
            ->toArray();
        $this->set(compact('songs', 'meetingMidweek', 'chairmen', 'openingPrayers', 'treasuresTalkIndividuals', 'treasuresGemsIndividuals', 'treasuresReadingIndividuals', 'ministryPresentationIndividuals', 'ministryItem1Students', 'ministryItem1Assistants', 'ministryItem2Students', 'ministryItem2Assistants', 'ministryItem3Students', 'ministryItem3Assistants', 'livingItem1Individuals', 'livingItem2Individuals', 'livingCbsConductors', 'livingCbsReaders', 'closingPrayers'));
        $this->set('_serialize', ['meetingMidweek']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Meeting Midweek id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $meetingMidweek = $this->MeetingMidweeks->get($id);
        if ($this->MeetingMidweeks->delete($meetingMidweek)) {
            $this->Flash->success(__('The meeting midweek has been deleted.'));
        } else {
            $this->Flash->error(__('The meeting midweek could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }


    public function reports() {

        //  init base date to work from
        $now = Time::now();

        //  change to 6 days ago (should cover last week)
        $now->subDays(28);

        $installationId = $this->request->session()->read('installation')->id;

        $meetingMidweeks = $this->MeetingMidweeks->find()
					->order(['MeetingMidweeks.id' => 'DESC'])
					->limit(26)
          ->matching('Meeting', function($q) use($installationId) {
            return $q
							->where(['Meeting.installation_id' => $installationId]);
          })
          ->contain(['Meeting.Week'])
          ;

        $this->set(compact('meetingMidweeks'));

    }


		public function assignmentSlips($id = null, $format = null)
		{
			$meetingMidweek = $this->MeetingMidweeks->find()
				->where(['MeetingMidweeks.id' => $id])
				->contain(['TreasuresReadingIndividual'])
				->contain(['TreasuresReadingStudy'])
				->contain(['Meeting.Week.WeekMidweek.MinistryItem1Type'])
				->contain(['MinistryItem1Student'])
				->contain(['MinistryItem1Study'])
				->contain(['MinistryItem1Assistant'])
				->contain(['Meeting.Week.WeekMidweek.MinistryItem2Type'])
				->contain(['MinistryItem2Student'])
				->contain(['MinistryItem2Study'])
				->contain(['MinistryItem2Assistant'])
				->contain(['Meeting.Week.WeekMidweek.MinistryItem3Type'])
				->contain(['MinistryItem3Student'])
				->contain(['MinistryItem3Study'])
				->contain(['MinistryItem3Assistant'])
				->first()
				;
				$this->set(compact('meetingMidweek'));

			if($this->request->_ext === 'pdf') {
				//  friendsOfCake
				Configure::write('CakePdf', [
					'className' => 'CakePdf.WkHtmlToPdf',
					'engine' => 'CakePdf.WkHtmlToPdf',
					'binary' => '/usr/bin/wkhtmltopdf',
					'margin' => [
						'bottom' => 8,
						'left' => 8,
						'right' => 8,
						'top' => 10
					],
					'orientation' => 'portrait',
					'download' => false,
					'encoding' => 'UTF-8'
				]);
				$this->render('assignment_slips', 'wkhtml');
			}
		}


    public function clmWorksheet($id = null, $format = null)
    {

        $meetingMidweek = $this->MeetingMidweeks->find()
            ->where(['MeetingMidweeks.id' => $id])
            ->contain(['Meeting.Week.Month'])
            ->contain(['Chairman'])
            ->contain(['OpeningPrayer'])
            ->contain(['TreasuresTalkIndividual'])
            ->contain(['TreasuresGemsIndividual'])
            ->contain(['TreasuresReadingIndividual'])
            ->contain(['TreasuresReadingStudy'])
            ->contain(['MinistryPresentationIndividual'])
            ->contain(['Meeting.Week.WeekMidweek.MinistryItem1Type'])
            ->contain(['MinistryItem1Student'])
            ->contain(['MinistryItem1Study'])
            ->contain(['MinistryItem1Assistant'])
            ->contain(['Meeting.Week.WeekMidweek.MinistryItem2Type'])
            ->contain(['MinistryItem2Student'])
            ->contain(['MinistryItem2Study'])
            ->contain(['MinistryItem2Assistant'])
            ->contain(['Meeting.Week.WeekMidweek.MinistryItem3Type'])
            ->contain(['MinistryItem3Student'])
            ->contain(['MinistryItem3Study'])
            ->contain(['MinistryItem3Assistant'])
            ->contain(['LivingItem1Individual'])
            ->contain(['LivingItem2Individual'])
            ->contain(['LivingCbsConductor'])
            ->contain(['LivingCbsReader'])
            ->contain(['Meeting.Installation'])
            ->contain(['ClosingPrayer'])
            ->first()
            ;

        $this->set(compact('meetingMidweek'));

        if($this->request->_ext === 'pdf') {

            //  friendsOfCake
            Configure::write('CakePdf', [
                'className' => 'CakePdf.WkHtmlToPdf',
                'engine' => 'CakePdf.WkHtmlToPdf',
                'binary' => '/usr/bin/wkhtmltopdf',
                'margin' => [
                    'bottom' => 8,
                    'left' => 8,
                    'right' => 8,
                    'top' => 10
                ],
                'orientation' => 'portrait',
                'download' => false,
                'encoding' => 'UTF-8'
            ]);

            $this->render('clm_worksheet'.(($format != null) ? '_'.$format : ''), 'wkhtml');

        }

        //debug($meetingMidweek);
        //debug(json_encode($meetingMidweek, JSON_PRETTY_PRINT));
        //die();


    }


    public function editTreasuresTalk($id = null) {
        $meetingMidweek = $this->MeetingMidweeks->get($id, [
            'fields' => ['id', 'meeting_id', 'treasures_talk_individual_id'],
            'contain' => ['TreasuresTalkIndividual' => ['fields' => ['id', 'first_name', 'last_name']]]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $meetingMidweek = $this->MeetingMidweeks->patchEntity($meetingMidweek, $this->request->data, ['fieldList' => ['treasures_talk_individual_id']]);
            if ($this->MeetingMidweeks->save($meetingMidweek)) {
                $this->Flash->success(__('The Talk assignment is saved.'));
                return $this->redirect(['controller' => 'meetings', 'action' => 'view', $meetingMidweek->meeting_id]);
            } else {
                $this->Flash->error(__('The Talk assignment could not be saved. Please, try again.'));
            }
        }

        $treasuresTalkIndividuals = $this->MeetingMidweeks->TreasuresTalkIndividual->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 2])
            ->order(['TreasuresTalkIndividual.last_name' => 'ASC', 'TreasuresTalkIndividual.first_name' => 'ASC'])
            ->toArray();

        $this->set(compact('meetingMidweek', 'treasuresTalkIndividuals'));

    }


    public function editTreasuresGems($id = null) {
        $meetingMidweek = $this->MeetingMidweeks->get($id, [
            'fields' => ['id', 'meeting_id', 'treasures_gems_individual_id'],
            'contain' => ['TreasuresGemsIndividual' => ['fields' => ['id', 'first_name', 'last_name']]]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $meetingMidweek = $this->MeetingMidweeks->patchEntity($meetingMidweek, $this->request->data, ['fieldList' => ['treasures_gems_individual_id']]);
            if ($this->MeetingMidweeks->save($meetingMidweek)) {
                $this->Flash->success(__('The Spiritual Gems assignment is saved.'));
                return $this->redirect(['controller' => 'meetings', 'action' => 'view', $meetingMidweek->meeting_id]);
            } else {
                $this->Flash->error(__('The Spiritual Gems assignment could not be saved. Please, try again.'));
            }
        }

        $treasuresGemsIndividuals = $this->MeetingMidweeks->TreasuresGemsIndividual->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 3])
            ->order(['TreasuresGemsIndividual.last_name' => 'ASC', 'TreasuresGemsIndividual.first_name' => 'ASC'])
            ->toArray();

        $this->set(compact('meetingMidweek', 'treasuresGemsIndividuals'));

    }


    public function editTreasuresReading($id = null) {

        $meetingMidweek = $this->MeetingMidweeks->get($id, [
            'fields' => ['id', 'meeting_id', 'treasures_reading_individual_id', 'treasures_reading_study_id', 'treasures_reading_study_notes'],
            'contain' => [
                'TreasuresReadingIndividual' => ['fields' => ['id', 'first_name', 'last_name']],
                'Week' => ['fields' => ['id', 'date_monday']]
            ]
        ]);

        $individualId = $meetingMidweek->treasures_reading_individual_id;

        if ($this->request->is(['patch', 'post', 'put'])) {
            $meetingMidweek = $this->MeetingMidweeks->patchEntity($meetingMidweek, $this->request->data, ['fieldList' => ['treasures_reading_individual_id', 'treasures_reading_study_id', 'treasures_reading_study_notes']]);
            if ($this->MeetingMidweeks->save($meetingMidweek)) {
                $this->Flash->success(__('The meeting midweek has been saved.'));
                return $this->redirect(['controller' => 'meetings', 'action' => 'view', $meetingMidweek->meeting_id]);
            } else {
                $this->Flash->error(__('The meeting midweek could not be saved. Please, try again.'));
            }
        }

        $treasuresReadingIndividuals = $this->MeetingMidweeks->TreasuresReadingIndividual->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 4])
            ->toArray()
            ;

//            debug(json_encode($meetingMidweek, JSON_PRETTY_PRINT));

        if(isset($meetingMidweek->treasures_reading_individual->id)) {
            $meetingMidweeks = $this->MeetingMidweeks->find()
                ->where([sprintf('treasures_reading_individual_id = %d', $meetingMidweek->treasures_reading_individual->id)])
                ->orWhere([sprintf('ministry_item1_student_id = %d', $meetingMidweek->treasures_reading_individual->id)])
                ->orWhere([sprintf('ministry_item2_student_id = %d', $meetingMidweek->treasures_reading_individual->id)])
                ->orWhere([sprintf('ministry_item3_student_id = %d', $meetingMidweek->treasures_reading_individual->id)])
                ->contain(['TreasuresReadingStudy' => function($q) {
                    return $q->where(['is_reading' => 1]);
                }])
                ->contain(['Week'])
                ;
//        debug(json_encode($meetingMidweeks, JSON_PRETTY_PRINT));
            $this->set(compact('meetingMidweeks'));
        }


        /*
         * SELECT DISTINCT i.id, i.first_name, i.last_name, w.date_monday
FROM individuals i
INNER JOIN meeting_midweeks mm ON mm.treasures_reading_individual_id = i.id
INNER JOIN weeks w ON w.id = mm.week_id
ORDER BY w.date_monday DESC
         */

        $treasuresReadingStudies = $this->MeetingMidweeks->TreasuresReadingStudy->find('list', ['keyField' => id, 'valueField' => 'full_name'])
			->where(['is_reading' => 1]);


        $this->set(compact('meetingMidweek', 'treasuresReadingIndividuals', 'individualId', 'treasuresReadingStudies'));

    }


    public function editMinistryItem1($id = null) {

        $meetingMidweek = $this->MeetingMidweeks->get($id, [
            'fields' => ['id', 'meeting_id', 'ministry_item1_student_id', 'ministry_item1_study_id', 'ministry_item1_study_notes', 'ministry_item1_assistant_id'],
            'contain' => [
                'MinistryItem1Student' => ['fields' => ['id', 'first_name', 'last_name']],
                'MinistryItem1Assistant' => ['fields' => ['id', 'first_name', 'last_name']],
                'Week' => ['fields' => ['id', 'date_monday']]
            ]
        ]);

        $individualId = $meetingMidweek->ministry_item1_student_id;
        $assistantId = $meetingMidweek->ministry_item1_student_id;

        if ($this->request->is(['patch', 'post', 'put'])) {
            $meetingMidweek = $this->MeetingMidweeks->patchEntity($meetingMidweek, $this->request->data, ['fieldList' => ['ministry_item1_student_id', 'ministry_item1_study_id', 'ministry_item1_study_notes', 'ministry_item1_assistant_id']]);
            if ($this->MeetingMidweeks->save($meetingMidweek)) {
                $this->Flash->success(__('The meeting midweek has been saved.'));
                return $this->redirect(['controller' => 'meetings', 'action' => 'view', $meetingMidweek->meeting_id]);
            } else {
                $this->Flash->error(__('The meeting midweek could not be saved. Please, try again.'));
            }
        }

        $ministryItem1Students = $this->MeetingMidweeks->MinistryItem1Student->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 6])
            ->order(['MinistryItem1Student.last_name' => 'ASC', 'MinistryItem1Student.first_name' => 'ASC'])
            ->toArray();

        $ministryItem1Assistants = $this->MeetingMidweeks->MinistryItem1Assistant->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 9])
            ->order(['MinistryItem1Assistant.last_name' => 'ASC', 'MinistryItem1Assistant.first_name' => 'ASC'])
            ->toArray();

        $ministryItem1Studies = $this->MeetingMidweeks->MinistryItem1Study->find('list', ['keyField' => id, 'valueField' => 'full_name'])
            ->where(['is_demonstration' => 1]);

        $this->set(compact('meetingMidweek', 'ministryItem1Students', 'ministryItem1Assistants', 'individualId', 'assistantId', 'ministryItem1Studies'));

        if(isset($meetingMidweek->ministry_item1_student->id)) {
            //  talks by student
            $meetingMidweeks = $this->MeetingMidweeks->find()
                ->where([sprintf('ministry_item1_student_id = %d', $meetingMidweek->ministry_item1_student->id)])
                ->orWhere([sprintf('ministry_item1_student_id = %d', $meetingMidweek->ministry_item1_student->id)])
                ->orWhere([sprintf('ministry_item1_student_id = %d', $meetingMidweek->ministry_item1_student->id)])
                ->orWhere([sprintf('ministry_item1_student_id = %d', $meetingMidweek->ministry_item1_student->id)])
                ->contain(['MinistryItem1Study' => function($q) {
                    return $q->where(['is_demonstration' => 1]);
                }])
                ->contain(['Week'])
                ;
//        debug(json_encode($meetingMidweeks, JSON_PRETTY_PRINT));
            $this->set(compact('meetingMidweeks'));
        }

    }


    public function editMinistryItem2($id = null) {

        $meetingMidweek = $this->MeetingMidweeks->get($id, [
            'fields' => ['id', 'meeting_id', 'ministry_item2_student_id', 'ministry_item2_study_id', 'ministry_item2_study_notes', 'ministry_item2_assistant_id'],
            'contain' => [
                'MinistryItem2Student' => ['fields' => ['id', 'first_name', 'last_name']],
                'MinistryItem2Assistant' => ['fields' => ['id', 'first_name', 'last_name']],
                'Week' => ['fields' => ['id', 'date_monday']]
            ]
        ]);

        $individualId = $meetingMidweek->ministry_item2_student_id;
        $assistantId = $meetingMidweek->ministry_item2_student_id;

        if ($this->request->is(['patch', 'post', 'put'])) {
            $meetingMidweek = $this->MeetingMidweeks->patchEntity($meetingMidweek, $this->request->data, ['fieldList' => ['ministry_item2_student_id', 'ministry_item2_study_id', 'ministry_item2_study_notes', 'ministry_item2_assistant_id']]);
            if ($this->MeetingMidweeks->save($meetingMidweek)) {
                $this->Flash->success(__('The meeting midweek has been saved.'));
                return $this->redirect(['controller' => 'meetings', 'action' => 'view', $meetingMidweek->meeting_id]);
            } else {
                $this->Flash->error(__('The meeting midweek could not be saved. Please, try again.'));
            }
        }

        $ministryItem2Students = $this->MeetingMidweeks->MinistryItem2Student->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 6])
            ->order(['MinistryItem2Student.last_name' => 'ASC', 'MinistryItem2Student.first_name' => 'ASC'])
            ->toArray();

        $ministryItem2Assistants = $this->MeetingMidweeks->MinistryItem2Assistant->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 9])
            ->order(['MinistryItem2Assistant.last_name' => 'ASC', 'MinistryItem2Assistant.first_name' => 'ASC'])
            ->toArray();

        $ministryItem2Studies = $this->MeetingMidweeks->MinistryItem2Study->find('list', ['keyField' => id, 'valueField' => 'full_name'])
            ->where(['is_demonstration' => 1]);

        $this->set(compact('meetingMidweek', 'ministryItem2Students', 'ministryItem2Assistants', 'individualId', 'assistantId', 'ministryItem2Studies'));

    }


    public function editMinistryItem3($id = null) {

        $meetingMidweek = $this->MeetingMidweeks->get($id, [
            'fields' => ['id', 'meeting_id', 'ministry_item3_student_id', 'ministry_item3_study_id', 'ministry_item3_study_notes', 'ministry_item3_assistant_id'],
            'contain' => [
                'MinistryItem3Student' => ['fields' => ['id', 'first_name', 'last_name']],
                'MinistryItem3Assistant' => ['fields' => ['id', 'first_name', 'last_name']],
                'Week' => ['fields' => ['id', 'date_monday']]
            ]
        ]);

        $individualId = $meetingMidweek->ministry_item3_student_id;
        $assistantId = $meetingMidweek->ministry_item3_student_id;

        if ($this->request->is(['patch', 'post', 'put'])) {
            $meetingMidweek = $this->MeetingMidweeks->patchEntity($meetingMidweek, $this->request->data, ['fieldList' => ['ministry_item3_student_id', 'ministry_item3_study_id', 'ministry_item3_study_notes', 'ministry_item3_assistant_id']]);
            if ($this->MeetingMidweeks->save($meetingMidweek)) {
                $this->Flash->success(__('The meeting midweek has been saved.'));
                return $this->redirect(['controller' => 'meetings', 'action' => 'view', $meetingMidweek->meeting_id]);
            } else {
                $this->Flash->error(__('The meeting midweek could not be saved. Please, try again.'));
            }
        }

        $ministryItem3Students = $this->MeetingMidweeks->MinistryItem3Student->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 6])
            ->order(['MinistryItem3Student.last_name' => 'ASC', 'MinistryItem3Student.first_name' => 'ASC'])
            ->toArray();

        $ministryItem3Assistants = $this->MeetingMidweeks->MinistryItem3Assistant->find('list')
            ->matching('Congregation')->where(['Congregation.installation_id' => $this->request->session()->read('installation')->id])
            ->matching('Privileges')->where(['IndividualsPrivileges.privilege_id' => 9])
            ->order(['MinistryItem3Assistant.last_name' => 'ASC', 'MinistryItem3Assistant.first_name' => 'ASC'])
            ->toArray();

        $ministryItem3Studies = $this->MeetingMidweeks->MinistryItem3Study->find('list', ['keyField' => 'id', 'valueField' => 'full_name'])
            ->where(['is_demonstration' => 1]);

        $this->set(compact('meetingMidweek', 'ministryItem3Students', 'ministryItem3Assistants', 'individualId', 'assistantId', 'ministryItem3Studies'));

    }


    public function aByIndividual()
    {

        $individualId = $this->request->data('individualId');

        $meetingMidweeks = $this->MeetingMidweeks->find()
            ->where([sprintf('treasures_reading_individual_id = %d', $individualId)])
            ->orWhere([sprintf('ministry_item1_student_id = %d', $individualId)])
            ->orWhere([sprintf('ministry_item2_student_id = %d', $individualId)])
            ->orWhere([sprintf('ministry_item3_student_id = %d', $individualId)])
            ->contain(['TreasuresReadingStudy'])
            ->contain(['Week' => ['fields' => ['id', 'date_monday']]])
            ;

//        debug(json_encode($meetingMidweeks, JSON_PRETTY_PRINT));
//        die();
        $this->set(compact('meetingMidweeks', 'individualId'));

        $this->viewBuilder()->layout('ajax');

    }

}
