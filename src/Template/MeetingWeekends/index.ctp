<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Meeting Weekend'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Meetings'), ['controller' => 'Meetings', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Meeting'), ['controller' => 'Meetings', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Congregations'), ['controller' => 'Congregations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Congregation'), ['controller' => 'Congregations', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="meetingWeekends index large-9 medium-8 columns content">
    <h3><?= __('Meeting Weekends') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('meeting_id') ?></th>
                <th><?= $this->Paginator->sort('time_meeting') ?></th>
                <th><?= $this->Paginator->sort('chairman_id') ?></th>
                <th><?= $this->Paginator->sort('speaker_id') ?></th>
                <th><?= $this->Paginator->sort('speaker_name') ?></th>
                <th><?= $this->Paginator->sort('congregation_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($meetingWeekends as $meetingWeekend): ?>
            <tr>
                <td><?= $this->Number->format($meetingWeekend->id) ?></td>
                <td><?= $meetingWeekend->has('meeting') ? $this->Html->link($meetingWeekend->meeting->id, ['controller' => 'Meetings', 'action' => 'view', $meetingWeekend->meeting->id]) : '' ?></td>
                <td><?= h($meetingWeekend->time_meeting) ?></td>
                <td><?= $this->Number->format($meetingWeekend->chairman_id) ?></td>
                <td><?= $this->Number->format($meetingWeekend->speaker_id) ?></td>
                <td><?= h($meetingWeekend->speaker_name) ?></td>
                <td><?= $meetingWeekend->has('congregation') ? $this->Html->link($meetingWeekend->congregation->name, ['controller' => 'Congregations', 'action' => 'view', $meetingWeekend->congregation->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $meetingWeekend->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $meetingWeekend->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $meetingWeekend->id], ['confirm' => __('Are you sure you want to delete # {0}?', $meetingWeekend->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
