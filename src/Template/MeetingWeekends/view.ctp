<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Meeting Weekend'), ['action' => 'edit', $meetingWeekend->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Meeting Weekend'), ['action' => 'delete', $meetingWeekend->id], ['confirm' => __('Are you sure you want to delete # {0}?', $meetingWeekend->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Meeting Weekends'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Meeting Weekend'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Meetings'), ['controller' => 'Meetings', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Meeting'), ['controller' => 'Meetings', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Congregations'), ['controller' => 'Congregations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Congregation'), ['controller' => 'Congregations', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="meetingWeekends view large-9 medium-8 columns content">
    <h3><?= h($meetingWeekend->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Meeting') ?></th>
            <td><?= $meetingWeekend->has('meeting') ? $this->Html->link($meetingWeekend->meeting->id, ['controller' => 'Meetings', 'action' => 'view', $meetingWeekend->meeting->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Speaker Name') ?></th>
            <td><?= h($meetingWeekend->speaker_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Congregation') ?></th>
            <td><?= $meetingWeekend->has('congregation') ? $this->Html->link($meetingWeekend->congregation->name, ['controller' => 'Congregations', 'action' => 'view', $meetingWeekend->congregation->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Congregation Name') ?></th>
            <td><?= h($meetingWeekend->congregation_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($meetingWeekend->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Chairman Id') ?></th>
            <td><?= $this->Number->format($meetingWeekend->chairman_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Speaker Id') ?></th>
            <td><?= $this->Number->format($meetingWeekend->speaker_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Middle Song Number') ?></th>
            <td><?= $this->Number->format($meetingWeekend->middle_song_number) ?></td>
        </tr>
        <tr>
            <th><?= __('Wt Reader Id') ?></th>
            <td><?= $this->Number->format($meetingWeekend->wt_reader_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Closing Song Number') ?></th>
            <td><?= $this->Number->format($meetingWeekend->closing_song_number) ?></td>
        </tr>
        <tr>
            <th><?= __('Time Meeting') ?></th>
            <td><?= h($meetingWeekend->time_meeting) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($meetingWeekend->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($meetingWeekend->modified) ?></td>
        </tr>
    </table>
</div>
