<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Meeting Weekends'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Meetings'), ['controller' => 'Meetings', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Meeting'), ['controller' => 'Meetings', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Congregations'), ['controller' => 'Congregations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Congregation'), ['controller' => 'Congregations', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="meetingWeekends form large-9 medium-8 columns content">
    <?= $this->Form->create($meetingWeekend) ?>
    <fieldset>
        <legend><?= __('Add Meeting Weekend') ?></legend>
        <?php
            echo $this->Form->input('meeting_id', ['options' => $meetings, 'empty' => true]);
            echo $this->Form->input('time_meeting');
            echo $this->Form->input('chairman_id');
            echo $this->Form->input('speaker_id');
            echo $this->Form->input('speaker_name');
            echo $this->Form->input('congregation_id', ['options' => $congregations, 'empty' => true]);
            echo $this->Form->input('congregation_name');
            echo $this->Form->input('middle_song_number');
            echo $this->Form->input('wt_reader_id');
            echo $this->Form->input('closing_song_number');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
