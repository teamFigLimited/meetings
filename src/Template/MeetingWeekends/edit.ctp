<div class="panel panel-weekhead">
    <div class="panel-heading" style="background: repeating-linear-gradient(45deg, #<?= $meetingWeekend->meeting->week->month->colour1 ?>, #<?= $meetingWeekend->meeting->week->month->colour1 ?> 4px, #<?= $meetingWeekend->meeting->week->month->colour2 ?> 4px, #<?= $meetingWeekend->meeting->week->month->colour2 ?> 6px);">
        <div class="panel-title">Weekend Meeting</div>
    </div>
</div>

<?= $this->Form->create($meetingWeekend) ?>
<div class="row">
    <div class="col-xs-12 col-sm-4">
        <div class="panel panel-default">
            <ul class="list-group">
                <li class="list-group-item">
                    <b>Chairman</b>
                    <?= $this->Form->input('chairman_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>                
                </li>
            </ul>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4">
        <div class="panel panel-default">
            <ul class="list-group">
                <li class="list-group-item">
                    <b>Middle Song</b>
                    <?= $this->Form->input('middle_song_number', ['label' => false, 'empty' => true, 'class' => 'form-control', 'options' => $songs]) ?>                
                </li>
            </ul>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4">
        <div class="panel panel-default">
            <ul class="list-group">
                <li class="list-group-item">
                    <b>Closing Song</b>
                    <?= $this->Form->input('closing_song_number', ['label' => false, 'empty' => true, 'class' => 'form-control', 'options' => $songs]) ?>                
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h4 class="panel-title">Public Discourse</h4>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <b>Speaker Congregation</b>
                    <?= $this->Form->input('congregation_id', ['label' => false, 'empty' => true, 'class' => 'form-control congregation-id-trigger-true']) ?>
                </li>
                <li class="list-group-item">
                    <b>Speaker</b>
                    <?= $this->Form->input('speaker_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">Watchtower<sub>&reg;</sub></h4>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <b>Reader</b>
                    <?= $this->Form->input('reader_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>
                </li>
            </ul>
        </div>
    </div>
</div>
<?= $this->Form->button(__('Save Changes'), ['class' => 'form-control btn btn-success']) ?>
<?= $this->Form->end() ?>


<?php
if($debug):
    debug(json_encode($meetingWeekend, JSON_PRETTY_PRINT));
endif;
?>



<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $meetingWeekend->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $meetingWeekend->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Meeting Weekends'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Meetings'), ['controller' => 'Meetings', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Meeting'), ['controller' => 'Meetings', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Congregations'), ['controller' => 'Congregations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Congregation'), ['controller' => 'Congregations', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="meetingWeekends form large-9 medium-8 columns content">
    <?= $this->Form->create($meetingWeekend) ?>
    <fieldset>
        <legend><?= __('Edit Meeting Weekend') ?></legend>
        <?php
            echo $this->Form->input('meeting_id', ['options' => $meetings, 'empty' => true]);
            echo $this->Form->input('time_meeting');
            echo $this->Form->input('chairman_id');
            echo $this->Form->input('speaker_id');
            echo $this->Form->input('speaker_name');
            echo $this->Form->input('congregation_id', ['options' => $congregations, 'empty' => true]);
            echo $this->Form->input('congregation_name');
            echo $this->Form->input('middle_song_number');
            echo $this->Form->input('wt_reader_id');
            echo $this->Form->input('closing_song_number');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
