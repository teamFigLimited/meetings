<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Week Living'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Weeks'), ['controller' => 'Weeks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Week'), ['controller' => 'Weeks', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="weekLivings index large-9 medium-8 columns content">
    <h3><?= __('Week Livings') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('week_id') ?></th>
                <th><?= $this->Paginator->sort('theme') ?></th>
                <th><?= $this->Paginator->sort('material') ?></th>
                <th><?= $this->Paginator->sort('minutes') ?></th>
                <th><?= $this->Paginator->sort('sort') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($weekLivings as $weekLiving): ?>
            <tr>
                <td><?= $this->Number->format($weekLiving->id) ?></td>
                <td><?= $weekLiving->has('week') ? $this->Html->link($weekLiving->week->id, ['controller' => 'Weeks', 'action' => 'view', $weekLiving->week->id]) : '' ?></td>
                <td><?= h($weekLiving->theme) ?></td>
                <td><?= h($weekLiving->material) ?></td>
                <td><?= $this->Number->format($weekLiving->minutes) ?></td>
                <td><?= $this->Number->format($weekLiving->sort) ?></td>
                <td><?= h($weekLiving->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $weekLiving->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $weekLiving->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $weekLiving->id], ['confirm' => __('Are you sure you want to delete # {0}?', $weekLiving->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
