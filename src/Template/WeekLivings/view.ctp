<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Week Living'), ['action' => 'edit', $weekLiving->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Week Living'), ['action' => 'delete', $weekLiving->id], ['confirm' => __('Are you sure you want to delete # {0}?', $weekLiving->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Week Livings'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Week Living'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Weeks'), ['controller' => 'Weeks', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Week'), ['controller' => 'Weeks', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="weekLivings view large-9 medium-8 columns content">
    <h3><?= h($weekLiving->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Week') ?></th>
            <td><?= $weekLiving->has('week') ? $this->Html->link($weekLiving->week->id, ['controller' => 'Weeks', 'action' => 'view', $weekLiving->week->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Theme') ?></th>
            <td><?= h($weekLiving->theme) ?></td>
        </tr>
        <tr>
            <th><?= __('Material') ?></th>
            <td><?= h($weekLiving->material) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($weekLiving->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Minutes') ?></th>
            <td><?= $this->Number->format($weekLiving->minutes) ?></td>
        </tr>
        <tr>
            <th><?= __('Sort') ?></th>
            <td><?= $this->Number->format($weekLiving->sort) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($weekLiving->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($weekLiving->modified) ?></tr>
        </tr>
    </table>
</div>
