<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $weekLiving->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $weekLiving->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Week Livings'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Weeks'), ['controller' => 'Weeks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Week'), ['controller' => 'Weeks', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="weekLivings form large-9 medium-8 columns content">
    <?= $this->Form->create($weekLiving) ?>
    <fieldset>
        <legend><?= __('Edit Week Living') ?></legend>
        <?php
            echo $this->Form->input('week_id', ['options' => $weeks, 'empty' => true]);
            echo $this->Form->input('theme');
            echo $this->Form->input('material');
            echo $this->Form->input('minutes');
            echo $this->Form->input('sort');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
