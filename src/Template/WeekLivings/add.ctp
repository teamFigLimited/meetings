<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Week Livings'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Weeks'), ['controller' => 'Weeks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Week'), ['controller' => 'Weeks', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="weekLivings form large-9 medium-8 columns content">
    <?= $this->Form->create($weekLiving) ?>
    <fieldset>
        <legend><?= __('Add Week Living') ?></legend>
        <?php
            echo $this->Form->input('theme');
            echo $this->Form->textarea('material');
            echo $this->Form->input('minutes');
            echo $this->Form->input('sort');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
