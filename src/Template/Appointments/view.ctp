<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Appointment'), ['action' => 'edit', $appointment->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Appointment'), ['action' => 'delete', $appointment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $appointment->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Appointments'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Appointment'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Individuals'), ['controller' => 'Individuals', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Individual'), ['controller' => 'Individuals', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="appointments view large-9 medium-8 columns content">
    <h3><?= h($appointment->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($appointment->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Abbrev') ?></th>
            <td><?= h($appointment->abbrev) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($appointment->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Ministerial Servant') ?></th>
            <td><?= $this->Number->format($appointment->is_ministerial_servant) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Elder') ?></th>
            <td><?= $this->Number->format($appointment->is_elder) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($appointment->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($appointment->modified) ?></tr>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Individuals') ?></h4>
        <?php if (!empty($appointment->individuals)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Congregation Id') ?></th>
                <th><?= __('Is Unbaptised Publisher') ?></th>
                <th><?= __('Is Baptised') ?></th>
                <th><?= __('Is Bro Sis') ?></th>
                <th><?= __('First Name') ?></th>
                <th><?= __('Last Name') ?></th>
                <th><?= __('Gender Id') ?></th>
                <th><?= __('Appointment Id') ?></th>
                <th><?= __('Family Head Id') ?></th>
                <th><?= __('Is Clm Enrolled') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($appointment->individuals as $individuals): ?>
            <tr>
                <td><?= h($individuals->id) ?></td>
                <td><?= h($individuals->congregation_id) ?></td>
                <td><?= h($individuals->is_unbaptised_publisher) ?></td>
                <td><?= h($individuals->is_baptised) ?></td>
                <td><?= h($individuals->is_bro_sis) ?></td>
                <td><?= h($individuals->first_name) ?></td>
                <td><?= h($individuals->last_name) ?></td>
                <td><?= h($individuals->gender_id) ?></td>
                <td><?= h($individuals->appointment_id) ?></td>
                <td><?= h($individuals->family_head_id) ?></td>
                <td><?= h($individuals->is_clm_enrolled) ?></td>
                <td><?= h($individuals->created) ?></td>
                <td><?= h($individuals->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Individuals', 'action' => 'view', $individuals->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Individuals', 'action' => 'edit', $individuals->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Individuals', 'action' => 'delete', $individuals->id], ['confirm' => __('Are you sure you want to delete # {0}?', $individuals->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
