<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Meeting Event'), ['action' => 'edit', $meetingEvent->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Meeting Event'), ['action' => 'delete', $meetingEvent->id], ['confirm' => __('Are you sure you want to delete # {0}?', $meetingEvent->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Meeting Events'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Meeting Event'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="meetingEvents view large-9 medium-8 columns content">
    <h3><?= h($meetingEvent->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($meetingEvent->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($meetingEvent->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Co') ?></th>
            <td><?= $this->Number->format($meetingEvent->is_co) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Assembly') ?></th>
            <td><?= $this->Number->format($meetingEvent->is_assembly) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Convention') ?></th>
            <td><?= $this->Number->format($meetingEvent->is_convention) ?></td>
        </tr>
    </table>
</div>
