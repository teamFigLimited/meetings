<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Meeting Event'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="meetingEvents index large-9 medium-8 columns content">
    <h3><?= __('Meeting Events') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('is_co') ?></th>
                <th><?= $this->Paginator->sort('is_assembly') ?></th>
                <th><?= $this->Paginator->sort('is_convention') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($meetingEvents as $meetingEvent): ?>
            <tr>
                <td><?= $this->Number->format($meetingEvent->id) ?></td>
                <td><?= h($meetingEvent->name) ?></td>
                <td><?= $this->Number->format($meetingEvent->is_co) ?></td>
                <td><?= $this->Number->format($meetingEvent->is_assembly) ?></td>
                <td><?= $this->Number->format($meetingEvent->is_convention) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $meetingEvent->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $meetingEvent->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $meetingEvent->id], ['confirm' => __('Are you sure you want to delete # {0}?', $meetingEvent->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
