<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Meeting Events'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="meetingEvents form large-9 medium-8 columns content">
    <?= $this->Form->create($meetingEvent) ?>
    <fieldset>
        <legend><?= __('Add Meeting Event') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('is_co');
            echo $this->Form->input('is_assembly');
            echo $this->Form->input('is_convention');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
