<div class="col-xs-6 slip">
  <div class="heading">OUR CHRISTIAN LIFE AND MINISTRY MEETING ASSIGNMENT</div>
  <div class="slip-row rule name"><b>Name: <?= $name ?></b></div>
  <div class="slip-row rule assistant"><b>Assistant: <?= $assistant ?></b></div>
  <div class="slip-row rule date-monday"><b>Date: w/c <?= $date ?></b></div>
  <div class="slip-row rule counsel-point"><b>Counsel point: <?= $counselPoint ?></b></div>
  <div class="slip-row row">
    <div class="col-xs-6">
      <b>Assignment:</b><br>
      &square;<?= ($assignment == 0) ? '<span style="position:relative;left:-10px;">&check; Bible reading</span>' : 'Bible reading' ?><br>
      &square;<?= ($assignment == 1) ? '<span style="position:relative;left:-10px;">&check; Initial call</span>' : 'Initial call' ?><br>
      &square;<?= ($assignment == 2) ? '<span style="position:relative;left:-10px;">&check; Return visit</span>' : 'Return visit' ?><br>
      &square;<?= ($assignment == 3) ? '<span style="position:relative;left:-10px;">&check; Bible study</span>' : 'Bible study' ?><br>
    </div>
    <div class="col-xs-6">
      <b>To be given in:</b><br>
      &square;<?= ($school == 1) ? '<span style="position:relative;left:-10px;">&check; Main hall</span>' : 'Main hall' ?><br>
      &square;<?= ($school == 2) ? '<span style="position:relative;left:-10px;">&check; Auxiliary classroom 1</span>' : 'Auxiliary classroom 1' ?><br>
      &square;<?= ($school == 3) ? '<span style="position:relative;left:-10px;">&check; Auxiliary classroom 2</span>' : 'Auxiliary classroom 2' ?><br>
    </div>
  </div>
  <div class="slip-row note">
    <b>Note to Student:</b>
    <p>The source material for your assignment can be found in the Life and Ministry Workbook. Please work on the counsel point noted above, which is discussed in the Ministry School book. You should bring your book with you to the Life and Ministry Meeting.</p>
  </div>
</div>
