{% for mW in meetingMidweeks %}
<tr>
    <td>{{ mW.week.date_monday|date("d-M-Y") }}</td>
    <td>{% if mW.treasures_reading_individual_id == individualId %}Y{% endif %}</td>
    <td>{% if mW.ministry_item1_student_id == individualId %}Y{% endif %}</td>
    <td>{% if mW.ministry_item2_student_id == individualId %}Y{% endif %}</td>
    <td>{% if mW.ministry_item3_student_id == individualId %}Y{% endif %}</td>
    <td>
        {% if mW.treasures_reading_individual_id == individualId %}{{ mW.treasures_reading_study_id }}:{{ mW.treasures_reading_study.name }}{% endif %}
        {% if mW.ministry_item1_student_id == individualId %}{{ mW.ministry_item1_study_id }}: {{ mW.ministry_item1_study.name }}{% endif %}
        {% if mW.ministry_item2_student_id == individualId %}{{ mW.ministry_item2_study_id }}: {{ mW.ministry_item2_study.name }}{% endif %}
        {% if mW.ministry_item3_student_id == individualId %}{{ mW.ministry_item3_study_id }}: {{ mW.ministry_item3_study.name }}{% endif %}
    </td>
    <td>
        {% if mW.treasures_reading_individual_id == individualId %}{{ mW.treasures_reading_study_notes }}{% endif %}
        {% if mW.ministry_item1_student_id == individualId %}{{ mW.ministry_item1_study_notes }}{% endif %}
        {% if mW.ministry_item2_student_id == individualId %}{{ mW.ministry_item2_study_notes }}{% endif %}
        {% if mW.ministry_item3_student_id == individualId %}{{ mW.ministry_item3_study_notes }}{% endif %}
    </td>
</tr>
{% endfor %}