<div class="page-header">
    <h1>Meeting Week edit <small class="text-uppercase"><?= $meeting->week->full_date ?></small></h1>
</div>

<?= $this->Form->create($meeting) ?>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Week Options</h4>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <?= $this->Form->input('meeting_week_type_id', ['class' => 'form-control']) ?>
                </li>
            </ul>
        </div>
    </div>
</div>
<?= $this->Form->button(__('Save Changes'), ['class' => 'btn btn-success']) ?>


<?php
if($debug):
    debug(json_encode($meeting, JSON_PRETTY_PRINT));
endif;
?>

