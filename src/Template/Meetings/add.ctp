<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Meetings'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Installations'), ['controller' => 'Installations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Installation'), ['controller' => 'Installations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Weeks'), ['controller' => 'Weeks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Week'), ['controller' => 'Weeks', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Meeting Midweeks'), ['controller' => 'MeetingMidweeks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Meeting Midweek'), ['controller' => 'MeetingMidweeks', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Meeting Prayers'), ['controller' => 'MeetingPrayers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Meeting Prayer'), ['controller' => 'MeetingPrayers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Meeting Pts'), ['controller' => 'MeetingPts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Meeting Pt'), ['controller' => 'MeetingPts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Meeting Wts'), ['controller' => 'MeetingWts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Meeting Wt'), ['controller' => 'MeetingWts', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="meetings form large-9 medium-8 columns content">
    <?= $this->Form->create($meeting) ?>
    <fieldset>
        <legend><?= __('Add Meeting') ?></legend>
        <?php
            echo $this->Form->input('installation_id', ['options' => $installations, 'empty' => true]);
            echo $this->Form->input('week_id', ['options' => $weeks, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
