<div class="page-header">
    <h2>Meeting Schedule <small class="text-uppercase">and page options</small></h2>
</div>


<div class="btn-group page-options" role="group" aria-label="page options">
    
    <div class="btn-group" role="group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Months
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <?php foreach($months as $month): ?>
            <li<?= ($month->id == $monthId) ? ' class="active"' : '' ?>><?= $this->Html->link($month->date_month->format('F Y'), ['controller' => 'meetings', 'action' => 'index', '?' => ['month_id' => $month->id]]) ?></li>
            <?php endforeach; ?>
        </ul>
    </div>

    <div class="btn-group" role="group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Actions
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li><?= $this->Html->link('Update', ['controller' => 'meetings', 'action' => 'update']) ?></li>
        </ul>
    </div>
    
</div>



<?php foreach ($meetings as $meeting): ?>

<div class="well">
    <div class="h1 pull-right"><?= $this->Html->link('<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>', ['controller' => 'meetings', 'action'=> 'view', $meeting->id], ['escape' => false]) ?></div>
    <h1><?= $meeting->week->full_date ?></h1>
</div>

<!-- MIDWEEK -->
<?php if($meeting->meeting_midweek): ?>
<h3 class="h3">Our Christian Life and Ministry</h3>
<div class="panel panel-weekhead">
    <div class="panel-heading" style="background: repeating-linear-gradient(45deg, #<?= $meeting->week->month->colour1 ?>, #<?= $meeting->week->month->colour1 ?> 4px, #<?= $meeting->week->month->colour2 ?> 4px, #<?= $meeting->week->month->colour2 ?> 6px);"><h3 class="panel-title"><?= $meeting->week->full_date ?> | <?= $meeting->week->week_midweek->treasures_material ?></h3></div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6">

        <ul class="meetings treasures">
            <li class="bullet">
                <span class="pull-right"><?= (isset($meeting->meeting_midweek->opening_prayer)) ? '<span class="text-right text-success"><i>'.$meeting->meeting_midweek->opening_prayer->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
                Song <?= $meeting->meeting_midweek->opening_song_number ?> and Prayer
            </li>
            <li class="bullet">
                <span class="pull-right"><?= (isset($meeting->meeting_midweek->chairman)) ? '<span class="text-right text-success"><i>'.$meeting->meeting_midweek->chairman->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
                Opening Comments (3 min. or less)
            </li>
        </ul>

        <!-- TREASURES -->
        <div class="alert alert-treasures">TREASURES FROM GOD'S WORD</div>
        <ul class="meetings treasures">
            <li class="bullet">
                <span class="pull-right"><?= (isset($meeting->meeting_midweek->treasures_talk_individual)) ? '<span class="text-right text-success"><i>'.$meeting->meeting_midweek->treasures_talk_individual->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
                <b><?= $meeting->week->week_midweek->treasures_talk_theme ?></b>: (10 min.)
            </li>
            <li class="bullet">
                <span class="pull-right"><?= (isset($meeting->meeting_midweek->treasures_gems_individual)) ? '<span class="text-right text-success"><i>'.$meeting->meeting_midweek->treasures_gems_individual->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
                <b>Digging for Spiritual Gems</b>: (8 min.)
            </li>
            <li class="bullet">
                <span class="pull-right"><?= (isset($meeting->meeting_midweek->treasures_reading_individual)) ? '<span class="text-right text-success"><i>'.$meeting->meeting_midweek->treasures_reading_individual->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
                <b>Bible Reading:</b> <?= $meeting->week->treasures_reading_material ?> (4 min. or less)                <!-- TREASURES -->

            </li>
        </ul>

    </div>

    <div class="col-xs-12 col-sm-12 col-md-6">

        <!-- MINISTRY -->
        <div class="alert alert-ministry">APPLY YOURSELF TO THE FIELD MINISTRY</div>
        <ul class="meetings ministry">

            <?php if($meeting->meeting_midweek->is_week1 == 1): ?>
            <li class="bullet">
                <span class="pull-right"><?= (isset($meeting->meeting_midweek->ministry_presentation_individual)) ? '<span class="text-right text-success"><i>'.$meeting->meeting_midweek->ministry_presentation_individual->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
                <b>Prepare This Month’s Presentations</b>: (10 min.)
            </li>
            <?php else: ?>
            <li class="bullet">
                <span class="pull-right"><?= (isset($meeting->meeting_midweek->ministry_item1_student)) ? '<span class="text-right text-success"><i>'.$meeting->meeting_midweek->ministry_item1_student->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
                <b><?= $meeting->meeting_midweek->ministry_item1_type->name ?></b>: (2 min.)<br>
            </li>
            <li>
                <span class="pull-right"><?= (isset($meeting->meeting_midweek->ministry_item1_assistant)) ? '<span class="text-right text-warning"><i>'.$meeting->meeting_midweek->ministry_item1_assistant->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
            </li>
            <li class="bullet">
                <span class="pull-right"><?= (isset($meeting->meeting_midweek->ministry_item2_student)) ? '<span class="text-right text-success"><i>'.$meeting->meeting_midweek->ministry_item2_student->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
                <b><?= $meeting->meeting_midweek->ministry_item2_type->name ?></b>: (4 min.)<br>
            </li>
            <li>
                <span class="pull-right"><?= (isset($meeting->meeting_midweek->ministry_item2_assistant)) ? '<span class="text-right text-warning"><i>'.$meeting->meeting_midweek->ministry_item2_assistant->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
            </li>
            <li class="bullet">
                <span class="pull-right"><?= (isset($meeting->meeting_midweek->ministry_item3_student)) ? '<span class="text-right text-success"><i>'.$meeting->meeting_midweek->ministry_item3_student->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
                <b><?= $meeting->meeting_midweek->ministry_item3_type->name ?></b>: (6 min.)<br>
            </li>
            <li>
                <span class="pull-right"><?= (isset($meeting->meeting_midweek->ministry_item3_assistant)) ? '<span class="text-right text-warning"><i>'.$meeting->meeting_midweek->ministry_item3_assistant->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
            </li>
            <?php endif; ?>

        </ul>


        <!-- LIVING AS CHRISTIANS -->
        <div class="alert alert-living">LIVING AS CHRISTIANS</div>
        <ul class="meetings living">
            <li class="bullet">
                <span class="pull-right"><?= ($meeting->meeting_midweek->living_item1_individual) ? '<span class="text-right text-success"><i>'.$meeting->meeting_midweek->living_item1_individual->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
                <b><?= $meeting->meeting_midweek->living_item1_theme ?></b>: (<?= $meeting->meeting_midweek->living_item1_minutes ?> min.)
            </li>

            <?php if($meeting->meeting_midweek->living_item2_theme != null): ?>
            <li class="bullet">
                <span class="pull-right"><?= (isset($meeting->meeting_midweek->living_item2_individual)) ? '<span class="text-right text-success"><i>'.$meeting->meeting_midweek->living_item2_individual->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
                <b><?= $meeting->meeting_midweek->living_item2_theme ?></b>: (<?= $meeting->meeting_midweek->living_item2_minutes ?> min.)
            </li>
            <?php endif; ?>

            
            <?php if($meeting->meeting_week_type_id == 1): ?>
            <li class="bullet">
                <span class="pull-right"><?= (isset($meeting->meeting_midweek->living_cbs_conductor)) ? '<span class="text-right text-success"><i>'.$meeting->meeting_midweek->living_cbs_conductor->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
                <b>Congregation Bible Study</b>: (30 min.)
            </li>
            <li>
                <span class="pull-right">Reader: <?= (isset($meeting->meeting_midweek->living_cbs_reader)) ? '<span class="text-right text-warning"><i>'.$meeting->meeting_midweek->living_cbs_reader->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
            </li>
                
            <?php endif; ?>
            
        </ul>


        <ul class="meetings living">
            <li class="bullet">
                <span class="pull-right"><?= (isset($meeting->meeting_midweek->chairman)) ? '<span class="text-right text-success"><i>'.$meeting->meeting_midweek->chairman->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
                Review Followed by Preview of Next Week (3 min.)
            </li>
            
            <!-- CO -->
            <?php if($meeting->meeting_week_type_id == 2): ?>
            <li class="bullet">
                <span class="pull-right text-success"><i><?= $meeting->installation->circuit_overseer_name ?></i></span>
                <b><?= $meeting->meeting_midweek->co_talk_theme ?></b>
            </li>            
            <?php endif; ?>
            
            <li class="bullet">
                <span class="pull-right"><?= (isset($meeting->meeting_midweek->closing_prayer)) ? '<span class="text-right text-success"><i>'.$meeting->meeting_midweek->closing_prayer->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
                Song <?= ($meeting->meeting_week_type_id == 1) ? $meeting->meeting_midweek->closing_song_number : '' ?> and Prayer
            </li>
        </ul>


    </div>

</div>
<?php endif; ?>


<!-- WEEKEND -->
<?php if($meeting->meeting_weekend): ?>
<h3 class="h3">Public Talk &amp; Watchtower<sub><small>&reg;</small></sub> Study</h3>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h4 class="panel-title">Public Talk</h4>
            </div>
        </div>
        <ul class="meetings">

            <li class="bullet">
                <span class="pull-right"><?= (isset($meeting->meeting_weekend->chairman)) ? '<span class="text-right text-success"><i>'.$meeting->meeting_weekend->chairman->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
                <b>Chairman</b>
            </li>

            <?php
                $speakerName = '';
                $speakerTheme = '';
                $speakerCongregation = '';

                //  speaker name
                if($meeting->meeting_week_type_id == 2):
                    $speakerName = $meeting->installation->circuit_overseer_name;
                elseif($meeting->meeting_weekend->speaker):
                    $speakerName = $meeting->meeting_weekend->speaker->full_name;
                elseif($meeting->meeting_weekend->speaker_name != ''):
                    $speakerName = $meeting->meeting_weekend->speaker_name;
                else:
                    $speakerName = '';
                endif;

                //  theme
                if($meeting->meeting_weekend->outline):
                    $speakerTheme = $meeting->meeting_weekend->outline->name;
                elseif($meeting->meeting_weekend->theme != ''):
                    $speakerTheme = $meeting->meeting_weekend->theme;
                else:
                    $speakerTheme = '';
                endif;

                //  congregation
                if($meeting->meeting_week_type_id == 2):
                    $speakerCongregation = ' ';
                elseif($meeting->meeting_weekend->speaker_congregation):
                    $speakerCongregation = $meeting->meeting_weekend->speaker_congregation->name;
                elseif($meeting->meeting_weekend->congregation_name != ''):
                    $speakerCongregation = $meeting->meeting_weekend->congregation_name;
                else:
                    $speakerCongregation = '';
                endif;
            ?>
            <li class="bullet">
                <b><?= ($speakerTheme != '') ? $speakerTheme : '<span class="text-danger">no theme set</span>' ?></b>
            </li>
            <li>
                <span class="pull-right"><?= ($speakerName != '') ? '<span class="text-right text-success"><i>'.$speakerName.'</i></span>' : '<span class="text-right text-danger">no speaker set</span>' ?></span>
            </li>
            <li>
                <span class="pull-right"><?= ($speakerCongregation != '') ? '<span class="text-right text-warning"><i>'.$speakerCongregation.'</i></span>' : '<span class="text-right text-danger">no congregation set</span>' ?></span>                        
            </li>

        </ul>

    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">Watchtower<sub><small>&reg;</small></sub></h4>
            </div>
        </div>

        <ul class="meetings">
            <li class="bullet">
                Song <?= $meeting->meeting_weekend->middle_song_number ?>
            </li>					
            <li class="bullet">
                <b><?= $meeting->meeting_weekend->wt_theme ?></b>
            </li>
            
            <?php if($meeting->meeting_week_type_id == 1): ?>
            <li>
                <span class="pull-right">Reader <?= ($meeting->meeting_weekend->reader) ? '<span class="text-right text-success"><i>'.$meeting->meeting_weekend->reader->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
                &nbsp;
            </li>
            
            <?php elseif($meeting->meeting_week_type_id == 2): ?>
            <li class="bullet">
                <span class="pull-right text-success"><i><?= $meeting->installation->circuit_overseer_name ?></i></span>
                <b><?= $meeting->meeting_weekend->co_talk_theme ?></b>
            </li>
            <?php endif; ?>
            
            <li class="bullet">
                Song <?= $meeting->meeting_weekend->closing_song_number ?> and Prayer
            </li>					
        </ul>

    </div>
</div>
<?php endif; ?>




<!-- OUTGOING TALKS -->
<div class="panel panel-info">
    <div class="panel-heading">
        <div class="panel-title">Outgoing Public Talks</div>
    </div>
    
    <?php if($meeting->meeting_outgoing_talks): ?>    
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Speaker</th>
                <th>Outline</th>
                <th>Congregation</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($meeting->meeting_outgoing_talks as $outgoing): ?>
            <tr>
                <td><?= $outgoing->speaker->full_name ?></td>
                <td><?= $outgoing->outline->name ?></td>
                <td><?= $outgoing->congregation->name ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else: ?>
    <div class="panel-body">
        <p>There are no scheduled outgoing talks.</p>
    </div>
    <?php endif; ?>
    
</div>







<div class="panel panel-grey">
    <div class="panel-heading">
        <h4 class="panel-title">&nbsp;</h4>
    </div>
</div>
<br>        
<?php endforeach; ?>



<?php
if($debug):
    debug(json_encode($meetings, JSON_PRETTY_PRINT));
    debug($meetings);
endif;
?>

