<div class="page-header">
    <div class="pull-right"><?= $this->Html->link('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', ['controller' => 'meetings', 'action'=> 'edit', $meeting->id], ['escape' => false, 'style' => 'color:inherit;']) ?></div>    
    <h2>Meeting View <small class="text-uppercase"><?= $meeting->week->full_date ?></small></h2>
</div>

<?php if(isset($meeting->meeting_midweek)): ?>
<div class="panel panel-weekhead">
    <div class="panel-heading" style="background: repeating-linear-gradient(45deg, #<?= $meeting->week->month->colour1 ?>, #<?= $meeting->week->month->colour1 ?> 4px, #<?= $meeting->week->month->colour2 ?> 4px, #<?= $meeting->week->month->colour2 ?> 6px);">
        <div class="pull-right"><?= $this->Html->link('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', ['controller' => 'meeting_midweeks', 'action'=> 'edit', $meeting->meeting_midweek->id], ['escape' => false, 'style' => 'color:inherit;']) ?></div>
        <div class="panel-title">Midweek Meeting</div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-4">
        <div class="panel panel-treasures">
            <ul class="list-group">
                <li class="list-group-item">
                    <span class="pull-right"><?= $meeting->meeting_midweek->opening_song_number ?></span>
                    <b>Opening Song</b>
                </li>
                <li class="list-group-item">
                    <span class="pull-right"><?= ($meeting->meeting_midweek->opening_prayer) ? $meeting->meeting_midweek->opening_prayer->full_name : '<span class="text-right text-danger"><i>not set</i></span>' ?></span>
                    <b>Opening Prayer</b>
                </li>
                <li class="list-group-item">
                    <span class="pull-right"><?= ($meeting->meeting_midweek->chairman) ? $meeting->meeting_midweek->chairman->full_name : '<span class="text-right text-danger"><i>not set</i></span>' ?></span>
                    <b>Chairman</b>
                </li>
            </ul>
        </div>
    </div>
    
    <div class="col-xs-12 col-sm-4">
        <div class="panel panel-ministry">
            <ul class="list-group">
                <li class="list-group-item">
                    <span class="pull-right"><?= $meeting->meeting_midweek->middle_song_number ?></span>
                    <b>Middle Song</b>
                </li>
            </ul>
        </div>
    </div>
    
    <div class="col-xs-12 col-sm-4">
        <div class="panel panel-living">
            <ul class="list-group">
                <li class="list-group-item">
                    <span class="pull-right"><?= $meeting->meeting_midweek->closing_song_number ?></span>
                    <b>Closing Song</b>
                </li>
                <li class="list-group-item">
                    <span class="pull-right"><?= ($meeting->meeting_midweek->closing_prayer) ? $meeting->meeting_midweek->closing_prayer->full_name : '<span class="text-right text-danger"><i>not set</i></span>' ?></span>
                    <b>Closing Prayer</b>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel panel-treasures">
            <div class="panel-heading">
                <div class="panel-title">Treasures from God&apos;s Word</div>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <b><?= $meeting->week->week_midweek->treasures_talk_theme ?></b>: (10 min.)
                    <?= $this->Text->nl2p($meeting->meeting_midweek->treasures_talk_material) ?>
                    <p class="text-right"><i><?= $this->Html->link(($meeting->meeting_midweek->treasures_talk_individual) ? $meeting->meeting_midweek->treasures_talk_individual->full_name : 'not set', ['controller' => 'meeting_midweeks', 'action' => 'edit_treasures_talk', $meeting->meeting_midweek->id]) ?></i></p>                    wa
                </li>
                <li class="list-group-item">
                    <b>Digging for Spiritual Gems</b>: (8 min.)
                    <?= $this->Text->nl2p($meeting->meeting_midweek->treasures_gems_material) ?>
                    <p class="text-right"><i><?= $this->Html->link(($meeting->meeting_midweek->treasures_gems_individual) ? $meeting->meeting_midweek->treasures_gems_individual->full_name : 'not set', ['controller' => 'meeting_midweeks', 'action' => 'edit_treasures_gems', $meeting->meeting_midweek->id]) ?></i></p>                    
                </li>
                <li class="list-group-item">
                    <b>Bible Reading</b>: (4 min. or less) <?= $meeting->meeting_midweek->treasures_reading_material ?>
                    <p class="text-right">
                        <i><?= $this->Html->link(($meeting->meeting_midweek->treasures_reading_individual) ? $meeting->meeting_midweek->treasures_reading_individual->full_name : 'not set', ['controller' => 'meeting_midweeks', 'action' => 'edit_treasures_reading', $meeting->meeting_midweek->id]) ?></i>
                    </p>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel panel-ministry">
            <div class="panel-heading">
                <div class="panel-title">Apply Yourself to the Field Ministry</div>
            </div>
            <ul class="list-group">

                <?php if($meeting->meeting_midweek->is_week1): ?>

                <li class="list-group-item">
                    <b>Prepare This Month’s Presentations</b>: (15 min.)
                    <?= $this->Text->nl2p($meeting->meeting_midweek->ministry_presentation_material) ?>
                    <p class="text-right"><i><?= ($meeting->meeting_midweek->ministry_presentation_individual) ? '<span class="text-success">'.$meeting->meeting_midweek->ministry_presentation_individual->full_name.'</span>' : '<span class="text-danger"><i>not set</i></span>' ?></i></p>
                </li>

                <?php else: ?>

                <li class="list-group-item">
                    <b><?= $meeting->meeting_midweek->ministry_item1_type->name ?></b>: (2 min.)
                    <?= $this->Text->nl2p($meeting->meeting_midweek->ministry_item1_material) ?>
                    <p class="text-right fullname">
                        <i><?= $this->Html->link(($meeting->meeting_midweek->ministry_item1_student) ? $meeting->meeting_midweek->ministry_item1_student->full_name : 'not set', ['controller' => 'meeting_midweeks', 'action' => 'edit_ministry_item1', $meeting->meeting_midweek->id]) ?></i>
                    </p>
                    <p class="text-right fullname">
                        <i><?= ($meeting->meeting_midweek->ministry_item1_assistant) ? '<span class="text-warning">'.$meeting->meeting_midweek->ministry_item1_assistant->full_name.'</span>' : '<span class="text-danger"><i>not set</i></span>' ?></i>
                    </p>
                </li>
                <li class="list-group-item">
                    <b><?= $meeting->meeting_midweek->ministry_item2_type->name ?></b>: (4 min.)
                    <?= $this->Text->nl2p($meeting->meeting_midweek->ministry_item2_material) ?>
                    <p class="text-right fullname">
                        <i><?= $this->Html->link(($meeting->meeting_midweek->ministry_item2_student) ? $meeting->meeting_midweek->ministry_item2_student->full_name : 'not set', ['controller' => 'meeting_midweeks', 'action' => 'edit_ministry_item2', $meeting->meeting_midweek->id]) ?></i>
                    </p>
                    <p class="text-right fullname">
                        <i><?= ($meeting->meeting_midweek->ministry_item2_assistant) ? '<span class="text-warning">'.$meeting->meeting_midweek->ministry_item2_assistant->full_name.'</span>' : '<span class="text-danger"><i>not set</i></span>' ?></i>
                    </p>
                </li>
                <li class="list-group-item">
                    <b><?= $meeting->meeting_midweek->ministry_item3_type->name ?></b>: (6 min.)
                    <?= $this->Text->nl2p($meeting->meeting_midweek->ministry_item3_material) ?>
                    <p class="text-right fullname">
                        <i><?= $this->Html->link(($meeting->meeting_midweek->ministry_item3_student) ? $meeting->meeting_midweek->ministry_item3_student->full_name : 'not set', ['controller' => 'meeting_midweeks', 'action' => 'edit_ministry_item3', $meeting->meeting_midweek->id]) ?></i>
                    </p>
                    <p class="text-right fullname">
                        <i><?= ($meeting->meeting_midweek->ministry_item3_assistant) ? '<span class="text-warning">'.$meeting->meeting_midweek->ministry_item3_assistant->full_name.'</span>' : '<span class="text-danger"><i>not set</i></span>' ?></i>
                    </p>
                </li>

                <?php endif; ?>

            </ul>                    
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel panel-living">
            <div class="panel-heading">
                <div class="panel-title">Living as Christians</div>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <b><?= $meeting->meeting_midweek->living_item1_theme ?></b>: (<?= $meeting->meeting_midweek->living_item1_minutes ?> min.) 
                    <?= $meeting->meeting_midweek->living_item1_material ?>
                    <p class="text-right"><i><?= ($meeting->meeting_midweek->living_item1_individual) ? '<span class="text-success">'.$meeting->meeting_midweek->living_item1_individual->full_name.'</span>' : '<span class="text-danger"><i>not set</i></span>' ?></i></p>
                </li>
                
                <?php if($meeting->meeting_midweek->living_item2_theme): ?>
                <li class="list-group-item">
                    <b><?= $meeting->meeting_midweek->living_item2_theme ?></b>: (<?= $meeting->meeting_midweek->living_item2_minutes ?> min.)
                    <?= $meeting->meeting_midweek->living_item2_material ?>
                    <p class="text-right"><i><?= ($meeting->meeting_midweek->living_item2_individual) ? '<span class="text-success">'.$meeting->meeting_midweek->living_item2_individual->full_name.'</span>' : '<span class="text-danger"><i>not set</i></span>' ?></i></p>
                </li>
                <?php endif; ?>
                
                
                <?php if($meeting->meeting_week_type_id == 1): ?>
                <li class="list-group-item">
                    <b>Congregation Bible Study</b>: (30 min.) <?= $meeting->meeting_midweek->living_cbs_material ?>
                    <p class="text-right fullname"><i><?= ($meeting->meeting_midweek->living_cbs_conductor) ? '<span class="text-success">'.$meeting->meeting_midweek->living_cbs_conductor->full_name.'</span>' : '<span class="text-danger"><i>not set</i></span>' ?></i></p>
                    <p class="text-right">Reader: <i><?= ($meeting->meeting_midweek->living_cbs_reader) ? '<span class="text-success">'.$meeting->meeting_midweek->living_cbs_reader->full_name.'</span>' : '<span class="text-danger"><i>not set</i></span>' ?></i></p>
                </li>
                
                <!-- CO -->
                <?php elseif($meeting->meeting_week_type_id == 2): ?>
                <li class="list-group-item">
                    <b><?= $meeting->meeting_midweek->co_talk_theme ?></b>
                    <p class="text-right"><i><span class="text-success"><?= $meeting->installation->circuit_overseer_name ?></span></i></p>
                </li>
                <?php endif; ?>
            </ul>
        </div>                
    </div>
</div>
<?php endif; ?>


<!-- WEEKEND -->
<?php if(isset($meeting->meeting_weekend)): ?>
<div class="panel panel-weekhead">
    <div class="panel-heading" style="background: repeating-linear-gradient(45deg, #<?= $meeting->week->month->colour1 ?>, #<?= $meeting->week->month->colour1 ?> 4px, #<?= $meeting->week->month->colour2 ?> 4px, #<?= $meeting->week->month->colour2 ?> 6px);">
        <div class="pull-right"><?= $this->Html->link('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', ['controller' => 'meeting_weekends', 'action'=> 'edit', $meeting->meeting_weekend->id], ['escape' => false, 'style' => 'color:inherit;']) ?></div>
        <div class="panel-title">Weekend Meeting</div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4"></div>
    <div class="col-xs-12 col-sm-6 col-md-4">
        <ul class="list-group">
            <li class="list-group-item">
                <span class="pull-right"><?= $meeting->meeting_weekend->middle_song_number ?></span>
                <b>Middle Song</b>
            </li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4">
        <ul class="list-group">
            <li class="list-group-item">
                <span class="pull-right"><?= $meeting->meeting_weekend->closing_song_number ?></span>
                <b>Closing Song</b>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4">
        
        <ul class="meetings">
            <li class="bullet">
                <span class="pull-right"><?= (isset($meeting->meeting_weekend->chairman)) ? '<span class="text-right text-success"><i>'.$meeting->meeting_weekend->chairman->full_name.'</i></span>' : '<span class="text-right text-danger">not set</span>' ?></span>
                <b>Chairman</b>
            </li>

            <?php
                $speakerName = '';
                $speakerTheme = '';
                $speakerCongregation = '';

                //  speaker name
                if($meeting->meeting_weekend->speaker):
                    $speakerName = $meeting->meeting_weekend->speaker->full_name;
                elseif($meeting->meeting_weekend->speaker_name != ''):
                    $speakerName = $meeting->meeting_weekend->speaker_name;
                else:
                    $speakerName = '';
                endif;

                //  theme
                if($meeting->meeting_weekend->outline):
                    $speakerTheme = $meeting->meeting_weekend->outline->name;
                elseif($meeting->meeting_weekend->theme != ''):
                    $speakerTheme = $meeting->meeting_weekend->theme;
                else:
                    $speakerTheme = '';
                endif;

                //  congregation
                if($meeting->meeting_weekend->speaker_congregation && $meeting->meeting_weekend->speaker_congregation->id != $currentInstallation->congregation_id):
                    $speakerCongregation = $meeting->meeting_weekend->speaker_congregation->name;
                elseif($meeting->meeting_weekend->congregation_name != ''):
                    $speakerCongregation = $meeting->meeting_weekend->congregation_name;
                else:
                    $speakerCongregation = '';
                endif;
            ?>
            <li class="bullet">
                <b><?= ($speakerTheme != '') ? $speakerTheme : '<span class="text-danger">no theme set</span>' ?></b>
            </li>
            <li>
                <span class="pull-right"><?= ($speakerName != '') ? '<span class="text-right text-success"><i>'.$speakerName.'</i></span>' : '<span class="text-right text-danger">no speaker set</span>' ?></span>
            </li>
            <li>
                <span class="pull-right"><?= ($speakerCongregation != '') ? '<span class="text-right text-warning"><i>'.$speakerCongregation.'</i></span>' : '' ?></span>                        
            </li>
        </ul>
        
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">Watchtower<sub>&reg;</sub></div>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <b><?= $meeting->meeting_weekend->wt_theme ?></b>
                    <p class="text-right">Reader: <i><?= ($meeting->meeting_weekend->reader) ? '<span class="text-success">'.$meeting->meeting_weekend->reader->full_name.'</span>' : '<span class="text-danger"><i>not set</i></span>' ?></i></p>
                </li>
            </ul>                    
        </div>
    </div>

    <?php if($meeting->meeting_week_type_id == 2): ?>
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">Circuit Overseer</div>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <b><?= $meeting->meeting_weekend->co_talk_theme ?></b>
                    <span class="pull-right text-success"><i><?= $meeting->installation->circuit_overseer_name ?></i></span>
                </li>
            </ul>                    
        </div>
    </div>
    <?php endif; ?>
    
</div>
<?php endif; ?>


<div class="panel panel-weekhead">
    <div class="panel-heading" style="background: repeating-linear-gradient(45deg, #<?= $meeting->week->month->colour1 ?>, #<?= $meeting->week->month->colour1 ?> 4px, #<?= $meeting->week->month->colour2 ?> 4px, #<?= $meeting->week->month->colour2 ?> 6px);">
        <div class="pull-right"><?= $this->Html->link('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', ['controller' => 'meeting_outgoing_talks', 'action'=> 'add', '?' => ['meeting_id' => $meeting->id]], ['escape' => false, 'style' => 'color:inherit;']) ?></div>    
        <div class="panel-title">Outgoing Public Talks</div>
    </div>
    
    <?php if($meeting->meeting_outgoing_talks): ?>    
    <table class="table">
        <thead>
            <tr>
                <th>Speaker</th>
                <th>Outline</th>
                <th>Congregation</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($meeting->meeting_outgoing_talks as $outgoing): ?>
            <tr>
                <td><?= $outgoing->speaker->full_name ?></td>
                <td><?= $outgoing->outline->name ?></td>
                <td><?= $outgoing->congregation->name ?></td>
                <td><?= $this->Html->link('edit', ['controller' => 'meeting_outgoing_talks', 'action' => 'edit', $outgoing->id, '?' => ['meeting_id' => $meeting->id]]) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else: ?>
    <div class="panel-body">
        <p>There are no scheduled outgoing talks.</p>
    </div>
    <?php endif; ?>
    
</div>


<?php
if($debug):
    debug(json_encode($meeting, JSON_PRETTY_PRINT));
endif;
?>

