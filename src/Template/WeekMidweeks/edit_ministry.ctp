<div class="page-header">
    <h2>APPLY YOURSELF TO THE FIELD MINISTRY</h2>
</div>

<div class="alert alert-treasures">Apply Yourself to the Field Ministry</div>

<?= $this->Form->create($weekMidweek) ?>
<ul class="list-group">
    <li class="list-group-item">
        <b>Prepare This Month’s Presentations</b>
        <?= $this->Form->textarea('ministry_presentation_material', ['class' => 'form-control', 'placeholder' => 'Material']) ?>
    </li>
</ul>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4">
        <ul class="list-group">
            <li class="list-group-item">
                <b>Initial Call</b>
                <?= $this->Form->textarea('ministry_initial_material', ['class' => 'form-control', 'placeholder' => 'Material']) ?>
            </li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4">
        <ul class="list-group">
            <li class="list-group-item">
                <b>Return Visit</b>
                <?= $this->Form->textarea('ministry_return_material', ['class' => 'form-control', 'placeholder' => 'Material']) ?>
            </li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4">
        <ul class="list-group">
            <li class="list-group-item">
                <b>Bible Study</b>
                <?= $this->Form->textarea('ministry_study_material', ['class' => 'form-control', 'placeholder' => 'Material']) ?>
            </li>
        </ul>
    </div>
</div>
<?= $this->Form->button(__('Save Changes'), ['class' => 'btn btn-success']) ?>
<?= $this->Form->end() ?>

<?php
if($debug):
    debug(json_encode($weekMidweek, JSON_PRETTY_PRINT));
endif;
?>


<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $weekMidweek->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $weekMidweek->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Week Midweeks'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Weeks'), ['controller' => 'Weeks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Week'), ['controller' => 'Weeks', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="weekMidweeks form large-9 medium-8 columns content">
    <?= $this->Form->create($weekMidweek) ?>
    <fieldset>
        <legend><?= __('Edit Week Midweek') ?></legend>
        <?php
            echo $this->Form->input('week_id', ['options' => $weeks, 'empty' => true]);
            echo $this->Form->input('treasures_talk_theme');
            echo $this->Form->input('treasures_talk_material');
            echo $this->Form->input('treasures_gems_material');
            echo $this->Form->input('treasures_reading_material');
            echo $this->Form->input('ministry_presentation_material');
            echo $this->Form->input('ministry_initial_material');
            echo $this->Form->input('ministry_return_material');
            echo $this->Form->input('ministry_study_material');
            echo $this->Form->input('cbs_material');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
