<div class="page-header">
    <h1>Add Midweek Section <small>Week <?= $week->full_date ?></small></h1>
</div>

<?= $this->Form->create($weekMidweek) ?>

<div class="row">
    <div class="col-xs-12 col-sm-6">
        <ul class="list-group">
            <li class="list-group-item">
                <?= $this->Form->input('opening_song_number', ['class' => 'form-control']) ?>
            </li>
        </ul>
        <div class="panel panel-treasures">
            <div class="panel-heading">
                <div class="panel-title">Treasures From God&apos;s Word</div>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <?= $this->Form->input('treasures_material', ['class' => 'form-control', 'label' => 'Bible Reading']) ?>
                </li>
                <li class="list-group-item">
                    <?= $this->Form->input('treasures_talk_theme', ['class' => 'form-control', 'label' => 'Talk Theme']) ?>
                    <b>Talk Material</b><?= $this->Form->textarea('treasures_talk_material', ['class' => 'form-control', 'label' => 'Talk Material']) ?>
                </li>
                <li class="list-group-item"><b>Gems Material</b><?= $this->Form->textarea('treasures_gems_material', ['class' => 'form-control']) ?></li>
                <li class="list-group-item"><?= $this->Form->input('treasures_reading_material', ['class' => 'form-control', 'label' => 'Reading Material']) ?></li>
            </ul>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6">
        <div class="panel panel-ministry">
            <div class="panel-heading">
                <div class="panel-title">Apply Yourself to the Field Ministry</div>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <?= $this->Form->input('is_week1', ['options' => [1 => 'Week 1', 0 => 'Week 2, 3, 4 or 5'], 'class' => 'form-control', 'label' => false]) ?>
                </li>

                <li class="list-group-item">
                    <b>Presentation Material</b><?= $this->Form->textarea('ministry_presentation_material', ['class' => 'form-control', 'label' => 'Presentation Material']) ?>
                </li>
                <li class="list-group-item disabled">- OR -</li>
                <li class="list-group-item">
                    <b>Initial Call</b><?= $this->Form->textarea('ministry_item1_material', ['class' => 'form-control']) ?>
                </li>
                <li class="list-group-item">
                    <b>Return Visit</b><?= $this->Form->textarea('ministry_item2_material', ['class' => 'form-control']) ?>
                </li>
                <li class="list-group-item">
                    <b>Bible Study</b><?= $this->Form->textarea('ministry_item3_material', ['class' => 'form-control']) ?>
                </li>
            </ul>
        </div>
        <div class="panel panel-living">
            <div class="panel-heading">
                <div class="panel-title">Living As Christians</div>
            </div>
            <ul class="list-group">
              <li class="list-group-item">
                  <?= $this->Form->input('middle_song_number', ['class' => 'form-control']) ?>
              </li>
                <li class="list-group-item">
                    <?= $this->Form->input('living_item1_theme', ['class' => 'form-control', 'label' => 'Item 1 Theme']) ?>
                    <b>Material</b><?= $this->Form->textarea('living_item1_material', ['class' => 'form-control']) ?>
                    <?= $this->Form->input('living_item1_minutes', ['class' => 'form-control', 'label' => 'Minutes', 'options' => [1 => 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]]) ?>
                </li>
                <li class="list-group-item disabled">- AND OPTIONALLY -</li>
                <li class="list-group-item list-group-item-warning">
                    <?= $this->Form->input('living_item2_theme', ['class' => 'form-control', 'label' => 'Item 2 Theme']) ?>
                    <b>Material</b><?= $this->Form->textarea('living_item2_material', ['class' => 'form-control']) ?>
                    <?= $this->Form->input('living_item2_minutes', ['class' => 'form-control', 'label' => 'Minutes', 'options' => [1 => 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]]) ?>
                </li>
                <li class="list-group-item"><?= $this->Form->input('living_cbs_material', ['class' => 'form-control', 'label' => 'CBS Material']) ?></li>
                <li class="list-group-item">
                    <?= $this->Form->input('closing_song_number', ['class' => 'form-control']) ?>
                </li>
            </ul>
        </div>
    </div>
</div>
<?= $this->Form->button(__('Submit')) ?>
<?= $this->Form->end() ?>


<?php
if($debug):
  debug(json_encode($week, JSON_PRETTY_PRINT));
endif;
?>
