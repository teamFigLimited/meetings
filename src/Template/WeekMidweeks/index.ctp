<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Week Midweek'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Weeks'), ['controller' => 'Weeks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Week'), ['controller' => 'Weeks', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="weekMidweeks index large-9 medium-8 columns content">
    <h3><?= __('Week Midweeks') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('week_id') ?></th>
                <th><?= $this->Paginator->sort('treasures_talk_theme') ?></th>
                <th><?= $this->Paginator->sort('treasures_talk_material') ?></th>
                <th><?= $this->Paginator->sort('treasures_gems_material') ?></th>
                <th><?= $this->Paginator->sort('treasures_reading_material') ?></th>
                <th><?= $this->Paginator->sort('ministry_presentation_material') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($weekMidweeks as $weekMidweek): ?>
            <tr>
                <td><?= $this->Number->format($weekMidweek->id) ?></td>
                <td><?= $weekMidweek->has('week') ? $this->Html->link($weekMidweek->week->id, ['controller' => 'Weeks', 'action' => 'view', $weekMidweek->week->id]) : '' ?></td>
                <td><?= h($weekMidweek->treasures_talk_theme) ?></td>
                <td><?= h($weekMidweek->treasures_talk_material) ?></td>
                <td><?= h($weekMidweek->treasures_gems_material) ?></td>
                <td><?= h($weekMidweek->treasures_reading_material) ?></td>
                <td><?= h($weekMidweek->ministry_presentation_material) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $weekMidweek->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $weekMidweek->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $weekMidweek->id], ['confirm' => __('Are you sure you want to delete # {0}?', $weekMidweek->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
