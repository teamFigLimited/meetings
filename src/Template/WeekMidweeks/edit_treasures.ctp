<div class="page-header">
    <h2>TREASURES FROM GOD'S WORD <small class="text-uppercase">edit</small></h2>
</div>

<div class="alert alert-treasures">Treasures from God's Word</div>

<?= $this->Form->create($weekMidweek) ?>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4">
        <ul class="list-group">
            <li class="list-group-item">
                <?= $this->Form->input('treasures_talk_theme', ['class' => 'form-control', 'label' => 'Treasures Talk', 'placeholder' => 'Theme']) ?>
                <?= $this->Form->textarea('treasures_talk_material', ['class' => 'form-control', 'placeholder' => 'Material']) ?>
            </li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4">
        <ul class="list-group">
            <li class="list-group-item">
                <b>Digging for Spiritual Gems</b>
                <?= $this->Form->textarea('treasures_gems_material', ['class' => 'form-control', 'placeholder' => 'Material']) ?>
            </li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4">
        <ul class="list-group">
            <li class="list-group-item">
                <?= $this->Form->input('treasures_reading_material', ['class' => 'form-control', 'placeholder' => 'Material', 'label' => 'Bible Reading']) ?>
            </li>
        </ul>
    </div>
</div>
<?= $this->Form->button(__('Save Changes'), ['class' => 'btn btn-success']) ?>
<?= $this->Form->end() ?>

<?php
if($debug):
    debug(json_encode($weekMidweek, JSON_PRETTY_PRINT));
endif;
?>
