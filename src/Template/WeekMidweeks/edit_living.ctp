<div class="page-header">
    <h2>LIVING AS CHRISTIANS</h2>
</div>

<div class="alert alert-living">Living as Christians</div>

<?= $this->Form->create($weekMidweek) ?>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">Item 1</div>
            </div>
            
            <ul class="list-group">
                <li class="list-group-item">
                    <b>Theme</b>
                    <?= $this->Form->input('living_item1_theme', ['class' => 'form-control', 'placeholder' => 'Theme', 'label' => false]) ?>
                </li>
                <li class="list-group-item">
                    <b>Material</b>
                    <?= $this->Form->textarea('living_item1_material', ['class' => 'form-control', 'placeholder' => 'Material']) ?>
                </li>
                <li class="list-group-item">
                    <b>Minutes</b>
                    <?= $this->Form->input('living_item1_minutes', ['class' => 'form-control', 'placeholder' => 'Minutes', 'label' => false]) ?>
                </li>
            </ul>

        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">Item 2</div>
            </div>
            
            <ul class="list-group">
                <li class="list-group-item">
                    <b>Theme</b>
                    <?= $this->Form->input('living_item2_theme', ['class' => 'form-control', 'placeholder' => 'Theme', 'label' => false]) ?>
                </li>
                <li class="list-group-item">
                    <b>Material</b>
                    <?= $this->Form->textarea('living_item2_material', ['class' => 'form-control', 'placeholder' => 'Material']) ?>
                </li>
                <li class="list-group-item">
                    <b>Minutes</b>
                    <?= $this->Form->input('living_item2_minutes', ['class' => 'form-control', 'placeholder' => 'Minutes', 'label' => false]) ?>
                </li>
            </ul>

        </div>
    </div>
</div>
<?= $this->Form->button(__('Save Changes'), ['class' => 'btn btn-success']) ?>
<?= $this->Form->end() ?>

<?php
if($debug):
    debug(json_encode($weekMidweek, JSON_PRETTY_PRINT));
endif;
?>
