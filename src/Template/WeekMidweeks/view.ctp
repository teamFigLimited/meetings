<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Week Midweek'), ['action' => 'edit', $weekMidweek->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Week Midweek'), ['action' => 'delete', $weekMidweek->id], ['confirm' => __('Are you sure you want to delete # {0}?', $weekMidweek->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Week Midweeks'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Week Midweek'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Weeks'), ['controller' => 'Weeks', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Week'), ['controller' => 'Weeks', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="weekMidweeks view large-9 medium-8 columns content">
    <h3><?= h($weekMidweek->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Week') ?></th>
            <td><?= $weekMidweek->has('week') ? $this->Html->link($weekMidweek->week->id, ['controller' => 'Weeks', 'action' => 'view', $weekMidweek->week->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Treasures Talk Theme') ?></th>
            <td><?= h($weekMidweek->treasures_talk_theme) ?></td>
        </tr>
        <tr>
            <th><?= __('Treasures Talk Material') ?></th>
            <td><?= h($weekMidweek->treasures_talk_material) ?></td>
        </tr>
        <tr>
            <th><?= __('Treasures Gems Material') ?></th>
            <td><?= h($weekMidweek->treasures_gems_material) ?></td>
        </tr>
        <tr>
            <th><?= __('Treasures Reading Material') ?></th>
            <td><?= h($weekMidweek->treasures_reading_material) ?></td>
        </tr>
        <tr>
            <th><?= __('Ministry Presentation Material') ?></th>
            <td><?= h($weekMidweek->ministry_presentation_material) ?></td>
        </tr>
        <tr>
            <th><?= __('Ministry Initial Material') ?></th>
            <td><?= h($weekMidweek->ministry_initial_material) ?></td>
        </tr>
        <tr>
            <th><?= __('Ministry Return Material') ?></th>
            <td><?= h($weekMidweek->ministry_return_material) ?></td>
        </tr>
        <tr>
            <th><?= __('Ministry Study Material') ?></th>
            <td><?= h($weekMidweek->ministry_study_material) ?></td>
        </tr>
        <tr>
            <th><?= __('Cbs Material') ?></th>
            <td><?= h($weekMidweek->cbs_material) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($weekMidweek->id) ?></td>
        </tr>
    </table>
</div>
