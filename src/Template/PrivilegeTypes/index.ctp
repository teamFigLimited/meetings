<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Privilege Type'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="privilegeTypes index large-9 medium-8 columns content">
    <h3><?= __('Privilege Types') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($privilegeTypes as $privilegeType): ?>
            <tr>
                <td><?= $this->Number->format($privilegeType->id) ?></td>
                <td><?= h($privilegeType->name) ?></td>
                <td><?= h($privilegeType->created) ?></td>
                <td><?= h($privilegeType->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $privilegeType->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $privilegeType->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $privilegeType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $privilegeType->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
