<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Privilege Type'), ['action' => 'edit', $privilegeType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Privilege Type'), ['action' => 'delete', $privilegeType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $privilegeType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Privilege Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Privilege Type'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="privilegeTypes view large-9 medium-8 columns content">
    <h3><?= h($privilegeType->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($privilegeType->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($privilegeType->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($privilegeType->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($privilegeType->modified) ?></tr>
        </tr>
    </table>
</div>
