<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Outline'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="outlines index large-9 medium-8 columns content">
    <h3><?= __('Outlines') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('number') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('date_revision') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($outlines as $outline): ?>
            <tr>
                <td><?= $this->Number->format($outline->id) ?></td>
                <td><?= $this->Number->format($outline->number) ?></td>
                <td><?= h($outline->name) ?></td>
                <td><?= h($outline->date_revision) ?></td>
                <td><?= h($outline->created) ?></td>
                <td><?= h($outline->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $outline->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $outline->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $outline->id], ['confirm' => __('Are you sure you want to delete # {0}?', $outline->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
