<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Outline'), ['action' => 'edit', $outline->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Outline'), ['action' => 'delete', $outline->id], ['confirm' => __('Are you sure you want to delete # {0}?', $outline->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Outlines'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Outline'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="outlines view large-9 medium-8 columns content">
    <h3><?= h($outline->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($outline->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($outline->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Number') ?></th>
            <td><?= $this->Number->format($outline->number) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Revision') ?></th>
            <td><?= h($outline->date_revision) ?></tr>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($outline->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($outline->modified) ?></tr>
        </tr>
    </table>
</div>
