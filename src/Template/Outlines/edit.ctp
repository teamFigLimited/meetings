<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $outline->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $outline->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Outlines'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="outlines form large-9 medium-8 columns content">
    <?= $this->Form->create($outline) ?>
    <fieldset>
        <legend><?= __('Edit Outline') ?></legend>
        <?php
            echo $this->Form->input('number');
            echo $this->Form->input('name');
            echo $this->Form->input('date_revision', ['empty' => true, 'default' => '']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
