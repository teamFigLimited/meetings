<div class="page-header">
    <h3>Weeks Schedule</h3>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">Month</h4>
			</div>
			<div class="list-group">
                <?php foreach($months as $month): ?>
                <?= $this->Html->link($month->date_month->format('F, Y'), ['controller' => 'weeks', 'action' => 'index', '?' => ['month_id' => $month->id]], ['class' => 'list-group-item'.(($month->id == $monthId) ? ' list-group-item-success' : '')]) ?>
                <?php endforeach; ?>
                <a class="list-group-item divider"></a>
                <?= $this->Html->link('Add a Week', ['controller' => 'weeks', 'action' => 'add'], ['class' => 'list-group-item']) ?>
			</div>
		</div>
    </div>
    <div class="col-xs-12 col-sm-9 col-md-10 col-lg-10">
		<?php foreach ($weeks as $week): ?>
        <div class="h1 pull-right"><?= $this->Html->link('<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>', ['controller' => 'weeks', 'action'=> 'view', $week->id], ['escape' => false]) ?></div>
        <h1><?= $week->full_date ?></h1>
        
        <?php if($week->week_midweek): ?>
        <h3 class="h3">Our Christian Life and Ministry</h3>
        <div class="panel panel-weekhead">
            <div class="panel-heading" style="background: repeating-linear-gradient(45deg, #<?= $week->month->colour1 ?>, #<?= $week->month->colour1 ?> 4px, #<?= $week->month->colour2 ?> 4px, #<?= $week->month->colour2 ?> 6px);"><h3 class="panel-title"><?= $week->full_date ?> | <?= $week->week_midweek->treasures_material ?></h3></div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                
                <ul class="meetings treasures">
					<li class="bullet">
						Song <?= $week->week_midweek->opening_song_number ?> and Prayer
					</li>
					<li class="bullet">
						Opening Comments (3 min. or less)
					</li>
                </ul>

                <!-- TREASURES -->
                <div class="alert alert-treasures">TREASURES FROM GOD'S WORD</div>
                
                <ul class="meetings treasures">
					<li class="bullet">
						<b><?= $week->week_midweek->treasures_talk_theme ?></b>: (10 min.)
                        <?= $this->Text->nl2p($week->week_midweek->treasures_talk_material, false) ?>
					</li>
					<li class="bullet">
						<b>Digging for Spiritual Gems</b>: (8 min.)
                        <?= $this->Text->nl2p($week->week_midweek->treasures_gems_material) ?>
					</li>
					<li class="bullet">
						<b>Bible Reading:</b> <?= $week->week_midweek->treasures_reading_material ?> (4 min. or less)
					</li>
                </ul>
                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="alert alert-ministry">APPLY YOURSELF TO THE FIELD MINISTRY</div>
				
				<ul class="meetings ministry">
					<?php if($week->week_midweek->is_week1 == 1): ?>
					<li class="bullet">
						<b>Prepare This Month’s Presentations</b>: (15 min.)
                        <p><?= $week->week_midweek->ministry_presentation_material ?></p>
					</li>
					<?php else: ?>
					
					<!-- INITIAL -->
					<li class="bullet">
						<b><?= $week->week_midweek->ministry_item1_type->name ?></b>: (2 min. or less) <?= $week->week_midweek->ministry_item1_material ?>
					</li>

					<!-- RETURN -->
					<li class="bullet">
						<b><?= $week->week_midweek->ministry_item2_type->name ?></b>: (4 min. or less) <?= $week->week_midweek->ministry_item2_material ?>
					</li>

					<!-- STUDY -->
					<li class="bullet">
						<b><?= $week->week_midweek->ministry_item3_type->name ?></b>: (6 min. or less) <?= $week->week_midweek->ministry_item3_material ?>
					</li>

					<?php endif; ?>
				</ul>

                <div class="alert alert-living">LIVING AS CHRISTIANS</div>

				<ul class="meetings living">
					<li class="bullet">
						Song <?= $week->week_midweek->middle_song_number ?>
					</li>
                    
                    <!-- LIVING -->
					<li class="bullet">
						<b><?= $week->week_midweek->living_item1_theme ?></b>: (<?= $week->week_midweek->living_item1_minutes ?> min.) <?= $week->week_midweek->living_item1_material ?>
					</li>
                    <?php if($week->week_midweek->living_item2_theme): ?>
					<li class="bullet">
						<b><?= $week->week_midweek->living_item2_theme ?></b>: (<?= $week->week_midweek->living_item2_minutes ?> min.) <?= $week->week_midweek->living_item2_material ?>
					</li>
                    <?php endif; ?>
                    					
					<li class="bullet">
						<b>Congregation Bible Study</b>: <?= $week->week_midweek->living_cbs_material ?> (30 min.)
					</li>

					<li class="bullet">
						Review Followed by Preview of Next Week (3 min.)
					</li>
					<li class="bullet">
						Song <?= $week->week_midweek->closing_song_number ?> and Prayer
					</li>					
				</ul>
				
            </div>
        </div>
        <?php endif; ?> <!-- MIDWEEK -->
        
        <?php if($week->week_weekend): ?>        
        <h3 class="h3">Weekend Meeting</h3>
        <div class="row">
            <div class="col-xs-12 col-sm-6"></div>
            <div class="col-xs-12 col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">Watchtower<sub><small>&reg;</small></sub></h4>
                    </div>
				</div>
				
				<ul class="meetings">
					<li class="bullet">
						Song <?= $week->week_weekend->middle_song_number ?>
					</li>					
					<li class="bullet">
						<b><?= $week->week_weekend->wt_theme ?></b>
					</li>
					<li class="bullet">
						Song <?= $week->week_weekend->closing_song_number ?> and Prayer
					</li>					
				</ul>

            </div>
        </div>
        <?php endif; ?>
        
        <div class="panel panel-grey">
            <div class="panel-heading">
                <h4 class="panel-title">&nbsp;</h4>
            </div>
        </div>
        <br>
		<?php endforeach; ?>
    </div>
</div>

<?php
if($debug):
    debug(json_encode($weeks, JSON_PRETTY_PRINT));
endif;
?>

