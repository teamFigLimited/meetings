<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $week->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $week->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Weeks'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Months'), ['controller' => 'Months', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Month'), ['controller' => 'Months', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Meetings'), ['controller' => 'Meetings', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Meeting'), ['controller' => 'Meetings', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Week Livings'), ['controller' => 'WeekLivings', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Week Living'), ['controller' => 'WeekLivings', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Week Midweeks'), ['controller' => 'WeekMidweeks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Week Midweek'), ['controller' => 'WeekMidweeks', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Week Songs'), ['controller' => 'WeekSongs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Week Song'), ['controller' => 'WeekSongs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Week Weekends'), ['controller' => 'WeekWeekends', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Week Weekend'), ['controller' => 'WeekWeekends', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Week Wts'), ['controller' => 'WeekWts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Week Wt'), ['controller' => 'WeekWts', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="weeks form large-9 medium-8 columns content">
    <?= $this->Form->create($week) ?>
    <fieldset>
        <legend><?= __('Edit Week') ?></legend>
        <?php
            echo $this->Form->input('month_id', ['options' => $months, 'empty' => true]);
            echo $this->Form->input('date_monday', ['empty' => true, 'default' => '']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
