<div class="page-header">
    <h2>Week View <small class="text-uppercase"><?= $week->full_date ?></small></h2>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-3 col-md-3">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">Page Options</div>
            </div>
            <div class="list-group">
                <?= $this->Html->link('back to list', ['controller' => 'weeks', 'action' => 'index', '?' => ['month_id' => $week->month->id]], ['class' => 'list-group-item']) ?>
                <?= (!$week->week_midweek) ? $this->Html->link('Add Midweek Meeting', ['controller' => 'week_midweeks', 'action' => 'add', '?' => ['week_id' => $week->id]], ['class' => 'list-group-item']) : '' ?>
                <?= (!$week->week_midweek) ? $this->Html->link('Import Midweek Meeting', ['controller' => 'week_midweeks', 'action' => 'import', '?' => ['week_id' => $week->id]], ['class' => 'list-group-item']) : '' ?>
                <?= (!$week->week_weekend) ? $this->Html->link('Add Weekend Meeting', ['controller' => 'week_weekends', 'action' => 'add', '?' => ['week_id' => $week->id]], ['class' => 'list-group-item']) : '' ?>
                <?= ($week->is_live == 1) ? $this->Html->link('De-activate Week', ['controller' => 'weeks', 'action' => 'deactivate', $week->id], ['class' => 'list-group-item']) : '' ?>
                <?= ($week->is_live != 1) ? $this->Html->link('Activate Week', ['controller' => 'weeks', 'action' => 'activate', $week->id], ['class' => 'list-group-item']) : '' ?>
                <?= (isset($week->week_midweek) && $week->week_midweek->is_live == 1) ? $this->Html->link('De-activate Midweek', ['controller' => 'week_midweeks', 'action' => 'deactivate', $week->week_midweek->id], ['class' => 'list-group-item']) : '' ?>
                <?= (isset($week->week_midweek) && $week->week_midweek->is_live != 1) ? $this->Html->link('Activate Midweek', ['controller' => 'week_midweeks', 'action' => 'activate', $week->week_midweek->id], ['class' => 'list-group-item']) : '' ?>
                <?= (isset($week->week_weekend) && $week->week_weekend->is_live == 1) ? $this->Html->link('De-activate Weekend', ['controller' => 'week_weekends', 'action' => 'deactivate', $week->week_weekend->id], ['class' => 'list-group-item']) : '' ?>
                <?= (isset($week->week_weekend) && $week->week_weekend->is_live != 1) ? $this->Html->link('Activate Weekend', ['controller' => 'week_weekends', 'action' => 'activate', $week->week_weekend->id], ['class' => 'list-group-item']) : '' ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-9 col-md-9">
        <?php if(isset($week->week_midweek)): ?>
        <h3 class="h3">Our Christian Life and Ministry</h3>
        <div class="panel panel-weekhead">            
            <div class="panel-heading" style="background: repeating-linear-gradient(45deg, #<?= $week->month->colour1 ?>, #<?= $week->month->colour1 ?> 4px, #<?= $week->month->colour2 ?> 4px, #<?= $week->month->colour2 ?> 6px);">
                <div class="pull-right"><?= $this->Html->link('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', ['controller' => 'week_midweeks', 'action'=> 'edit', $week->week_midweek->id], ['escape' => false, 'style' => 'color:inherit;']) ?></div>
                <h3 class="panel-title"><?= $week->full_date ?> | <?= $week->week_midweek->treasures_material ?></h3>
            </div>
        </div>


        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <ul class="list-group">
                    <li class="list-group-item">
                        <span class="pull-right"><?= $week->week_midweek->opening_song_number ?></span>
                        <b>Opening Song</b>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <ul class="list-group">
                    <li class="list-group-item">
                        <span class="pull-right"><?= $week->week_midweek->middle_song_number ?></span>
                        <b>Middle Song</b>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <ul class="list-group">
                    <li class="list-group-item">
                        <span class="pull-right"><?= $week->week_midweek->closing_song_number ?></span>
                        <b>Closing Song</b>
                    </li>
                </ul>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="panel panel-treasures">
                    <div class="panel-heading">
                        <div class="panel-title">Treasures from God&apos;s Word</div>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item"><b><?= $week->week_midweek->treasures_talk_theme ?></b>: (10 min.)<br><?= $this->Text->nl2p($week->week_midweek->treasures_talk_material) ?></li>
                        <li class="list-group-item"><b>Digging for Spiritual Gems</b>: (8 min.)<br><?= $this->Text->nl2p($week->week_midweek->treasures_gems_material) ?></li>
                        <li class="list-group-item"><b>Bible Reading</b>: (4 min. or less)<br><?= $this->Text->nl2p($week->week_midweek->treasures_reading_material) ?></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="panel panel-ministry">
                    <div class="panel-heading">
                        <div class="panel-title">Apply Yourself to the Field Ministry</div>
                    </div>
                    <ul class="list-group">
                        
                        <?php if($week->week_midweek->is_week1): ?>
                        
                        <li class="list-group-item"><b>Prepare This Month’s Presentations</b>: (15 min.)<br><?= $this->Text->nl2p($week->week_midweek->ministry_presentation_material) ?></li>
                        
                        <?php else: ?>
                        
                        <li class="list-group-item"><b><?= $week->week_midweek->ministry_item1_type->name ?></b>: (2 min.)<br><?= $week->week_midweek->ministry_item1_material ?></li>
                        <li class="list-group-item"><b><?= $week->week_midweek->ministry_item2_type->name ?></b>: (4 min.)<br><?= $week->week_midweek->ministry_item2_material ?></li>
                        <li class="list-group-item"><b><?= $week->week_midweek->ministry_item3_type->name ?></b>: (6 min.)<br><?= $week->week_midweek->ministry_item3_material ?></li>
                            
                        <?php endif; ?>
                        
                    </ul>                    
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="panel panel-living">
                    <div class="panel-heading">
                        <div class="panel-title">Living as Christians</div>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item"><b><?= $week->week_midweek->living_item1_theme ?></b>: (<?= $week->week_midweek->living_item1_minutes ?> min.) <?= $week->week_midweek->living_item1_material ?></li>
                        <?php if($week->week_midweek->living_item2_theme): ?>
                        <li class="list-group-item"><b><?= $week->week_midweek->living_item2_theme ?></b>: (<?= $week->week_midweek->living_item2_minutes ?> min.) <?= $week->week_midweek->living_item2_material ?></li>
                        <?php endif; ?>
                        <li class="list-group-item"><b><?= $week->week_midweek->living_cbs_material ?></b>: (30 min.)</li>
                    </ul>
                </div>                
            </div>
        </div>
        <?php endif; ?>


        <!-- WEEKEND -->
        <?php if(isset($week->week_weekend)): ?>
        <h3 class="h3">Weekend Meeting</h3>
        
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <ul class="list-group">
                    <li class="list-group-item">
                        <span class="pull-right"><?= $week->week_weekend->middle_song_number ?></span>
                        <b>Middle Song</b>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-title">Watchtower<sub>&reg;</sub></div>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item"><b>Theme</b>: <?= $week->week_weekend->wt_theme ?></li>
                    </ul>                    
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <ul class="list-group">
                    <li class="list-group-item">
                        <span class="pull-right"><?= $week->week_weekend->closing_song_number ?></span>
                        <b>Closing Song</b>
                    </li>
                </ul>
            </div>
        </div>
        
        <?php endif; ?>
    </div>
</div>


<?php if($debug): ?>
<?php debug(json_encode($week, JSON_PRETTY_PRINT)); ?>
<?php endif; ?>

