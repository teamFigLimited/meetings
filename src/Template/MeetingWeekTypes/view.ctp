<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Meeting Week Type'), ['action' => 'edit', $meetingWeekType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Meeting Week Type'), ['action' => 'delete', $meetingWeekType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $meetingWeekType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Meeting Week Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Meeting Week Type'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="meetingWeekTypes view large-9 medium-8 columns content">
    <h3><?= h($meetingWeekType->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($meetingWeekType->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($meetingWeekType->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Meeting') ?></th>
            <td><?= $this->Number->format($meetingWeekType->is_meeting) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($meetingWeekType->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($meetingWeekType->modified) ?></td>
        </tr>
    </table>
</div>
