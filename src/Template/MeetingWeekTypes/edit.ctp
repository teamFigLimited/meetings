<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $meetingWeekType->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $meetingWeekType->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Meeting Week Types'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="meetingWeekTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($meetingWeekType) ?>
    <fieldset>
        <legend><?= __('Edit Meeting Week Type') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('is_meeting');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
