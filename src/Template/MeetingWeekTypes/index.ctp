<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Meeting Week Type'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="meetingWeekTypes index large-9 medium-8 columns content">
    <h3><?= __('Meeting Week Types') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('is_meeting') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($meetingWeekTypes as $meetingWeekType): ?>
            <tr>
                <td><?= $this->Number->format($meetingWeekType->id) ?></td>
                <td><?= h($meetingWeekType->name) ?></td>
                <td><?= $this->Number->format($meetingWeekType->is_meeting) ?></td>
                <td><?= h($meetingWeekType->created) ?></td>
                <td><?= h($meetingWeekType->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $meetingWeekType->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $meetingWeekType->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $meetingWeekType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $meetingWeekType->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
