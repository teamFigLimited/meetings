<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Month'), ['action' => 'edit', $month->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Month'), ['action' => 'delete', $month->id], ['confirm' => __('Are you sure you want to delete # {0}?', $month->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Months'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Month'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Weeks'), ['controller' => 'Weeks', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Week'), ['controller' => 'Weeks', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="months view large-9 medium-8 columns content">
    <h3><?= h($month->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($month->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Month') ?></th>
            <td><?= h($month->date_month) ?></tr>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($month->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($month->modified) ?></tr>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Weeks') ?></h4>
        <?php if (!empty($month->weeks)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Month Id') ?></th>
                <th><?= __('Date Monday') ?></th>
                <th><?= __('Treasures Talk Theme') ?></th>
                <th><?= __('Treasures Talk Material') ?></th>
                <th><?= __('Treasures Gems Material') ?></th>
                <th><?= __('Treasures Reading Material') ?></th>
                <th><?= __('Ministry Presentation Material') ?></th>
                <th><?= __('Ministry Initial Material') ?></th>
                <th><?= __('Ministry Return Material') ?></th>
                <th><?= __('Ministry Study Material') ?></th>
                <th><?= __('Living Item1 Theme') ?></th>
                <th><?= __('Living Item1 Material') ?></th>
                <th><?= __('Living Item1 Minutes') ?></th>
                <th><?= __('Living Item2 Theme') ?></th>
                <th><?= __('Living Item2 Material') ?></th>
                <th><?= __('Living Item2 Minutes') ?></th>
                <th><?= __('Cbs Material') ?></th>
                <th><?= __('Wt Theme') ?></th>
                <th><?= __('Is Midweek Live') ?></th>
                <th><?= __('Is Weekend Live') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($month->weeks as $weeks): ?>
            <tr>
                <td><?= h($weeks->id) ?></td>
                <td><?= h($weeks->month_id) ?></td>
                <td><?= h($weeks->date_monday) ?></td>
                <td><?= h($weeks->treasures_talk_theme) ?></td>
                <td><?= h($weeks->treasures_talk_material) ?></td>
                <td><?= h($weeks->treasures_gems_material) ?></td>
                <td><?= h($weeks->treasures_reading_material) ?></td>
                <td><?= h($weeks->ministry_presentation_material) ?></td>
                <td><?= h($weeks->ministry_initial_material) ?></td>
                <td><?= h($weeks->ministry_return_material) ?></td>
                <td><?= h($weeks->ministry_study_material) ?></td>
                <td><?= h($weeks->living_item1_theme) ?></td>
                <td><?= h($weeks->living_item1_material) ?></td>
                <td><?= h($weeks->living_item1_minutes) ?></td>
                <td><?= h($weeks->living_item2_theme) ?></td>
                <td><?= h($weeks->living_item2_material) ?></td>
                <td><?= h($weeks->living_item2_minutes) ?></td>
                <td><?= h($weeks->cbs_material) ?></td>
                <td><?= h($weeks->wt_theme) ?></td>
                <td><?= h($weeks->is_midweek_live) ?></td>
                <td><?= h($weeks->is_weekend_live) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Weeks', 'action' => 'view', $weeks->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Weeks', 'action' => 'edit', $weeks->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Weeks', 'action' => 'delete', $weeks->id], ['confirm' => __('Are you sure you want to delete # {0}?', $weeks->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
