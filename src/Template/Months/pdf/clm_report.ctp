<?= $this->Html->css('spacelab.min', ['fullBase' => true]) ?>
<?= $this->Html->css('css', ['fullBase' => true]) ?>
<?= $this->fetch('css') ?>
<style>
    @page { margin: 0;}
    body { margin: 0 5mm 0 5mm;  font-size:10px;}
    h3 { padding: 8px 8px 2px 8px;margin:0;}
    h4 { border-bottom-color: rgb(238, 238, 238); border-bottom-style: solid; border-bottom-width: 1px; }
    p {margin:0;padding:0;}
</style>

<div class="page-header">
    <h1>Our Christian Life and Ministry</h1>
</div>

<table style="width:100%;margin-bottom:0;padding-bottom:0;margin-top:0;padding-top:0;">
    <tr>
        <td style="width:33%;padding:2px 8px;"><div class="alert alert-treasures">Treasures From Gods Word</div></td>
        <td style="width:34%;padding:2px 8px;"><div class="alert alert-ministry">Applying Ourselves to the Ministry</div></td>
        <td style="width:33%;padding:2px 8px;"><div class="alert alert-living">Living as Christians</div></td>
    </tr>
</table
<?php
foreach($month->weeks as $week):
?>
<h3><?= $week->full_date ?></h3>
<table style="width:100%;">
    <tr>
        <td style="width:33%;padding:2px 8px; vertical-align: top;">
            
            <div style="border-bottom:1px solid #dddddd;">
                <p><?= $week->meetings[0]->meeting_midweek->treasures_talk_theme ?></p>
                <p style="margin-bottom:4px;width:100%;text-align:right;">
                    <b><?= ($week->meetings[0]->meeting_midweek->treasures_talk_individual) ? '<span class="text-success">'.$week->meetings[0]->meeting_midweek->treasures_talk_individual->full_name.'</span>' : '' ?></b>
                </p>
            </div>
            
            <div style="border-bottom:1px solid #dddddd;">
                <p>Digging For Spiritual Gems</p>
                <p style="margin-bottom:4px;width:100%;text-align:right;">
                    <b><?= ($week->meetings[0]->meeting_midweek->treasures_gems_individual) ? $week->meetings[0]->meeting_midweek->treasures_gems_individual->full_name : '' ?></b>
                </p>
            </div>
            
            <div style="border-bottom:1px solid #dddddd;">
                <p>Bible Reading</p>
                <p style="margin-bottom:4px;width:100%;text-align:right;">
                    <b><?= ($week->meetings[0]->meeting_midweek->treasures_reading_individual) ? $week->meetings[0]->meeting_midweek->treasures_reading_individual->full_name : '' ?></b>
                </p>
            </div>
            
        </td>
        
        <td style="width:34%;padding:8px; vertical-align: top;">
            
            <?php if($week->meetings[0]->meeting_midweek->is_week1 == 1): ?>

            <div style="border-bottom:1px solid #dddddd;">
                <p>Prepare This Month's Presentations: (15 min.)</p>
                <p style="margin-bottom:4px;width:100%;text-align:right;">
                    <b><?= ($week->meetings[0]->meeting_midweek->ministry_presentation_individual) ? '<span class="text-success">'.$week->meetings[0]->meeting_midweek->ministry_presentation_individual->full_name.'</span>' : ' ' ?></b>
                </p>
            </div>
            
            <?php else: ?>

            <div style="border-bottom:1px solid #dddddd;">
                <p><b><?= $week->meetings[0]->meeting_midweek->ministry_item1_type->name ?></b>: (2 min.)</p>
                <p style="margin-bottom:4px;width:100%;text-align:right;">
                    <b><?= ($week->meetings[0]->meeting_midweek->ministry_item1_student) ? '<span class="text-success">'.$week->meetings[0]->meeting_midweek->ministry_item1_student->full_name.'</span>' : ' ' ?></b>
                    <b><?= ($week->meetings[0]->meeting_midweek->ministry_item1_assistant) ? '<span class="text-warning">(A: '.$week->meetings[0]->meeting_midweek->ministry_item1_assistant->full_name.')</span>' : ' ' ?></b>
                </p>
            </div>

            <div style="border-bottom:1px solid #dddddd;">
                <p><b><?= $week->meetings[0]->meeting_midweek->ministry_item2_type->name ?></b>: (4 min.)</p>
                <p style="margin-bottom:4px;width:100%;text-align:right;">
                    <b><?= ($week->meetings[0]->meeting_midweek->ministry_item2_student) ? '<span class="text-success">'.$week->meetings[0]->meeting_midweek->ministry_item2_student->full_name.'</span>' : ' ' ?></b>
                    <b><?= ($week->meetings[0]->meeting_midweek->ministry_item2_assistant) ? '<span class="text-warning">(A: '.$week->meetings[0]->meeting_midweek->ministry_item2_assistant->full_name.')</span>' : ' ' ?></b>
                </p>
            </div>

            <div style="border-bottom:1px solid #dddddd;">
                <p><b><?= $week->meetings[0]->meeting_midweek->ministry_item3_type->name ?></b>: (4 min.)</p>
                <p style="margin-bottom:4px;width:100%;text-align:right;">
                    <b><?= ($week->meetings[0]->meeting_midweek->ministry_item3_student) ? '<span class="text-success">'.$week->meetings[0]->meeting_midweek->ministry_item3_student->full_name.'</span>' : ' ' ?></b>
                    <b><?= ($week->meetings[0]->meeting_midweek->ministry_item3_assistant) ? '<span class="text-warning">(A: '.$week->meetings[0]->meeting_midweek->ministry_item3_assistant->full_name.')</span>' : ' ' ?></b>
                </p>
            </div>

            <?php endif; ?>
            
        </td>
        
        <td style="width:33%;padding:8px; vertical-align: top;">
            Column 3
        </td>
    </tr>
</table>
<?php
endforeach;
?>


