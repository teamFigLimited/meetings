<?= $this->Html->css('spacelab.min', ['fullBase' => true]) ?>
<?= $this->Html->css('css', ['fullBase' => true]) ?>
<?= $this->fetch('css') ?>
<style>
    @page { margin: 0;}
    body { font-size:12px;}
    .page-header{padding:0 0 6px 0;margin:0 0 2px 0;}
    h3 {padding: 0;margin: 0;}
    h4 { border-bottom-color: rgb(238, 238, 238); border-bottom-style: solid; border-bottom-width: 1px; }
    h6 { padding: 4px 8px 2px 8px;margin:0;}
    p {margin:0;padding:0;}
    .alert {padding:2px 4px;}
    .alert-treasures, .alert-ministry, .alert-living{padding:0;margin:0;}
    .text-success{margin:0;padding:0;}
    table {width:100%;padding:0 8px;margin:0;}
    tr, td {margin:0;padding:0;}
    
    .col1 {padding-right:4px;width:4%;text-align:center;}
    .col2 {width:54%;}
    .col3 {width:16%;font-size:11px;}
    .col4 {width:22%;font-size:11px;}
    .col5 {width:4%;font-size:10px;}
    
    .col1 > .alert-treasures {background-color:#4D565D !important;color:#fff !important;}
    .col1 > .alert-ministry {background-color: #B2731D !important; color:#fff !important;}
    .col1 > .alert-living {background-color:#82031D !important;color:#fff !important;}
    
    .border-bottom-treasures {border-bottom:1px solid #4D565D;}
    .border-bottom-ministry {border-bottom:1px solid #B2731D;}
    .border-bottom-living {border-bottom:1px solid #82031D;}
</style>

<div class="page-header">
    <h3>Our Christian Life and Ministry</h3>
</div>

<?php
$weekCount = 0;
$isThereAnAssembly = false;

foreach($month->weeks as $week):
    
    if(in_array($week->meetings[0]->meeting_week_type_id, [3, 4])):
        $isThereAnAssembly = true;
    endif;
    
    if(((int)(++$weekCount % 5) === 0) && !$isThereAnAssembly):
?>
</table>
<table style="page-break-before: always;">
<?php
    endif;
?>

<table>
    
    <!-- week and material -->
    <tr>
        <td colspan="5">
            <div class="alert" style="margin:3px 0 0 0;background-color:#<?= $month->colour1 ?> !important;color:#fff !important;"><?= $week->full_date ?>
                <span class="text-uppercase" style="float:right;color:#fff !important;padding:0;margin:0;width:50%;text-align:right;"><?= $week->week_midweek->treasures_material ?></span>
            </div>
        </td>
    </tr>
    
    <?php if(in_array($week->meetings[0]->meeting_week_type_id, [1, 2])): ?>
        
    <!-- Song, Prayer and Opening Comments -->
    <tr>
        <td class="col1">&nbsp;</td>
        <td class="col2">Chairman: Introduce Song <?= $week->meetings[0]->meeting_midweek->opening_song_number ?> and Prayer. Opening Comments</td>
        <td class="col3"><b><?= ($week->meetings[0]->meeting_midweek->chairman) ? $week->meetings[0]->meeting_midweek->chairman->full_name : '&nbsp;' ?></b></td>
        <td class="col4"><?= ($week->meetings[0]->meeting_midweek->opening_prayer) ? 'Prayer: '.$week->meetings[0]->meeting_midweek->opening_prayer->full_name : '&nbsp;' ?></td>
        <td class="col5"><?= $this->Time->format($week->meetings[0]->installation->time_midweek->addMinutes(8), 'h:mm') ?></td>
    </tr>
    
    <!-- Treasures Talk -->
    <tr>
        <td class="col1"><div class="alert alert-treasures">Tr</div></td>
        <td><?= $week->week_midweek->treasures_talk_theme ?></td>
        <td colspan="2"><b><?= ($week->meetings[0]->meeting_midweek->treasures_talk_individual) ? $week->meetings[0]->meeting_midweek->treasures_talk_individual->full_name : '&nbsp;' ?></b></td>
        <td class="col5"><?= $this->Time->format($week->meetings[0]->installation->time_midweek->addMinutes(10), 'h:mm') ?></td>
    </tr>
    
    <!-- Gems -->
    <tr>
        <td class="col1"><div class="alert alert-treasures">Tr</div></td>
        <td>Digging For Spiritual Gems</td>
        <td colspan="2"><b><?= ($week->meetings[0]->meeting_midweek->treasures_gems_individual) ? $week->meetings[0]->meeting_midweek->treasures_gems_individual->full_name : '&nbsp;' ?></b></td>
        <td class="col5"><?= $this->Time->format($week->meetings[0]->installation->time_midweek->addMinutes(8), 'h:mm') ?></td>
    </tr>
    
    <!-- Bible Reading -->
    <tr>
        <td class="col1"><div class="alert alert-treasures">Tr</div></td>
        <td class="border-bottom-treasures">Bible Reading</td>
        <td class="border-bottom-treasures col3" colspan="2"><b><?= ($week->meetings[0]->meeting_midweek->treasures_reading_individual) ? $week->meetings[0]->meeting_midweek->treasures_reading_individual->full_name /*.' ('.$week->meetings[0]->meeting_midweek->treasures_reading_study->number.')'*/ : '&nbsp;' ?></b></td>
        <td class="border-bottom-treasures col5"><?= $this->Time->format($week->meetings[0]->installation->time_midweek->addMinutes(5), 'h:mm') ?>*</td>
    </tr>
    
    <?php if($week->week_midweek->is_week1 == 1): ?>
    <!-- Ministry Preferences -->
    <tr>
        <td class="col1"><div class="alert alert-ministry">Ap</div></td>
        <td class="border-bottom-ministry">Prepare This Month's Presentations:</td>
        <td class="border-bottom-ministry" colspan="2"><b><?= ($week->meetings[0]->meeting_midweek->ministry_presentation_individual) ? $week->meetings[0]->meeting_midweek->ministry_presentation_individual->full_name : ' ' ?></b></td>
        <td class="border-bottom-ministry col5"><?= $this->Time->format($week->meetings[0]->installation->time_midweek->addMinutes(15), 'h:mm') ?></td>
    </tr>
    
    <?php else: ?>
    
    <!-- Talk 1 -->
    <tr>
        <td class="col1"><div class="alert alert-ministry">Ap</div></td>
        <td><?= $week->meetings[0]->meeting_midweek->ministry_item1_type->name ?></td>
        <td class="col3"><b><?= ($week->meetings[0]->meeting_midweek->ministry_item1_student) ? $week->meetings[0]->meeting_midweek->ministry_item1_student->full_name /*.' ('.$week->meetings[0]->meeting_midweek->ministry_item1_study->number.')'*/ : ' ' ?></b></td>
        <td><?= ($week->meetings[0]->meeting_midweek->ministry_item1_assistant) ? 'Assistant: '.$week->meetings[0]->meeting_midweek->ministry_item1_assistant->full_name : ' ' ?></td>
    </tr>
    
    <!-- Talk 2 -->
    <tr>
        <td class="col1"><div class="alert alert-ministry">Ap</div></td>
        <td><?= $week->meetings[0]->meeting_midweek->ministry_item2_type->name ?></td>
        <td class="col3"><b><?= ($week->meetings[0]->meeting_midweek->ministry_item2_student) ? $week->meetings[0]->meeting_midweek->ministry_item2_student->full_name /*.' ('.$week->meetings[0]->meeting_midweek->ministry_item2_study->number.')' */ : ' ' ?></b></td>
        <td><?= ($week->meetings[0]->meeting_midweek->ministry_item2_assistant) ? 'Assistant: '.$week->meetings[0]->meeting_midweek->ministry_item2_assistant->full_name : ' ' ?></td>
    </tr>
    
    <!-- Talk 3 -->
    <tr>
        <td class="col1"><div class="alert alert-ministry">Ap</div></td>
        <td class="border-bottom-ministry"><?= $week->meetings[0]->meeting_midweek->ministry_item3_type->name ?></td>
        <td class="border-bottom-ministry col3"><b><?= ($week->meetings[0]->meeting_midweek->ministry_item3_student) ? $week->meetings[0]->meeting_midweek->ministry_item3_student->full_name /*.' ('.$week->meetings[0]->meeting_midweek->ministry_item3_study->number.')'*/ : ' ' ?></b></td>
        <td class="border-bottom-ministry"><?= ($week->meetings[0]->meeting_midweek->ministry_item3_assistant) ? 'Assistant: '.$week->meetings[0]->meeting_midweek->ministry_item3_assistant->full_name : ' ' ?></td>
        <td class="border-bottom-ministry col5"><?= $this->Time->format($week->meetings[0]->installation->time_midweek->addMinutes(15), 'h:mm') ?>*</td>
    </tr>
    
    <?php endif; ?>
    
    <!-- Middle Song -->
    <tr>
        <td class="col1"> </td>
        <td>Song <?= $week->meetings[0]->meeting_midweek->middle_song_number ?></td>
        <td></td>
    </tr>
    
    <!-- LIVING ITEM 1 -->
    <tr>
        <td class="col1"><div class="alert alert-living">Li</div></td>
        <td><?= $week->meetings[0]->meeting_midweek->living_item1_theme ?></td>
        <td colspan="2"><b><?= ($week->meetings[0]->meeting_midweek->living_item1_individual) ? $week->meetings[0]->meeting_midweek->living_item1_individual->full_name : ' ' ?></b></td>
        <td class="col5"><?= $this->Time->format($week->meetings[0]->installation->time_midweek->addMinutes($week->meetings[0]->meeting_midweek->living_item1_minutes+5), 'h:mm') ?></td>
    </tr>

    <!-- LIVING ITEM 2 -->
    <?php if($week->meetings[0]->meeting_midweek->living_item2_theme): ?>
    <tr>
        <td class="col1"><div class="alert alert-living">Li</div></td>
        <td><?= $week->meetings[0]->meeting_midweek->living_item2_theme ?></td>
        <td colspan="2"><b><?= ($week->meetings[0]->meeting_midweek->living_item2_individual) ? $week->meetings[0]->meeting_midweek->living_item2_individual->full_name : ' ' ?></b></td>
        <td class="col5"><?= $this->Time->format($week->meetings[0]->installation->time_midweek->addMinutes($week->meetings[0]->meeting_midweek->living_item2_minutes), 'h:mm') ?></td>
    </tr>
    <?php endif; ?>    
    
    <?php if($week->meetings[0]->meeting_week_type_id === 1): ?>
    <!-- Normal Week -->
    <!-- CBS -->
    <tr>
        <td class="col1"><div class="alert alert-living" style="padding:0px 10px;">Li</div></td>
        <td>Congregation Bible Study</td>
        <td><b><?= ($week->meetings[0]->meeting_midweek->living_cbs_conductor) ? $week->meetings[0]->meeting_midweek->living_cbs_conductor->full_name : ' ' ?></b></td>
        <td><?= ($week->meetings[0]->meeting_midweek->living_cbs_reader) ? 'Reader: '.$week->meetings[0]->meeting_midweek->living_cbs_reader->full_name : ' ' ?></td>
        <td class="col5"><?= $this->Time->format($week->meetings[0]->installation->time_midweek->addMinutes(30), 'h:mm') ?></td>
    </tr>
    <!-- Chairman's Review -->
    <tr>
        <td class="col1"></td>
        <td>Review followed by Preview of next week. Song <?= $week->meetings[0]->meeting_midweek->closing_song_number ?> and Prayer</td>
        <td><b><?= ($week->meetings[0]->meeting_midweek->chairman) ? $week->meetings[0]->meeting_midweek->chairman->full_name : ' ' ?></b></td>
        <td><?= ($week->meetings[0]->meeting_midweek->closing_prayer) ? 'Prayer: '.$week->meetings[0]->meeting_midweek->closing_prayer->full_name : ' ' ?></td>
        <td class="col5"><?= $this->Time->format($week->meetings[0]->installation->time_midweek->addMinutes(3), 'h:mm') ?></td>
    </tr>

    <?php elseif($week->meetings[0]->meeting_week_type_id === 2): ?>
    <!-- CO Visit Week -->
    <!-- Chairmans Review -->
    <tr>
        <td class="col1"></td>
        <td>Review followed by Preview of next week.</td>
        <td colspan="2"><b><?= ($week->meetings[0]->meeting_midweek->chairman) ? $week->meetings[0]->meeting_midweek->chairman->full_name : ' ' ?></b></td>
        <td class="col5"><?= $this->Time->format($week->meetings[0]->installation->time_midweek->addMinutes(3), 'h:mm') ?></td>
    </tr>

    <!-- CO Talk -->
    <tr>
        <td class="col1"><?= $week->meetings[0]->meetingMidweek->co_talk_theme ?></td>
        <td><b><?= $week->meetings[0]->installation->circuit_overseer_name ?></b></td>
        <td class="col5"><?= $this->Time->format($week->meetings[0]->installation->time_midweek->addMinutes(30), 'h:mm') ?></td>
    </tr>
    
    <!-- Closing Song and Prayer -->
    <tr>
        <td class="col1"></td>
        <td colspan="2">Song <?= $week->meetings[0]->meeting_midweek->closing_song_number ?> and Prayer</td>
        <td><?= ($week->meetings[0]->meeting_midweek->closing_prayer) ? 'Prayer: '.$week->meetings[0]->meeting_midweek->closing_prayer->full_name : ' ' ?></td>
    </tr>
    <?php endif; ?>
    
    
    <?php else: ?> 
    <!-- MeetingWeekType -->
    
    <tr>
        <td class="col1"></td>
        <td colspan="3"><?= $week->meetings[0]->meeting_week_type->name ?></td>
    </tr>
    
    <?php endif; ?>
    
</table>

<?php
endforeach;
?>

<p style="margin-top:4px;font-size:10px;"><b>*</b> Timings shown are finish times and are for the benefit of the Chairman and those giving Talks. Students with Bible Readings or Ministry Assignments are timed separately.</p>

<?php
if($debug):
    debug(json_encode($month, JSON_PRETTY_PRINT));
endif;
?>