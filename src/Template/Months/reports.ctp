<div class="page-header">
    <h1>Reports <small class="text-uppercase">by month</small></h1>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Christian Life and Ministry</h4>
            </div>
            <div class="list-group">
                <a class="list-group-item disabled">PDF Reports</a>
                <?php foreach($months as $month): ?>
                <?= $this->Html->link($month->date_first->format('F Y'), ['controller' => 'months', 'action' => 'clm_report', $month->id, 'month_view', '_ext' => 'pdf'], ['class' => 'list-group-item', 'target' => '_blank']) ?>
                <?php endforeach; ?>
            </div>
            <div class="panel-body alert alert-info">
                <p><b>Please note:</b> PDF reports can take up to 30 seconds to run so please be patient. Thank you.</p>
            </div>
        </div>
    </div>
</div>

<?php
if($debug):
    debug(json_encode($months, JSON_PRETTY_PRINT));
endif;
?>