<div class="page-header">
    <h1>Our Christian Life and Ministry</h1>
</div>


<table style="width:100%;margin-bottom:0;padding-bottom:0;margin-top:0;padding-top:0;">
    <tr>
        <td style="width:33%;padding:2px 8px;"><div class="alert alert-treasures">Treasures From Gods Word</div></td>
        <td style="width:34%;padding:2px 8px;"><div class="alert alert-ministry">Applying Ourselves to the Ministry</div></td>
        <td style="width:33%;padding:2px 8px;"><div class="alert alert-living">Living as Christians</div></td>
    </tr>
</table
<?php
foreach($month->weeks as $week):
?>
<table style="width:100%;">
    <tr>
        <td style="width:33%;padding:2px 8px;">
            
            <div style="border-bottom:1px solid #dddddd;">
                <p><?= $week->meetings[0]->meeting_midweek->treasures_talk_theme ?></p>
                <p style="margin-bottom:4px;width:100%;text-align:right;">
                    <b><?= ($week->meetings[0]->meeting_midweek->treasures_talk_individual) ? $week->meetings[0]->meeting_midweek->treasures_talk_individual->full_name : '' ?></b>
                </p>
            </div>
            
            <div style="border-bottom:1px solid #dddddd;">
                <p>Digging For Spiritual Gems</p>
                <p style="margin-bottom:4px;width:100%;text-align:right;">
                    <b><?= ($week->meetings[0]->meeting_midweek->treasures_gems_individual) ? $week->meetings[0]->meeting_midweek->treasures_gems_individual->full_name : '' ?></b>
                </p>
            </div>
            
            <div style="border-bottom:1px solid #dddddd;">
                <p>Bible Reading</p>
                <p style="margin-bottom:4px;width:100%;text-align:right;">
                    <b><?= ($week->meetings[0]->meeting_midweek->treasures_reading_individual) ? $week->meetings[0]->meeting_midweek->treasures_reading_individual->full_name : '' ?></b>
                </p>
            </div>
            
        </td>
        
        <td style="width:34%;padding:8px;">
            
            <?php if($week->meetings[0]->meeting_midweek->is_week1 == 1): ?>

            <li class="list-group-item">
                <b>Prepare This Month’s Presentations</b>: (15 min.)
                <?= $this->Text->nl2p($week->meetings[0]->meeting_midweek->ministry_presentation_material) ?>
                <p class="text-right"><i><?= ($week->meetings[0]->meeting_midweek->ministry_presentation_individual) ? '<span class="text-success">'.$week->meetings[0]->meeting_midweek->ministry_presentation_individual->full_name.'</span>' : '<span class="text-danger"><i>not set</i></span>' ?></i></p>
            </li>

            <?php else: ?>

            <li class="list-group-item">
                <b><?= $meeting->meeting_midweek->ministry_item1_type->name ?></b>: (2 min.)
                <?= $this->Text->nl2p($week->meetings[0]->meeting_midweek->ministry_item1_material) ?>
                <p class="text-right fullname"><i><?= ($meeting->meeting_midweek->ministry_item1_student) ? '<span class="text-success"><b>'.$meeting->meeting_midweek->ministry_item1_student->full_name.'</b></span>' : '<span class="text-danger"><i>not set</i></span>' ?></i></p>
                <p class="text-right"><i><?= ($meeting->meeting_midweek->ministry_item1_assistant) ? '<span class="text-warning">'.$meeting->meeting_midweek->ministry_item1_assistant->full_name.'</span>' : '<span class="text-danger"><i>not set</i></span>' ?></i></p>
            </li>
            <li class="list-group-item">
                <b><?= $meeting->meeting_midweek->ministry_item2_type->name ?></b>: (4 min.)
                <?= $this->Text->nl2p($week->meetings[0]->meeting_midweek->ministry_item2_material) ?>
                <p class="text-right fullname"><i><?= ($meeting->meeting_midweek->ministry_item2_student) ? '<span class="text-success"><b>'.$meeting->meeting_midweek->ministry_item2_student->full_name.'</b></span>' : '<span class="text-danger"><i>not set</i></span>' ?></i></p>
                <p class="text-right"><i><?= ($meeting->meeting_midweek->ministry_item2_assistant) ? '<span class="text-warning">'.$meeting->meeting_midweek->ministry_item2_assistant->full_name.'</span>' : '<span class="text-danger"><i>not set</i></span>' ?></i></p>
            </li>
            <li class="list-group-item">
                <b><?= $meeting->meeting_midweek->ministry_item3_type->name ?></b>: (6 min.)
                <?= $this->Text->nl2p($meeting->meeting_midweek->ministry_item3_material) ?>
                <p class="text-right fullname"><i><?= ($meeting->meeting_midweek->ministry_item3_student) ? '<span class="text-success"><b>'.$meeting->meeting_midweek->ministry_item3_student->full_name.'</b></span>' : '<span class="text-danger"><i>not set</i></span>' ?></i></p>
                <p class="text-right"><i><?= ($meeting->meeting_midweek->ministry_item3_assistant) ? '<span class="text-warning">'.$meeting->meeting_midweek->ministry_item3_assistant->full_name.'</span>' : '<span class="text-danger"><i>not set</i></span>' ?></i></p>
            </li>

            <?php endif; ?>
            
        </td>
        
        <td style="width:33%;padding:8px;">
            Column 3
        </td>
    </tr>
</table>
<?php
endforeach;
?>

<?php
if($debug):
    debug(json_encode($month, JSON_PRETTY_PRINT));
endif;
?>