<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $month->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $month->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Months'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Weeks'), ['controller' => 'Weeks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Week'), ['controller' => 'Weeks', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="months form large-9 medium-8 columns content">
    <?= $this->Form->create($month) ?>
    <fieldset>
        <legend><?= __('Edit Month') ?></legend>
        <?php
            echo $this->Form->input('date_month', ['empty' => true, 'default' => '']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
