<div class="page-header">
    <h1>Add a Congregation</h1>
    <p>Any Congregations you add are visible only to you and are not shared with any other user.</p>
</div>

<?= $this->Form->create($congregation) ?>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <ul class="list-group">
                <li class="list-group-item"><?= $this->Form->input('name', ['class' => 'form-control']) ?></li>
                <li class="list-group-item"><?= $this->Form->button(__('Add Congregation'), ['class' => 'form-control btn btn-success']) ?></li>
            </ul>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>

<?php
if($debug):
    debug(json_encode($congregation, JSON_PRETTY_PRINT));
endif;
?>

