<?= $this->Html->css('spacelab.min', ['fullBase' => true]) ?>
<?= $this->Html->css('css', ['fullBase' => true]) ?>
<?= $this->fetch('css') ?>
<style>
    @page { margin: 0;}
    body { margin: 10mm 5mm 0 5mm; font-size:10px;}
    h2 {padding: 0 8px;margin: 0;}
    h4 { border-bottom-color: rgb(238, 238, 238); border-bottom-style: solid; border-bottom-width: 1px; }
    h6 { padding: 4px 8px 2px 8px;margin:0;}
    p {margin:0;padding:0;}
    .page-header {margin: 5px 0 5px 0;}
    .alert {font-size:10px;margin:0;padding:0;text-align:center;}
    .alert-treasures, .alert-ministry, .alert-living{padding:0;margin:0;}
    .text-success{margin:0;padding:0;}
    table {padding:0;margin:0;}
    tr, td {margin:0;padding:0;vertical-align:top;}
    th, td {text-align:center;}
    
</style>

<div class="page-header">
    <h2>Our Christian Life and Ministry <small class="text-uppercase"><?= $students->count() ?> Students</small></h2>
</div>

<table class="table">
    <tr>
        <th style="padding:0;margin:0;max-width:0;"></th>
        <th style="text-align:left;min-width:28%;width:28%;">Student</th>
        <th style="width:12%;">Bible Reading</th>
        <th style="width:12%;">Initial Call</th>
        <th style="width:12%;">Return Visit</th>
        <th style="width:12%;">Bible Study</th>
        <th style="width:12%;">Assistant</th>
        <th style="width:12%;">Volunteer</th>
    </tr>
<?php
$studentCount = 0;
foreach($students as $student):
    $studentCount++;

    //  page break and dompdf glitch
    if($studentCount % 30 == 0):
        echo '</table><table class="table" style="page-break-before: always;"><tr><th style="padding:0;margin:0;max-width:0;"></th>';
    else:
        echo '<tr>';
    endif;
?>  
        <td style="text-align:left;min-width:28%;width:28%;"><?= $student->full_name ?></td>
        <td style="width:12%;"><?= (isset($student->privileges_abbrev[4])) ? 'Y' : '' ?></td>
        <td style="width:12%;"><?= (isset($student->privileges_abbrev[6])) ? 'Y' : '' ?></td>
        <td style="width:12%;"><?= (isset($student->privileges_abbrev[7])) ? 'Y' : '' ?></td>
        <td style="width:12%;"><?= (isset($student->privileges_abbrev[8])) ? 'Y' : '' ?></td>
        <td style="width:12%;"><?= (isset($student->privileges_abbrev[9])) ? 'Y' : '' ?></td>
        <td style="width:12%;"><?= (isset($student->privileges_abbrev[10])) ? 'Y' : '' ?></td>
    </tr>    
<?php
endforeach;
?>
</table>
