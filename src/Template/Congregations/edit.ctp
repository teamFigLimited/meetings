<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $congregation->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $congregation->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Congregations'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Installations'), ['controller' => 'Installations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Installation'), ['controller' => 'Installations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Individuals'), ['controller' => 'Individuals', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Individual'), ['controller' => 'Individuals', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="congregations form large-9 medium-8 columns content">
    <?= $this->Form->create($congregation) ?>
    <fieldset>
        <legend><?= __('Edit Congregation') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('installation_id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
