<table class="table table-striped">
    <tr>
        <th>Student</th>
        <th>Bible Reading</th>
        <th>Initial Call</th>
        <th>Return Visit</th>
        <th>Bible Study</th>
        <th>Assistant</th>
        <th>Volunteer</th>
    </tr>
    <?php foreach($students as $student): ?>
    <tr>
        <td><?= $student->full_name ?></td>
        <td><?= (isset($student->privileges_abbrev[4])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : '' ?></td>
        <td><?= (isset($student->privileges_abbrev[6])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : '' ?></td>
        <td><?= (isset($student->privileges_abbrev[7])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : '' ?></td>
        <td><?= (isset($student->privileges_abbrev[8])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : '' ?></td>
        <td><?= (isset($student->privileges_abbrev[9])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : '' ?></td>
        <td><?= (isset($student->privileges_abbrev[10])) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : '' ?></td>
    </tr>
    <?php endforeach; ?>
</table>
<?php
debug(json_encode($students, JSON_PRETTY_PRINT));
?>
