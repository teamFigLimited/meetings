<div class="page-header">
    <h3><?= $this->Html->link('<span class="glyphicon glyphicon-plus-sign pull-right" aria-hidden="true"></span>', ['controller' => 'congregations', 'action' => 'add'], ['escape' => false]) ?></h3>
    <h1>Congregations <small class="text-uppercase">for this Installation</small></h1>
</div>

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($congregations as $congregation): ?>
            <tr>
                <td><?= $this->Number->format($congregation->id) ?></td>
                <td><?= h($congregation->name) ?></td>
                <td><?= h($congregation->created) ?></td>
                <td><?= h($congregation->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $congregation->id], ['class' => 'btn btn-xs btn-info']) ?>
                    <!--<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $congregation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $congregation->id)]) ?>-->
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>


<?php
if($debug):
    debug(json_encode($congregations, JSON_PRETTY_PRINT));
endif;
?>

