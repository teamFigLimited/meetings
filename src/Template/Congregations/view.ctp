<div class="page-header">
    <?= ($congregation->installation->congregation_id == $congregation->id) ? ' <small class="pull-right">yes, this is your Congregation</small>' : '' ?>    
    <h1>Congregation View <small class="text-uppercase"><?= $congregation->name ?></small></h1>
</div>

<h3><?= $this->Html->link('<span class="glyphicon glyphicon-plus-sign pull-right" aria-hidden="true"></span>', ['controller' => 'individuals', 'action' => 'add', '?' => ['congregation_id' => $congregation->id]], ['escape' => false]) ?></h3>
<h2>Individuals</h2>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Appointment Id') ?></th>
                <th><?= __('Family Head Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($congregation->individuals as $individual): ?>
            <tr>
                <td><?= h($individual->id) ?></td>
                <td><?= h($individual->full_name) ?></td>
                <td><?= h($individual->appointment->abbrev) ?></td>
                <td><?= ($individual->family_head) ? $individual->family_head->full_name : '' ?></td>
                <td><?= $this->Html->link('view', ['controller' => 'individuals', 'action' => 'view', $individual->id], ['class' => 'btn btn-xs btn-info']) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php
if($debug):
    debug(json_encode($congregation, JSON_PRETTY_PRINT));
endif;
?>

