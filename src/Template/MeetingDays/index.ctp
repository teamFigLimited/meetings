<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Meeting Day'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="meetingDays index large-9 medium-8 columns content">
    <h3><?= __('Meeting Days') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('is_midweek') ?></th>
                <th><?= $this->Paginator->sort('is_weekend') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($meetingDays as $meetingDay): ?>
            <tr>
                <td><?= $this->Number->format($meetingDay->id) ?></td>
                <td><?= h($meetingDay->name) ?></td>
                <td><?= $this->Number->format($meetingDay->is_midweek) ?></td>
                <td><?= $this->Number->format($meetingDay->is_weekend) ?></td>
                <td><?= h($meetingDay->created) ?></td>
                <td><?= h($meetingDay->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $meetingDay->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $meetingDay->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $meetingDay->id], ['confirm' => __('Are you sure you want to delete # {0}?', $meetingDay->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
