<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Meeting Day'), ['action' => 'edit', $meetingDay->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Meeting Day'), ['action' => 'delete', $meetingDay->id], ['confirm' => __('Are you sure you want to delete # {0}?', $meetingDay->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Meeting Days'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Meeting Day'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="meetingDays view large-9 medium-8 columns content">
    <h3><?= h($meetingDay->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($meetingDay->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($meetingDay->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Midweek') ?></th>
            <td><?= $this->Number->format($meetingDay->is_midweek) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Weekend') ?></th>
            <td><?= $this->Number->format($meetingDay->is_weekend) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($meetingDay->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($meetingDay->modified) ?></tr>
        </tr>
    </table>
</div>
