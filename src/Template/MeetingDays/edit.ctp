<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $meetingDay->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $meetingDay->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Meeting Days'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="meetingDays form large-9 medium-8 columns content">
    <?= $this->Form->create($meetingDay) ?>
    <fieldset>
        <legend><?= __('Edit Meeting Day') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('is_midweek');
            echo $this->Form->input('is_weekend');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
