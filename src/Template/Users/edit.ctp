<div class="page-header">
    <h1>User Edit</h1>
</div>


<?= $this->Form->create($user, ['class' => 'form-signin']) ?>
<?= $this->Form->input('username', ['class' => 'form-control', 'placeholder' => 'Username', 'required', 'autofocus']) ?>
<?= $this->Form->input('email', ['class' => 'form-control', 'placeholder' => 'Email', 'type' => 'email']) ?>
<?= $this->Form->input('mobile', ['class' => 'form-control', 'placeholder' => 'Mobile Phone']) ?>
<?= $this->Form->button(__('Save Changes'), ['class' => 'btn btn-lg btn-success btn-block']) ?>
<?= $this->Form->end() ?>

<?= $this->Flash->render('auth') ?>

