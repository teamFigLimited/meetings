<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Installations'), ['controller' => 'Installations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Installation'), ['controller' => 'Installations', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Username') ?></th>
            <td><?= h($user->username) ?></td>
        </tr>
        <tr>
            <th><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($user->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($user->modified) ?></tr>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Installations') ?></h4>
        <?php if (!empty($user->installations)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Congregation Id') ?></th>
                <th><?= __('Congregation Name') ?></th>
                <th><?= __('Circuit Overseer Name') ?></th>
                <th><?= __('Next Midweek Time') ?></th>
                <th><?= __('Next Weekend Time') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->installations as $installations): ?>
            <tr>
                <td><?= h($installations->id) ?></td>
                <td><?= h($installations->user_id) ?></td>
                <td><?= h($installations->congregation_id) ?></td>
                <td><?= h($installations->congregation_name) ?></td>
                <td><?= h($installations->circuit_overseer_name) ?></td>
                <td><?= h($installations->next_midweek_time) ?></td>
                <td><?= h($installations->next_weekend_time) ?></td>
                <td><?= h($installations->created) ?></td>
                <td><?= h($installations->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Installations', 'action' => 'view', $installations->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Installations', 'action' => 'edit', $installations->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Installations', 'action' => 'delete', $installations->id], ['confirm' => __('Are you sure you want to delete # {0}?', $installations->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
