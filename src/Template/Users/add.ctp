<div class="page-header">
    <h1>User Registration</h1>
</div>

<?= $this->Form->create($user, ['class' => 'form-signin']) ?>
<?= $this->Form->input('username', ['class' => 'form-control', 'placeholder' => 'Username', 'required', 'autofocus']) ?>
<?= $this->Form->input('password', ['class' => 'form-control', 'placeholder' => 'Password', 'required']) ?>
<?= $this->Form->input('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Password', 'required', 'type' => 'password', 'error' => ['flash' => ['class' => 'alert alert-danger']]]) ?>
<?= $this->Form->input('email', ['class' => 'form-control', 'placeholder' => 'Email', 'required', 'type' => 'email']) ?>
<?= $this->Form->button(__('Register'), ['class' => 'btn btn-lg btn-success btn-block']) ?>
<?= $this->Form->end() ?>

<?= $this->Flash->render('auth') ?>

<div class="row">
    <div class="col-xs-12">
        <h3>Slack Discussion</h3>
        <p>As part of the registration process, we ask that you join our <a href="https://fig-meetings.slack.com">Slack discussion</a>. It's completely optional but, in reality, it's probably the fastest way for you to get a response from someone. Also we greatly appreciate your feedback and we feel that this channel is a good way to receive it.</p>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <h3>Terms of Use</h3>
        <p>We ask all who register an account with <kbd>fig</kbd> Limited to abide by the following terms and conditions (by registering an account you are agreeing to the terms and conditions stated below):</p>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <ol>
            <li><kbd>fig</kbd> Limited is the owner of the fig.limited domain and any subdomains that end in fig.limited. Any software or website that runs under this domain name is the property of <kbd>fig</kbd> Limited.</li>
            <li><kbd>fig</kbd> Limited provides software under the fig.limited domain with the following restrictions:
                <ol>
                    <li><kbd>fig</kbd> limited accepts no liability whatsoever for any consequential actions that result in a loss to the User by using any software under the fig.limited domain. Unless otherwise stated, all such software is to be used entirely at the User's risk.</li>
                    <li>Users are expected to use the provided interface to use the software. Trying to bypass the interface is considered a breach of these terms and conditions.</li>
                    <li><kbd>fig</kbd> limited make no charge for the use of its theocratic software. It is supported by optional, voluntary donations and by revenue from its commercial software.</li>
                </ol>
            </li>
            <li><kbd>fig</kbd> limited is in no way associated with the <i>International Bible Students Association (I.B.S.A.) of Britain</i> or any of its agents or partners and, as such, is not endorsed in any way by the I.B.S.A.</li>
        </ol>
    </div>
</div>

<?php
if($debug):
    debug(json_encode($user, JSON_PRETTY_PRINT));
endif;
?>
