<?php
$this->assign('title', 'Ecclesias Congregation Software :: User Login');
?>

<?= $this->Form->create(null, ['class' => 'form-signin']) ?>
	<h2 class="form-signin-heading">Please Log In</h2>
	<?= $this->Form->input('username', ['class' => 'form-control', 'placeholder' => 'Username', 'required', 'autofocus']) ?>
	<?= $this->Form->input('password', ['class' => 'form-control', 'placeholder' => 'Password', 'required']) ?>
	
	<?= $this->Form->button(__('Log In'), ['class' => 'btn btn-lg btn-success btn-block']) ?>
<?= $this->Form->end() ?>



<?= $this->Flash->render('auth') ?>
