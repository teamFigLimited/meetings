<?php
use Cake\Core\Configure;

require '../vendor/autoload.php';
define('DOMPDF_ENABLE_AUTOLOAD', false);
require_once("../vendor/dompdf/dompdf/dompdf_config.inc.php");
//spl_autoload_register('DOMPDF_autoload'); 
$dompdf = new DOMPDF(); 
$dompdf->set_paper('A4');
$dompdf->load_html(utf8_decode($this->fetch('content')), Configure::read('App.encoding'));
$dompdf->render();
echo $dompdf->output();