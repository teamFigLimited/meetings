<!DOCTYPE html>
<html>
    <head>
        {{ Html.charset()|raw }}
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            {{ _view.fetch('title')|raw }}
        </title>
        {{ Html.meta('icon')|raw }}

        {{ Html.css('spacelab.min')|raw }}
        {{ Html.css('sticky-footer')|raw }}
        {{ Html.css('login')|raw }}
        {{ Html.css('css')|raw }}

        {{ _view.fetch('meta')|raw }}
        {{ _view.fetch('css')|raw }}
        {{ _view.fetch('script')|raw }}

    </head>
    <body {{ ((debug) ? ' style="background: repeating-linear-gradient(135deg, #ffeeff, #ffeeff 4px, #ffffff 4px, #ffffff 8px);"' : '')|raw }}>


        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-70241209-2', 'auto');
        ga('send', 'pageview');
        </script>

        {% if currentInstallation is not empty %}
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    {{ Html.link(Html.image('fig-300-purple', {'style': 'width:30px;'}), {'controller': 'pages', 'action': 'display', 0: 'home'}, {'class': 'navbar-brand', 'escape': false})|raw }}
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>{{ Html.link(currentInstallation.congregation.name, {'controller': 'installations', 'action': 'view', 0: currentInstallation.id})|raw }}</li>
                        <li>{{ Html.link('Meetings', {'controller': 'meetings', 'action': 'index'})|raw }}</li>
                        <li>{{ Html.link('Individuals', {'controller': 'individuals', 'action': 'index'})|raw }}</li>
                        <li>{{ Html.link('Congregations', {'controller': 'congregations', 'action': 'index'})|raw }}</li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reports <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li>{{ Html.link('by Month', {'controller': 'months', 'action': 'reports'})|raw }}</li>
                                <li>{{ Html.link('by Midweek Meeting', {'controller': 'meeting_midweeks', 'action': 'reports'})|raw }}</li>
                                <li>{{ Html.link('by Congregation', {'controller': 'congregations', 'action': 'reports'})|raw }}</li>
                            </ul>
                        </li>

                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ currentUser.username }} <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                              <li>{{ Html.link('Meetings Settings', {'controller': 'installations', 'action': 'settings', 0: currentInstallation.id })|raw }}</li>
                              <li>{{ Html.link('User Settings', {'controller': 'users', 'action': 'edit', 0: currentUser.id })|raw }}</li>
                              <li>{{ Html.link('Fig Limited', 'https://www.fig.limited/')|raw }}</li>
                              <li role="separator" class="divider"></li>
                              <li>{{ Html.link('Log Out', {'controller': 'users', 'action': 'logout'})|raw }}</li>
                          </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        {% else %}
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              {{ Html.link(Html.image('fig-300-purple', {'style': 'width:30px;'}), {'controller': 'pages', 'action': 'display', 0: 'home'}, {'class': 'navbar-brand', 'escape': false})|raw }}
            </div>
          </div>
        </nav>
        {% endif %}

        <div id="container" class="container">
            <div id="content">

                <!-- <div class="row">
                    <div class="col-xs-12">

                        <div class="alert alert-danger" role="alert">
                            <b>Important!</b> Please make a note of the following information:
                            <p>Username: {{ currentUser.username }}</p>
                            <p>Transfer Code: {{ currentUser.transfer_code }}</p>
                            <p>More details will follow soon but essentially we are moving Fig Meetings to a more robust framework. You will need the above details to migrate your existing data over.</p>
                            <p>Also note that this is a separate excercise from the recent notice regarding Fig Congregation. That is still going ahead separately.</p>
                        </div>
                    </div>
                </div> -->


                {{ Flash.render('auth')|raw }}
                {{ Flash.render()|raw }}
                {{ _view.fetch('content')|raw }}
            </div>
        </div>

        <footer class="footer">
            <div class="container text-center">
                {{ Html.link('Home', {'controller': 'pages', 'action': 'display', 0: 'home'}, {'class': 'footer-item'})|raw }} |
		{{ Html.link('Support', {'controller': 'pages', 'action': 'display', 0: 'support'}, {'class': 'footer-item'})|raw }} |
		{{ Html.link('Terms', {'controller': 'pages', 'action': 'display', 0: 'terms'}, {'class': 'footer-item'})|raw }} |
		{{ Html.link('Technical Setup', {'controller': 'pages', 'action': 'display', 0: 'technical'}, {'class': 'footer-item'})|raw }}
            </div>
            {% if currentUser.is_admin == 1 %}
                <div class="container text-center">
                    {{ Html.link('Months', {'controller': 'months', 'action': 'index'}, {'class': 'footer-item'})|raw }} |
                    {{ Html.link('Weeks', {'controller': 'weeks', 'action': 'index'}, {'class': 'footer-item'})|raw }} |
                    {{ Html.link('Songs', {'controller': 'songs', 'action': 'index'}, {'class': 'footer-item'})|raw }} |
                    {{ Html.link('Outlines', {'controller': 'outlines', 'action': 'index'}, {'class': 'footer-item'})|raw }} |
                    {{ Html.link('Appointments', {'controller': 'appointments', 'action': 'index'}, {'class': 'footer-item'})|raw }} |
                    {{ Html.link('Genders', {'controller': 'genders', 'action': 'index'}, {'class': 'footer-item'})|raw }}
                </div>
            {% endif %}
        </footer>
        <script src="{{ www }}js/jquery-2.1.4.min.js"></script>
        <script src="{{ www }}js/jquery.form.js"></script>
        <script src="{{ www }}js/bootstrap.min.js"></script>
        <script src="{{ www }}js/js.js"></script>
    </body>
</html>
