<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Privilege'), ['action' => 'edit', $privilege->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Privilege'), ['action' => 'delete', $privilege->id], ['confirm' => __('Are you sure you want to delete # {0}?', $privilege->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Privileges'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Privilege'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="privileges view large-9 medium-8 columns content">
    <h3><?= h($privilege->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($privilege->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($privilege->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Brother') ?></th>
            <td><?= $this->Number->format($privilege->is_brother) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Qms') ?></th>
            <td><?= $this->Number->format($privilege->is_qms) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Elder') ?></th>
            <td><?= $this->Number->format($privilege->is_elder) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($privilege->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($privilege->modified) ?></tr>
        </tr>
    </table>
</div>
