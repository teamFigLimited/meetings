<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Privileges'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="privileges form large-9 medium-8 columns content">
    <?= $this->Form->create($privilege) ?>
    <fieldset>
        <legend><?= __('Add Privilege') ?></legend>
        <?php
            echo $this->Form->input('privilege_type_id');
            echo $this->Form->input('name');
            echo $this->Form->input('is_sister', ['options' => [0 => 'No', 1 => 'Yes']]);
            echo $this->Form->input('is_brother', ['options' => [0 => 'No', 1 => 'Yes']]);
            echo $this->Form->input('is_qms', ['options' => [0 => 'No', 1 => 'Yes']]);
            echo $this->Form->input('is_elder', ['options' => [0 => 'No', 1 => 'Yes']]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
