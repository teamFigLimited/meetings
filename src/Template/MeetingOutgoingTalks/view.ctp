<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Meeting Outgoing Talk'), ['action' => 'edit', $meetingOutgoingTalk->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Meeting Outgoing Talk'), ['action' => 'delete', $meetingOutgoingTalk->id], ['confirm' => __('Are you sure you want to delete # {0}?', $meetingOutgoingTalk->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Meeting Outgoing Talks'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Meeting Outgoing Talk'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Meetings'), ['controller' => 'Meetings', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Meeting'), ['controller' => 'Meetings', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Congregations'), ['controller' => 'Congregations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Congregation'), ['controller' => 'Congregations', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Outlines'), ['controller' => 'Outlines', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Outline'), ['controller' => 'Outlines', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="meetingOutgoingTalks view large-9 medium-8 columns content">
    <h3><?= h($meetingOutgoingTalk->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Meeting') ?></th>
            <td><?= $meetingOutgoingTalk->has('meeting') ? $this->Html->link($meetingOutgoingTalk->meeting->id, ['controller' => 'Meetings', 'action' => 'view', $meetingOutgoingTalk->meeting->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Congregation') ?></th>
            <td><?= $meetingOutgoingTalk->has('congregation') ? $this->Html->link($meetingOutgoingTalk->congregation->name, ['controller' => 'Congregations', 'action' => 'view', $meetingOutgoingTalk->congregation->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Outline') ?></th>
            <td><?= $meetingOutgoingTalk->has('outline') ? $this->Html->link($meetingOutgoingTalk->outline->name, ['controller' => 'Outlines', 'action' => 'view', $meetingOutgoingTalk->outline->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($meetingOutgoingTalk->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Speaker Id') ?></th>
            <td><?= $this->Number->format($meetingOutgoingTalk->speaker_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($meetingOutgoingTalk->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($meetingOutgoingTalk->modified) ?></td>
        </tr>
    </table>
</div>
