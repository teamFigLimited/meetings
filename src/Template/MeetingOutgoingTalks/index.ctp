<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Meeting Outgoing Talk'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Meetings'), ['controller' => 'Meetings', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Meeting'), ['controller' => 'Meetings', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Congregations'), ['controller' => 'Congregations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Congregation'), ['controller' => 'Congregations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Outlines'), ['controller' => 'Outlines', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Outline'), ['controller' => 'Outlines', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="meetingOutgoingTalks index large-9 medium-8 columns content">
    <h3><?= __('Meeting Outgoing Talks') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('meeting_id') ?></th>
                <th><?= $this->Paginator->sort('speaker_id') ?></th>
                <th><?= $this->Paginator->sort('congregation_id') ?></th>
                <th><?= $this->Paginator->sort('outline_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($meetingOutgoingTalks as $meetingOutgoingTalk): ?>
            <tr>
                <td><?= $this->Number->format($meetingOutgoingTalk->id) ?></td>
                <td><?= $meetingOutgoingTalk->has('meeting') ? $this->Html->link($meetingOutgoingTalk->meeting->id, ['controller' => 'Meetings', 'action' => 'view', $meetingOutgoingTalk->meeting->id]) : '' ?></td>
                <td><?= $this->Number->format($meetingOutgoingTalk->speaker_id) ?></td>
                <td><?= $meetingOutgoingTalk->has('congregation') ? $this->Html->link($meetingOutgoingTalk->congregation->name, ['controller' => 'Congregations', 'action' => 'view', $meetingOutgoingTalk->congregation->id]) : '' ?></td>
                <td><?= $meetingOutgoingTalk->has('outline') ? $this->Html->link($meetingOutgoingTalk->outline->name, ['controller' => 'Outlines', 'action' => 'view', $meetingOutgoingTalk->outline->id]) : '' ?></td>
                <td><?= h($meetingOutgoingTalk->created) ?></td>
                <td><?= h($meetingOutgoingTalk->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $meetingOutgoingTalk->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $meetingOutgoingTalk->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $meetingOutgoingTalk->id], ['confirm' => __('Are you sure you want to delete # {0}?', $meetingOutgoingTalk->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
