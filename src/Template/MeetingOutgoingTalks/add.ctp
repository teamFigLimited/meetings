<div class="page-header">
    <h1>Add Outgoing Public Talk</h1>
</div>

<?= $this->Form->create($meetingOutgoingTalk) ?>
<?= $this->Form->input('meeting_id', ['type' => 'hidden', 'value' => $meeting]) ?>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Outgoing Talk</h4>
            </div>
            
            <ul class="list-group">
                <li class="list-group-item">
                    <?= $this->Form->input('congregation_id', ['class' => 'form-control', 'options' => $congregations, 'empty' => true]) ?>
                </li>
                <li class="list-group-item">
                    <?= $this->Form->input('speaker_id', ['class' => 'form-control speaker-id-trigger-true', 'empty' => true]) ?>
                </li>
                <li class="list-group-item">
                    <?= $this->Form->input('outline_id', ['class' => 'form-control', 'empty' => true]) ?>
                </li>
                <li class="list-group-item">
                    <?= $this->Form->button(__('Add Outgoing Talk'), ['class' => 'btn btn-success form-control']) ?>
                </li>
            </ul>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>


