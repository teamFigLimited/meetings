<div class="panel panel-weekhead">
    <div class="panel-heading" style="background: repeating-linear-gradient(45deg, #<?= $meetingMidweek->meeting->week->month->colour1 ?>, #<?= $meetingMidweek->meeting->week->month->colour1 ?> 4px, #<?= $meetingMidweek->meeting->week->month->colour2 ?> 4px, #<?= $meetingMidweek->meeting->week->month->colour2 ?> 6px);">
        <div class="panel-title">Midweek Meeting</div>
    </div>
</div>

<?= $this->Form->create($meetingMidweek) ?>
<div class="row">
    <div class="col-xs-12 col-sm-4">
        <ul class="meetings treasures">
            <li class="bullet">
                <b>Opening Song</b>
                <?= $this->Form->input('opening_song_number', ['label' => false, 'empty' => true, 'class' => 'form-control', 'options' => $songs]) ?>                
            </li>
            <li class="bullet">
                <b>Chairman</b>
                <?= $this->Form->input('chairman_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>                
            </li>
            <li class="bullet">
                <b>Opening Prayer</b>
                <?= $this->Form->input('opening_prayer_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>                
            </li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-4">
        <ul class="meetings ministry">
            <li class="bullet">
                <b>Middle Song</b>
                <?= $this->Form->input('middle_song_number', ['label' => false, 'empty' => true, 'class' => 'form-control', 'options' => $songs]) ?>                
            </li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-4">
        <ul class="meetings living">
            <li class="bullet">
                <b>Closing Song</b>
                <?= $this->Form->input('closing_song_number', ['label' => false, 'empty' => true, 'class' => 'form-control', 'options' => $songs]) ?>                
            </li>
            <li class="bullet">
                <b>Closing Prayer</b>
                <?= $this->Form->input('closing_prayer_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>                
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="alert alert-treasures">Treasures from God&apos;s Word</div>
        <ul class="meetings treasures">
            <li class="bullet">
                <b><?= $meetingMidweek->meeting->week->week_midweek->treasures_talk_theme ?></b>: (10 min.)
                <?= $this->Form->input('treasures_talk_individual_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>
            </li>
            <li class="bullet">
                <b>Digging for Spiritual Gems</b>: (8 min.)
                <?= $this->Form->input('treasures_gems_individual_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>
            </li>
            <li class="bullet">
                <b>Bible Reading</b>: (4 min. or less)
                <?= $this->Form->input('treasures_reading_individual_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>
            </li>
        </ul>
    </div>
    
    <div class="col-xs-12 col-sm-6 col-md-4">
        
        <!-- MINISTRY -->
        <div class="alert alert-ministry">Apply Yourself to the Field Ministry</div>
        <ul class="meetings ministry">
            
            <?php if($meetingMidweek->is_week1): ?>

            <li class="bullet">
                <b>Prepare This Month’s Presentations</b>: (15 min.)
                <?= $this->Form->input('ministry_presentation_individual_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>
            </li>

            <?php else: ?>

            <li class="bullet">
                <b><?= $meetingMidweek->ministry_item1_type->name ?></b>: (2 min.)
                <?= $this->Form->input('ministry_item1_student_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>
                <?= $this->Form->input('ministry_item1_assistant_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>
            </li>
            <li class="bullet">
                <b><?= $meetingMidweek->ministry_item2_type->name ?></b>: (4 min.)
                <?= $this->Form->input('ministry_item2_student_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>
                <?= $this->Form->input('ministry_item2_assistant_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>
            </li>
            <li class="bullet">
                <b><?= $meetingMidweek->ministry_item3_type->name ?></b>: (6 min.)
                <?= $this->Form->input('ministry_item3_student_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>
                <?= $this->Form->input('ministry_item3_assistant_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>
            </li>

            <?php endif; ?>

        </ul>

    </div>

    <div class="col-xs-12 col-sm-6 col-md-4">
        
        <!-- LIVING -->
        <div class="alert alert-living">Living As Christians</div>
        <ul class="meetings living">
            <li class="bullet">
                <b><?= $meetingMidweek->living_item1_theme ?></b>: (<?= $meetingMidweek->living_item1_minutes ?> min.)
                <?= $this->Form->input('living_item1_individual_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>
            </li>
            <?php if($meetingMidweek->living_item2_theme != null): ?>
            <li class="bullet">
                <b><?= $meetingMidweek->living_item2_theme ?></b>: (<?= $meetingMidweek->living_item2_minutes ?> min.)
                <?= $this->Form->input('living_item2_individual_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>
            </li>
            <?php endif; ?>
            
            <?php if($meetingMidweek->meeting->meeting_week_type_id == 1): ?>
            <li class="bullet">
                <b>Congregation Bible Study</b>: (30 min.)<br>
                <i>Conductor</i>
                <?= $this->Form->input('living_cbs_conductor_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>
                <i>Reader</i>
                <?= $this->Form->input('living_cbs_reader_id', ['label' => false, 'empty' => true, 'class' => 'form-control']) ?>
            </li>
            
            <?php elseif($meetingMidweek->meeting->meeting_week_type_id == 2): ?>
            <li class="bullet">
                <b>CO Service Talk Theme</b>:
                <?= $this->Form->input('co_talk_theme', ['label' => false, 'class' => 'form-control']) ?>
            </li>
            
            <?php endif; ?>
        </ul>
        
    </div>
    
</div>
<?= $this->Form->button(__('Save Changes'), ['class' => 'form-control btn btn-success']) ?>
<?= $this->Form->end() ?>

<?php
if($debug):
    debug(json_encode($treasuresReadingIndividuals, JSON_PRETTY_PRINT));
    debug(json_encode($meetingMidweek, JSON_PRETTY_PRINT));
endif;
?>
