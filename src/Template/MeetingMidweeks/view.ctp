<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Meeting Midweek'), ['action' => 'edit', $meetingMidweek->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Meeting Midweek'), ['action' => 'delete', $meetingMidweek->id], ['confirm' => __('Are you sure you want to delete # {0}?', $meetingMidweek->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Meeting Midweeks'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Meeting Midweek'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Meetings'), ['controller' => 'Meetings', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Meeting'), ['controller' => 'Meetings', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="meetingMidweeks view large-9 medium-8 columns content">
    <h3><?= h($meetingMidweek->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Meeting') ?></th>
            <td><?= $meetingMidweek->has('meeting') ? $this->Html->link($meetingMidweek->meeting->id, ['controller' => 'Meetings', 'action' => 'view', $meetingMidweek->meeting->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Treasures Material') ?></th>
            <td><?= h($meetingMidweek->treasures_material) ?></td>
        </tr>
        <tr>
            <th><?= __('Treasures Talk Theme') ?></th>
            <td><?= h($meetingMidweek->treasures_talk_theme) ?></td>
        </tr>
        <tr>
            <th><?= __('Treasures Talk Material') ?></th>
            <td><?= h($meetingMidweek->treasures_talk_material) ?></td>
        </tr>
        <tr>
            <th><?= __('Treasures Gems Material') ?></th>
            <td><?= h($meetingMidweek->treasures_gems_material) ?></td>
        </tr>
        <tr>
            <th><?= __('Treasures Reading Material') ?></th>
            <td><?= h($meetingMidweek->treasures_reading_material) ?></td>
        </tr>
        <tr>
            <th><?= __('Ministry Presentation Material') ?></th>
            <td><?= h($meetingMidweek->ministry_presentation_material) ?></td>
        </tr>
        <tr>
            <th><?= __('Ministry Initial Material') ?></th>
            <td><?= h($meetingMidweek->ministry_initial_material) ?></td>
        </tr>
        <tr>
            <th><?= __('Ministry Return Material') ?></th>
            <td><?= h($meetingMidweek->ministry_return_material) ?></td>
        </tr>
        <tr>
            <th><?= __('Ministry Study Material') ?></th>
            <td><?= h($meetingMidweek->ministry_study_material) ?></td>
        </tr>
        <tr>
            <th><?= __('Living Item1 Theme') ?></th>
            <td><?= h($meetingMidweek->living_item1_theme) ?></td>
        </tr>
        <tr>
            <th><?= __('Living Item1 Material') ?></th>
            <td><?= h($meetingMidweek->living_item1_material) ?></td>
        </tr>
        <tr>
            <th><?= __('Living Item2 Theme') ?></th>
            <td><?= h($meetingMidweek->living_item2_theme) ?></td>
        </tr>
        <tr>
            <th><?= __('Living Item2 Material') ?></th>
            <td><?= h($meetingMidweek->living_item2_material) ?></td>
        </tr>
        <tr>
            <th><?= __('Living Cbs Material') ?></th>
            <td><?= h($meetingMidweek->living_cbs_material) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($meetingMidweek->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Chairman Id') ?></th>
            <td><?= $this->Number->format($meetingMidweek->chairman_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Opening Song Number') ?></th>
            <td><?= $this->Number->format($meetingMidweek->opening_song_number) ?></td>
        </tr>
        <tr>
            <th><?= __('Opening Prayer Id') ?></th>
            <td><?= $this->Number->format($meetingMidweek->opening_prayer_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Treasures Talk Individual Id') ?></th>
            <td><?= $this->Number->format($meetingMidweek->treasures_talk_individual_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Treasures Gems Individual Id') ?></th>
            <td><?= $this->Number->format($meetingMidweek->treasures_gems_individual_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Treasures Reading Individual Id') ?></th>
            <td><?= $this->Number->format($meetingMidweek->treasures_reading_individual_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Ministry Presentation Individual Id') ?></th>
            <td><?= $this->Number->format($meetingMidweek->ministry_presentation_individual_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Ministry Initial Student Id') ?></th>
            <td><?= $this->Number->format($meetingMidweek->ministry_initial_student_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Ministry Initial Assistant Id') ?></th>
            <td><?= $this->Number->format($meetingMidweek->ministry_initial_assistant_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Ministry Return Student Id') ?></th>
            <td><?= $this->Number->format($meetingMidweek->ministry_return_student_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Ministry Return Assistant Id') ?></th>
            <td><?= $this->Number->format($meetingMidweek->ministry_return_assistant_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Ministry Study Student Id') ?></th>
            <td><?= $this->Number->format($meetingMidweek->ministry_study_student_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Ministry Study Assistant Id') ?></th>
            <td><?= $this->Number->format($meetingMidweek->ministry_study_assistant_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Middle Song Number') ?></th>
            <td><?= $this->Number->format($meetingMidweek->middle_song_number) ?></td>
        </tr>
        <tr>
            <th><?= __('Living Item1 Minutes') ?></th>
            <td><?= $this->Number->format($meetingMidweek->living_item1_minutes) ?></td>
        </tr>
        <tr>
            <th><?= __('Living Item1 Individual Id') ?></th>
            <td><?= $this->Number->format($meetingMidweek->living_item1_individual_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Living Item2 Minutes') ?></th>
            <td><?= $this->Number->format($meetingMidweek->living_item2_minutes) ?></td>
        </tr>
        <tr>
            <th><?= __('Living Item2 Individual Id') ?></th>
            <td><?= $this->Number->format($meetingMidweek->living_item2_individual_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Living Cbs Conductor Id') ?></th>
            <td><?= $this->Number->format($meetingMidweek->living_cbs_conductor_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Living Cbs Reader Id') ?></th>
            <td><?= $this->Number->format($meetingMidweek->living_cbs_reader_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Closing Song Number') ?></th>
            <td><?= $this->Number->format($meetingMidweek->closing_song_number) ?></td>
        </tr>
        <tr>
            <th><?= __('Closing Prayer Id') ?></th>
            <td><?= $this->Number->format($meetingMidweek->closing_prayer_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Time Meeting') ?></th>
            <td><?= h($meetingMidweek->time_meeting) ?></td>
        </tr>
    </table>
</div>
