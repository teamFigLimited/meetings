<div class="page-header">
    <h1>Edit Reading <small class="text-uppercase">Treasures From God's Word</small></h1>
</div>

{{ Form.create(meetingMidweek)|raw }}
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">Reading Assignment</h4>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    {{ Form.input('treasures_reading_individual_id', { 'class': 'form-control individual-item-history', 'empty': true })|raw }}
                </li>
                <li class="list-group-item">
                    {{ Form.input('treasures_reading_study_id', { 'class': 'form-control', 'empty': true })|raw }}
                </li>
                <li class="list-group-item">
                    {{ Form.input('treasures_reading_study_notes', { 'class': 'form-control', 'type': 'textarea' })|raw }}
                </li>
                <li class="list-group-item">
                    {{ Form.submit('Assign Reader', {'class': 'btn btn-success btn-block'})|raw }}
                </li>
            </ul>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-8 col-lg-9">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>R</th>
                        <th>FC</th>
                        <th>RV</th>
                        <th>BS</th>
                        <th>Study</th>
                        <th>Notes</th>
                    </tr>
                </thead>
                <tbody>
                    {{ _view.element('a_talks_by_individual', {"meetingMidweeks": meetingMidweeks, "individualId": individualId})|raw }}
                </tbody>
            </table>
        </div>
    </div>
</div>
{{ Form.end()|raw }}

{% if debug %}
<pre>{{ meetingMidweek }}</pre>
{% endif %}
