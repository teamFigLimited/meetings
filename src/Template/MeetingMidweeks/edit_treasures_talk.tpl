<div class="page-header">
    <h1> <small class="text-uppercase">Treasures From God's Word</small></h1>
</div>

{{ Form.create(meetingMidweek)|raw }}
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">Spiritual Gems Assignment</h4>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    {{ Form.input('treasures_talk_individual_id', { 'class': 'form-control', 'empty': true })|raw }}
                </li>
                <li class="list-group-item">
                    {{ Form.submit('Assign Individual', {'class': 'btn btn-success btn-block'})|raw }}
                </li>
            </ul>
        </div>
    </div>
</div>
{{ Form.end()|raw }}


{% if debug %}
<pre>{{ meetingMidweek }}</pre>
{% endif %}
