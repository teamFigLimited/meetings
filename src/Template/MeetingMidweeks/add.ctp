<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Meeting Midweeks'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Meetings'), ['controller' => 'Meetings', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Meeting'), ['controller' => 'Meetings', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="meetingMidweeks form large-9 medium-8 columns content">
    <?= $this->Form->create($meetingMidweek) ?>
    <fieldset>
        <legend><?= __('Add Meeting Midweek') ?></legend>
        <?php
            echo $this->Form->input('meeting_id', ['options' => $meetings, 'empty' => true]);
            echo $this->Form->input('time_meeting');
            echo $this->Form->input('chairman_id');
            echo $this->Form->input('opening_song_number');
            echo $this->Form->input('opening_prayer_id');
            echo $this->Form->input('treasures_material');
            echo $this->Form->input('treasures_talk_theme');
            echo $this->Form->input('treasures_talk_material');
            echo $this->Form->input('treasures_talk_individual_id');
            echo $this->Form->input('treasures_gems_material');
            echo $this->Form->input('treasures_gems_individual_id');
            echo $this->Form->input('treasures_reading_material');
            echo $this->Form->input('treasures_reading_individual_id');
            echo $this->Form->input('ministry_presentation_material');
            echo $this->Form->input('ministry_presentation_individual_id');
            echo $this->Form->input('ministry_initial_material');
            echo $this->Form->input('ministry_initial_student_id');
            echo $this->Form->input('ministry_initial_assistant_id');
            echo $this->Form->input('ministry_return_material');
            echo $this->Form->input('ministry_return_student_id');
            echo $this->Form->input('ministry_return_assistant_id');
            echo $this->Form->input('ministry_study_material');
            echo $this->Form->input('ministry_study_student_id');
            echo $this->Form->input('ministry_study_assistant_id');
            echo $this->Form->input('middle_song_number');
            echo $this->Form->input('living_item1_theme');
            echo $this->Form->input('living_item1_material');
            echo $this->Form->input('living_item1_minutes');
            echo $this->Form->input('living_item1_individual_id');
            echo $this->Form->input('living_item2_theme');
            echo $this->Form->input('living_item2_material');
            echo $this->Form->input('living_item2_minutes');
            echo $this->Form->input('living_item2_individual_id');
            echo $this->Form->input('living_cbs_material');
            echo $this->Form->input('living_cbs_conductor_id');
            echo $this->Form->input('living_cbs_reader_id');
            echo $this->Form->input('closing_song_number');
            echo $this->Form->input('closing_prayer_id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
