<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Meeting Midweek'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Meetings'), ['controller' => 'Meetings', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Meeting'), ['controller' => 'Meetings', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="meetingMidweeks index large-9 medium-8 columns content">
    <h3><?= __('Meeting Midweeks') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('meeting_id') ?></th>
                <th><?= $this->Paginator->sort('time_meeting') ?></th>
                <th><?= $this->Paginator->sort('chairman_id') ?></th>
                <th><?= $this->Paginator->sort('opening_song_number') ?></th>
                <th><?= $this->Paginator->sort('opening_prayer_id') ?></th>
                <th><?= $this->Paginator->sort('treasures_material') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($meetingMidweeks as $meetingMidweek): ?>
            <tr>
                <td><?= $this->Number->format($meetingMidweek->id) ?></td>
                <td><?= $meetingMidweek->has('meeting') ? $this->Html->link($meetingMidweek->meeting->id, ['controller' => 'Meetings', 'action' => 'view', $meetingMidweek->meeting->id]) : '' ?></td>
                <td><?= h($meetingMidweek->time_meeting) ?></td>
                <td><?= $this->Number->format($meetingMidweek->chairman_id) ?></td>
                <td><?= $this->Number->format($meetingMidweek->opening_song_number) ?></td>
                <td><?= $this->Number->format($meetingMidweek->opening_prayer_id) ?></td>
                <td><?= h($meetingMidweek->treasures_material) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $meetingMidweek->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $meetingMidweek->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $meetingMidweek->id], ['confirm' => __('Are you sure you want to delete # {0}?', $meetingMidweek->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
