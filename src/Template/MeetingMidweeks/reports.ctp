<div class="page-header">
    <h1>Reports <small class="text-uppercase">by midweek meeting</small></h1>
</div>

<div class="row">

    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">CLM Worksheet (beta)</h4>
            </div>
            <div class="list-group">
                <a class="list-group-item disabled">PDF Reports</a>
                <?php foreach($meetingMidweeks as $meetingMidweek): ?>
                <?= $this->Html->link($meetingMidweek->meeting->week->full_date, ['controller' => 'meeting_midweeks', 'action' => 'clm_worksheet', $meetingMidweek->id, '_ext' => 'pdf'], ['class' => 'list-group-item', 'target' => '_blank']) ?>
                <?php endforeach; ?>
            </div>
            <div class="panel-body alert alert-info">
                <p><b>Please note:</b> PDF reports can take up to 30 seconds to run so please be patient. Thank you.</p>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">CLM Timing Sheet</h4>
            </div>
            <div class="list-group">
                <a class="list-group-item disabled">PDF Reports</a>
                <?php foreach($meetingMidweeks as $meetingMidweek): ?>
                <?= $this->Html->link($meetingMidweek->meeting->week->full_date, ['controller' => 'meeting_midweeks', 'action' => 'clm_worksheet', $meetingMidweek->id, 'timing', '_ext' => 'pdf'], ['class' => 'list-group-item', 'target' => '_blank']) ?>
                <?php endforeach; ?>
            </div>
            <div class="panel-body alert alert-info">
                <p><b>Please note:</b> PDF reports can take up to 30 seconds to run so please be patient. Thank you.</p>
            </div>
            <div class="list-group">
                <a class="list-group-item disabled">Standard Reports</a>
                <?php foreach($meetingMidweeks as $meetingMidweek): ?>
                <?= $this->Html->link($meetingMidweek->meeting->week->full_date, ['controller' => 'meeting_midweeks', 'action' => 'clm_worksheet', $meetingMidweek->id, 'timing'], ['class' => 'list-group-item', 'target' => '_blank']) ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Assignment Slips</h4>
            </div>
            <div class="list-group">
                <a class="list-group-item disabled">PDF Reports</a>
                <?php foreach($meetingMidweeks as $meetingMidweek): ?>
                <?= $this->Html->link($meetingMidweek->meeting->week->full_date, ['controller' => 'meeting_midweeks', 'action' => 'assignment_slips', $meetingMidweek->id, '_ext' => 'pdf'], ['class' => 'list-group-item', 'target' => '_blank']) ?>
                <?php endforeach; ?>
            </div>
            <div class="panel-body alert alert-info">
                <p><b>Please note:</b> PDF reports can take up to 30 seconds to run so please be patient. Thank you.</p>
            </div>
            <div class="list-group">
                <a class="list-group-item disabled">Standard Reports</a>
                <?php foreach($meetingMidweeks as $meetingMidweek): ?>
                <?= $this->Html->link($meetingMidweek->meeting->week->full_date, ['controller' => 'meeting_midweeks', 'action' => 'assignment_slips', $meetingMidweek->id], ['class' => 'list-group-item', 'target' => '_blank']) ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

</div>


<?php
if($debug):
    debug(json_encode($meetingMidweeks, JSON_PRETTY_PRINT));
    debug(json_encode($weeks, JSON_PRETTY_PRINT));
endif;
?>
