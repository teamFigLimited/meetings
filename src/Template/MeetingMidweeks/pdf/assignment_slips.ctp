<?= $this->Html->css('spacelab.min', ['fullBase' => true]) ?>
<?= $this->Html->css('css', ['fullBase' => true]) ?>
<?= $this->fetch('css') ?>

<style>
    @page { margin: 12px 0 0 0;}
    body { font-size:12px;}
    .page-header{padding:0;margin:0;}
    h2 {margin:0;padding:0;}
    /* h2 {padding: 0 8px;margin: 0;} */
    h4 { border-bottom-color: rgb(238, 238, 238); border-bottom-style: solid; border-bottom-width: 1px; }
    h6 { padding: 4px 8px 2px 8px;margin:0;}
    /*p {margin:0;padding:0;} */
    /*.page-header p{padding:0 8px;}*/
    /*.alert {font-size:10px;margin:0;padding:0;text-align:center;}*/
    .alert {padding:4px;}
    /*.alert-treasures, .alert-ministry, .alert-living{padding:0;margin:0;}*/
    .text-success{margin:0;padding:0;}

    .slip {margin-bottom:60px;margin-top:24px;}
    /*.row-slips:nth-child(even) {margin-left: :32px;}
    .row-slips:nth-child(odd) {margin-right: 32px;}*/

    .heading {font-size:16px;text-align: center;font-weight:bold;}
    .slip-row {margin-top:24px;}
    .t div {width:50%;float:left;}
    .note {margin-top:30px;font-size:11px;}
    .rule {border-bottom: 1px dotted #333;}

    td div {background-color:#123456;}
</style>

<div class="row row-slips">
  <?php if($meetingMidweek->treasures_reading_individual): ?>
    <?= $this->element('_assignment_slip', [
      'name' => $meetingMidweek->treasures_reading_individual->full_name,
      'assistant' => '',
      'date' => $meetingMidweek->meeting->week->date_monday,
      'counselPoint' => $meetingMidweek->treasures_reading_study_id,
      'assignment' => 0,
      'school' => 1
    ]) ?>
  <?php endif; ?>
  <?php if($meetingMidweek->ministry_item1_student): ?>
    <?= $this->element('_assignment_slip', [
      'name' => $meetingMidweek->ministry_item1_student->full_name,
      'assistant' => (isset($meetingMidweek->ministry_item1_assistant)) ? $meetingMidweek->ministry_item1_assistant->full_name : '',
      'date' => $meetingMidweek->meeting->week->date_monday,
      'counselPoint' => $meetingMidweek->ministry_item1_study_id,
      'assignment' => 1,
      'school' => 1
    ]) ?>
  <?php endif; ?>
  <?php if($meetingMidweek->ministry_item2_student): ?>
    <?= $this->element('_assignment_slip', [
      'name' => $meetingMidweek->ministry_item2_student->full_name,
      'assistant' => (isset($meetingMidweek->ministry_item2_assistant)) ? $meetingMidweek->ministry_item2_assistant->full_name : '',
      'date' => $meetingMidweek->meeting->week->date_monday,
      'counselPoint' => $meetingMidweek->ministry_item2_study_id,
      'assignment' => 2,
      'school' => 1
    ]) ?>
  <?php endif; ?>
  <?php if($meetingMidweek->ministry_item3_student): ?>
    <?= $this->element('_assignment_slip', [
      'name' => $meetingMidweek->ministry_item3_student->full_name,
      'assistant' => (isset($meetingMidweek->ministry_item3_assistant)) ? $meetingMidweek->ministry_item3_assistant->full_name : '',
      'date' => $meetingMidweek->meeting->week->date_monday,
      'counselPoint' => $meetingMidweek->ministry_item3_study_id,
      'assignment' => 3,
      'school' => 1
    ]) ?>
  <?php endif; ?>
</div>

<?php
if($debug):
    debug(json_encode($meetingMidweek, JSON_PRETTY_PRINT));
endif;
?>
