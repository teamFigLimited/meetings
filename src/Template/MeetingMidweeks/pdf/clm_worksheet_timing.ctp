<?= $this->Html->css('spacelab.min', ['fullBase' => true]) ?>
<?= $this->Html->css('css', ['fullBase' => true]) ?>
<?= $this->fetch('css') ?>

<style>
    @page { margin: 0;}
    body { font-size:12px;}
    .page-header{padding:0;margin:0;}
    h2 {margin:0;padding:0;}
    /* h2 {padding: 0 8px;margin: 0;} */
    h4 { border-bottom-color: rgb(238, 238, 238); border-bottom-style: solid; border-bottom-width: 1px; }
    h6 { padding: 4px 8px 2px 8px;margin:0;}
    /*p {margin:0;padding:0;} */
    /*.page-header p{padding:0 8px;}*/
    /*.alert {font-size:10px;margin:0;padding:0;text-align:center;}*/
    .alert {padding:4px;}
    /*.alert-treasures, .alert-ministry, .alert-living{padding:0;margin:0;}*/
    .text-success{margin:0;padding:0;}
    table {width:100%;padding:0 8px;margin:0;}
    tr, td {margin:0;padding:0;vertical-align:top;}
    
    .item-box {vertical-align:middle;font-size:11px;padding-top:10px;}
    .individual-box {vertical-align:middle;font-size:18px;padding-top:7px;margin-right:5px;}
    .timing-box {vertical-align:middle;border: 2px solid #<?= $meetingMidweek->meeting->week->month->colour1 ?> !important;min-width:80px;padding-right:5px;font-size:24px;margin:2px 0;}
</style>

<div class="page-header">
    <h2>Our Christian Life and Ministry</h2>
    <p>Timing Sheet<span style="float:right;padding:0;margin:0;" class="text-right"><b>Finish</b></span></p>
</div>

<table>
    
    <!-- week and material -->
    <tr>
        <td colspan="3">
            <div class="alert" style="margin:0;background-color:#<?= $meetingMidweek->meeting->week->month->colour1 ?> !important;">
                <b style="color:#fff !important;"><?= $meetingMidweek->meeting->week->full_date ?></b>
            </div>
        </td>
    </tr>
    
    <!-- song & prayer -->
    <tr>
        <td colspan="2"><div class="item-box">Song <?= $meetingMidweek->opening_song_number ?> and Prayer</div></td>
        <td><div class="text-right timing-box"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(5), 'h:mm') ?></div></td>
    </tr>
    
    <!-- opening comments -->
    <tr>
        <td><div class="item-box">Opening Comments (3 min. or less)</div></td>
        <td><div class="individual-box text-right"><b><?= ($meetingMidweek->has('chairman')) ? $meetingMidweek->chairman->full_name : '' ?></b></div></td>
        <td><div class="text-right timing-box"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(3), 'h:mm') ?></div></td>
    </tr>
    
    <!-- Treasures From God's Word -->
    <tr>
        <td colspan="3"><div class="alert alert-treasures text-uppercase" style="text-align:left;margin:3px 0 0 0;background-color:#4D565D !important;color:#fff !important;"><b style="color:#fff !important;">Treasures From God's Word</b></div></td>
    </tr>
        
    <!-- treasures talk -->
    <tr>
        <td><div class="item-box"><?= $meetingMidweek->meeting->week->week_midweek->treasures_talk_theme ?> (10 min.)</div></td>
        <td><div class="individual-box text-right"><?= ($meetingMidweek->treasures_talk_individual) ? '<b>'.$meetingMidweek->treasures_talk_individual->full_name.'</b>' : ' ' ?></div></td>
        <td><div class="text-right timing-box"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(10), 'h:mm') ?></div></td>
    </tr>

    <!-- spiritual gems -->
    <tr>
        <td><div class="item-box">Digging For Spiritual Gems (8 min.)</div></td>
        <td><div class="individual-box text-right"><?= ($meetingMidweek->treasures_gems_individual) ? '<b>'.$meetingMidweek->treasures_gems_individual->full_name.'</b>' : ' ' ?></div></td>
        <td><div class="text-right timing-box"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(8), 'h:mm') ?></div></td>
    </tr>
    
    <!-- READING -->
    <tr>
        <td><div class="item-box">Bible Reading (4 min. or less)</div></td>
        <td><div class="individual-box text-right"><?= ($meetingMidweek->treasures_reading_individual) ? '<b>'.$meetingMidweek->treasures_reading_individual->full_name.'</b>' : '' ?></div></td>
        <td><div class="text-right timing-box"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(4), 'h:mm') ?></div></td>
    </tr>
    <tr>
        <td><div class="item-box">Chairman's Counsel</td>
        <td><div class="individual-box text-right"><b><?= ($meetingMidweek->has('chairman')) ? $meetingMidweek->chairman->full_name : '' ?></b></td>
        <td><div class="timing-box text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(1), 'h:mm') ?></td>
    </tr>    

    <!-- Apply Yourself to the Field Ministry -->
    <tr>
        <td colspan="3"><div class="alert alert-ministry text-uppercase" style="text-align:left;margin:6px 0 0 0;padding:3px 0 4px 4px;background-color: #B2731D !important; color:#fff !important;"><b style="color:#fff !important;">Apply Yourself to the Field Ministry</b></div></td>
    </tr>

    
    <?php if($meetingMidweek->is_week1 === 1): ?>
    
    <tr>
        <td><div class="item-box">Prepare This Month's Presentations (15 min.)</div></td>
        <td><div class="individual-box text-right"><b><?= ($meetingMidweek->ministry_presentation_individual) ? $meetingMidweek->ministry_presentation_individual->full_name : '' ?></b></div></td>
        <td><div class="timing-box text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(15), 'h:mm') ?></td>
    </tr>

    <?php else: ?>

    <!-- TALK 1 -->
    <tr>
        <td><div class="item-box"><?= $meetingMidweek->meeting->week->week_midweek->ministry_item1_type->name ?> (2 min. or less)</td>
        <td><div class="individual-box text-right"><b><?= ($meetingMidweek->ministry_item1_student) ? $meetingMidweek->ministry_item1_student->full_name : '' ?></b></td>
        <td><div class="timing-box text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(2), 'h:mm') ?></td>
    </tr>    
    <tr>
        <td><div class="item-box">Chairman's Counsel</td>
        <td><div class="individual-box text-right"><b><?= ($meetingMidweek->has('chairman')) ? $meetingMidweek->chairman->full_name : '' ?></b></td>
        <td><div class="timing-box text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(1), 'h:mm') ?></td>
    </tr>    
    
    <!-- TALK 2 -->
    <tr>
        <td><div class="item-box"><?= $meetingMidweek->meeting->week->week_midweek->ministry_item2_type->name ?> (4 min. or less)</td>
        <td><div class="individual-box text-right"><b><?= ($meetingMidweek->ministry_item2_student) ? $meetingMidweek->ministry_item2_student->full_name : '' ?></b></td>
        <td><div class="timing-box text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(4), 'h:mm') ?></td>
    </tr>    
    <tr>
        <td><div class="item-box">Chairman's Counsel</td>
        <td><div class="individual-box text-right"><b><?= ($meetingMidweek->has('chairman')) ? $meetingMidweek->chairman->full_name : '' ?></b></td>
        <td><div class="timing-box text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(1), 'h:mm') ?></td>
    </tr>    
        
    <!-- TALK 3 -->
    <tr>
        <td><div class="item-box"><?= $meetingMidweek->meeting->week->week_midweek->ministry_item3_type->name ?> (6 min. or less)</td>
        <td><div class="individual-box text-right"><b><?= ($meetingMidweek->ministry_item3_student) ? $meetingMidweek->ministry_item3_student->full_name : '' ?></b></td>
        <td><div class="timing-box text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(6), 'h:mm') ?></td>
    </tr>    
    <tr>
        <td><div class="item-box">Chairman's Counsel</td>
        <td><div class="individual-box text-right"><b><?= ($meetingMidweek->has('chairman')) ? $meetingMidweek->chairman->full_name : '' ?></b></td>
        <td><div class="timing-box text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(1), 'h:mm') ?></td>
    </tr>    
        
    <?php endif; ?>
    
    <!-- Living as Christians -->
    <tr>
        <td colspan="3"><div class="alert alert-living text-uppercase" style="text-align:left;margin:6px 0 0 0;background-color:#82031D !important;color:#fff !important;"><b style="color:#fff !important;">Living as Christians</b></div></td>
    </tr>
    
    <!-- Song -->
    <tr>
        <td colspan="2"><div class="item-box">Song <?= $meetingMidweek->middle_song_number ?></div></td>
        <td><div class="timing-box text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(5), 'h:mm') ?></div></td>
    </tr>
    
    <!-- ITEM 1 -->
    <tr>
        <td><div class="item-box"><?= $meetingMidweek->meeting->week->week_midweek->living_item1_theme ?> (<?= $meetingMidweek->meeting->week->week_midweek->living_item1_minutes ?> min.)</div></td>
        <td><div class="individual-box text-right"><b><?= ($meetingMidweek->has('living_item1_individual')) ? $meetingMidweek->living_item1_individual->full_name : '' ?></b></div></td>
        <td><div class="timing-box text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes($meetingMidweek->meeting->week->week_midweek->living_item1_minutes), 'h:mm') ?></div></td>
    </tr>
    
    <!-- ITEM 2 -->
    <?php if($meetingMidweek->living_item2_theme): ?>
    <tr>
        <td><div class="item-box"><?= $meetingMidweek->meeting->week->week_midweek->living_item2_theme ?> (<?= $meetingMidweek->meeting->week->week_midweek->living_item2_minutes ?> min.)</div></td>
        <td><div class="individual-box text-right"><b><?= ($meetingMidweek->has('living_item2_individual')) ? $meetingMidweek->living_item2_individual->full_name : '' ?></b></div></td>
        <td><div class="timing-box text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes($meetingMidweek->meeting->week->week_midweek->living_item2_minutes), 'h:mm') ?></div></td>
    </tr>
    
    <?php endif; ?>
    
    <!-- CBS -->
    <?php if($meetingMidweek->meeting->meeting_week_type_id === 1): ?>
    <tr>
        <td><div class="item-box">Congregation Bible Study (30 min.)</div></td>
        <td><div class="individual-box text-right"><b><?= ($meetingMidweek->has('living_cbs_conductor')) ? $meetingMidweek->living_cbs_conductor->full_name : '' ?></b></div></td>
        <td><div class="timing-box text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(30), 'h:mm') ?></div></td>
    </tr>    
    <?php endif; ?>
    
    <!-- CLOSING COMMENTS -->
    <tr>
        <td><div class="item-box">Review followed by Preview of Next Week (3 min.)</div></td>
        <td><div class="individual-box text-right"><b><?= ($meetingMidweek->has('chairman')) ? $meetingMidweek->chairman->full_name : '' ?></b></div></td>
        <td><div class="timing-box text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(3), 'h:mm') ?></div></td>
    </tr>
    
    <!-- CO -->
    <?php if($meetingMidweek->meeting->meeting_week_type_id === 2): ?>
    <tr>
        <td><div class="item-box"<?= $meetingMidweek->co_talk_theme ?></td>
        <td><div class="individual-box text-right"<b><?= $meetingMidweek->meeting->installation->circuit_overseer_name ?></b></td>
        <td><div class="timing-box text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(30), 'h:mm') ?></div></td>
    </tr>
    <?php endif; ?>
    
    <!-- SONG & PRAYER -->
    <tr>
        <td colspan="3"><div class="item-box">Song <?= $meetingMidweek->closing_song_number ?> and Prayer</div></td>
    </tr>
        
</table>

<?php
if($debug):
    debug(json_encode($meetingMidweek, JSON_PRETTY_PRINT));
endif;
?>