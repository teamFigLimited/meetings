<?= $this->Html->css('spacelab.min', ['fullBase' => true]) ?>
<?= $this->Html->css('css', ['fullBase' => true]) ?>
<?= $this->fetch('css') ?>

<style>
    @page { margin: 0;}
    body { font-size:12px;}
    .page-header{padding:0;margin:0;}
    h2 {margin:0;padding:0;}
    /* h2 {padding: 0 8px;margin: 0;} */
    h4 { border-bottom-color: rgb(238, 238, 238); border-bottom-style: solid; border-bottom-width: 1px; }
    h6 { padding: 4px 8px 2px 8px;margin:0;}
    /*p {margin:0;padding:0;} */
    /*.page-header p{padding:0 8px;}*/
    /*.alert {font-size:10px;margin:0;padding:0;text-align:center;}*/
    .alert {padding:4px;}
    /*.alert-treasures, .alert-ministry, .alert-living{padding:0;margin:0;}*/
    .text-success{margin:0;padding:0;}
    table {width:100%;padding:0 8px;margin:0;}
    tr, td {margin:0;padding:0;vertical-align:top;}
    
</style>

<div class="page-header">
    <h2>Our Christian Life and Ministry</h2>
    <p>Chairman's Worksheet - <b><?= ($meetingMidweek->has('chairman')) ? $meetingMidweek->chairman->full_name : '' ?></b><span style="float:right;padding:0;margin:0;" class="text-right"><b>Finish</b></span></p>
</div>

<table>
    
    <!-- week and material -->
    <tr>
        <td colspan="3">
            <div class="alert" style="margin:0;background-color:#<?= $meetingMidweek->meeting->week->month->colour1 ?> !important;">
                <b style="color:#fff !important;"><?= $meetingMidweek->meeting->week->full_date ?>
                    <span style="float:right;color:#fff !important;padding:0;margin:0;"><?= $meetingMidweek->meeting->week->week_midweek->treasures_material ?></span>
                </b>
            </div>
        </td>
    </tr>
    
    <!-- song & prayer -->
    <tr>
        <td style="width:65%;">Song <?= $meetingMidweek->opening_song_number ?> and Prayer</td>
        <td style="width:30%;"><?= ($meetingMidweek->has('opening_prayer')) ? '<b>'.$meetingMidweek->opening_prayer->full_name.'</b>&nbsp;&nbsp;(Prayer)' : '&nbsp;' ?></td>
        <td style="width:5%;" class="text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(5), 'h:mm') ?></td>
    </tr>
    
    <!-- opening comments -->
    <tr>
        <td><div style="height:100px;font-size:11px;">Opening Comments (3 min. or less)</div></td>
        <td><b><?= ($meetingMidweek->has('chairman')) ? $meetingMidweek->chairman->full_name : '' ?></b></td>
        <td><div class="text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(3), 'h:mm') ?></div></td>
    </tr>
    
    <!-- Treasures From God's Word -->
    <tr>
        <td colspan="3"><div class="alert alert-treasures text-uppercase" style="text-align:left;margin:3px 0 0 0;background-color:#4D565D !important;color:#fff !important;"><b style="color:#fff !important;">Treasures From God's Word</b></div></td>
    </tr>
        
    <!-- treasures talk -->
    <tr>
        <td style="padding:3px 0;margin-bottom:3px;"><?= $meetingMidweek->meeting->week->week_midweek->treasures_talk_theme ?> (10 min.)</td>
        <td style="padding:3px 0;margin-bottom:3px;"><?= ($meetingMidweek->treasures_talk_individual) ? '<b>'.$meetingMidweek->treasures_talk_individual->full_name.'</b>' : ' ' ?></td>
        <td style="padding:3px 0;margin-bottom:3px;" class="text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(10), 'h:mm') ?></td>
    </tr>
    
    <!-- spiritual gems -->
    <tr>
        <td style="border-bottom:1px solid #4D565D;padding:3px 0;margin-bottom:3px;">Digging For Spiritual Gems (8 min.)</td>
        <td style="border-bottom:1px solid #4D565D;padding:3px 0;margin-bottom:3px;"><?= ($meetingMidweek->treasures_gems_individual) ? '<b>'.$meetingMidweek->treasures_gems_individual->full_name.'</b>' : ' ' ?></td>
        <td style="border-bottom:1px solid #4D565D;padding:3px 0;margin-bottom:3px;" class="text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(8), 'h:mm') ?></td>
    </tr>
    
    <!-- READING -->
    <tr>
        <td style="">Bible Reading (4 min. or less)</td>
        <td style="padding-left:4px;"><?= ($meetingMidweek->treasures_reading_individual) ? '<b>'.$meetingMidweek->treasures_reading_individual->full_name.'</b>' : '' ?></td>
        <td style="" class="text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(4), 'h:mm') ?></td>
    </tr>
<!--    <tr>
        <td rowspan="3" style="border-bottom:1px solid #B2731D;border-right:1px solid #B2731D;font-size:10px;padding-right:5px;"><?= $meetingMidweek->meeting->week->week_midweek->treasures_reading_material ?></td>
        <td style="padding-left:4px;"><i>Working On</i></td>
        <td style="vertical-align: bottom;"><?= $meetingMidweek->treasures_reading_study_id ?></td>
    </tr>-->
    <tr>
        <td rowspan="3" style="border-bottom:1px solid #B2731D;border-right:1px solid #B2731D;font-size:10px;padding-right:5px;"><?= $meetingMidweek->meeting->week->week_midweek->treasures_reading_material ?></td>
        <td style="padding-left:4px;padding-top:8px;" colspan="2"><i>Working On</i> <span class="pull-right"><small><?= $meetingMidweek->treasures_reading_study_id ?>: <?= $meetingMidweek->treasures_reading_study->name ?></small></span></td>    
    </tr>
    <tr>
        <td style="padding-left:4px;"><i>Next Counsel Point</i></td>
    </tr>
    <tr>
        <td style="padding-left:4px;"><i>Exercises done?</i></td>
        <td style="padding:3px 0;" class="text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(1), 'h:mm') ?></td>
    </tr>

    <!-- Apply Yourself to the Field Ministry -->
    <tr>
        <td colspan="3"><div class="alert alert-ministry text-uppercase" style="text-align:left;margin:6px 0 0 0;padding:3px 0 4px 4px;background-color: #B2731D !important; color:#fff !important;"><b style="color:#fff !important;">Apply Yourself to the Field Ministry</b></div></td>
    </tr>

    
    <?php if($meetingMidweek->is_week1 === 1): ?>
    
    <tr>
        <td>Prepare This Month's Presentations (15 min.)</td>
        <td><?= ($meetingMidweek->ministry_presentation_individual) ? '<b>'.$meetingMidweek->ministry_presentation_individual->full_name.'</b>' : '' ?></td>
        <td style="" class="text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(15), 'h:mm') ?></td>
    </tr>

    <?php else: ?>
    
    
    <!-- TALK 1 -->
    <tr>
        <td style=""><?= $meetingMidweek->meeting->week->week_midweek->ministry_item1_type->name ?> (2 min. or less)</td>
        <td style="padding-left:4px;"><?= ($meetingMidweek->ministry_item1_student) ? '<b>'.$meetingMidweek->ministry_item1_student->full_name.'</b>' : '' ?></td>
        <td style="" class="text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(2), 'h:mm') ?></td>
    </tr>
    <tr>
        <td rowspan="4" style="border-bottom:1px solid #B2731D;border-right:1px solid #B2731D;font-size:10px;padding-right:5px;"><?= $meetingMidweek->meeting->week->week_midweek->ministry_item1_material ?></td>
        <td style="padding-left:4px;font-size:11px;"><i>+ <?= ($meetingMidweek->ministry_item1_assistant) ? $meetingMidweek->ministry_item1_assistant->full_name : '&nbsp;' ?></i></td>
    </tr>
<!--    <tr>
        <td style="padding-left:4px;padding-top:8px;"><i>Working On</i></td>
        <td style="vertical-align: bottom;"><?= $meetingMidweek->ministry_item1_study_id ?></td>
    </tr>-->
    <tr>
        <td style="padding-left:4px;padding-top:8px;" colspan="2"><i>Working On</i> <span class="pull-right"><small><?= $meetingMidweek->ministry_item1_study_id ?>: <?= $meetingMidweek->ministry_item1_study->name ?></small></span></td>
    </tr>
    <tr>
        <td colspan="1" style="padding-left:4px;"><i>Next Counsel Point</i></td>
    </tr>
    <tr>
        <td style="border-bottom:1px solid #B2731D;padding-left:4px;"><i>Exercises done?</i></td>
        <td style="border-bottom:1px solid #B2731D;padding:0;" class="text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(1), 'h:mm') ?></td>
    </tr>
    
    
    <!-- TALK 2 -->
    <tr>
        <td style=""><?= $meetingMidweek->meeting->week->week_midweek->ministry_item2_type->name ?> (4 min. or less)</td>
        <td style="padding-left:4px;"><?= ($meetingMidweek->ministry_item2_student) ? '<b>'.$meetingMidweek->ministry_item2_student->full_name.'</b>' : '' ?></td>
        <td style="" class="text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(4), 'h:mm') ?></td>
    </tr>
    <tr>
        <td rowspan="4" style="border-bottom:1px solid #B2731D;border-right:1px solid #B2731D;font-size:10px;padding-right:5px;"><?= $meetingMidweek->meeting->week->week_midweek->ministry_item2_material ?></td>
        <td style="padding-left:4px;font-size:11px;"><i>+ <?= ($meetingMidweek->ministry_item2_assistant) ? $meetingMidweek->ministry_item2_assistant->full_name : '&nbsp;' ?></i></td>
    </tr>
<!--    <tr>
        <td style="padding-left:4px;padding-top:8px;"><i>Working On</i></td>
        <td style="vertical-align: bottom;"><?= $meetingMidweek->ministry_item2_study_id ?></td>
    </tr>-->
    <tr>
        <td style="padding-left:4px;padding-top:8px;" colspan="2"><i>Working On</i> <span class="pull-right"><small><?= $meetingMidweek->ministry_item2_study_id ?>: <?= $meetingMidweek->ministry_item2_study->name ?></small></span></td>
    </tr>
    <tr>
        <td style="padding-left:4px;"><i>Next Counsel Point</i></td>
    </tr>
    <tr>
        <td style="border-bottom:1px solid #B2731D;padding-left:4px;"><i>Exercises done?</i></td>
        <td style="border-bottom:1px solid #B2731D;padding:0;" class="text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(1), 'h:mm') ?></td>
    </tr>
    
    
    <!-- TALK 3 -->
    <tr>
        <td style=""><?= $meetingMidweek->meeting->week->week_midweek->ministry_item3_type->name ?> (6 min. or less)</td>
        <td style="padding-left:4px;"><?= ($meetingMidweek->ministry_item3_student) ? '<b>'.$meetingMidweek->ministry_item3_student->full_name.'</b>' : '' ?></td>
        <td style="" class="text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(6), 'h:mm') ?></td>
    </tr>
    <tr>
        <td rowspan="4" style="border-right:1px solid #B2731D;font-size:10px;padding-right:5px;"><?= $meetingMidweek->meeting->week->week_midweek->ministry_item3_material ?></td>
        <td style="padding-left:4px;font-size:11px;"><i>+ <?= ($meetingMidweek->ministry_item3_assistant) ? $meetingMidweek->ministry_item3_assistant->full_name : '&nbsp;' ?></i></td>
    </tr>
<!--    <tr>
        <td style="padding-left:4px;padding-top:8px;"><i>Working On</i></td>
        <td style="vertical-align: bottom;"><?= $meetingMidweek->ministry_item3_study_id ?></td>
    </tr>-->
    <tr>
        <td style="padding-left:4px;padding-top:8px;" colspan="2"><i>Working On</i> <span class="pull-right"><small><?= $meetingMidweek->ministry_item3_study_id ?>: <?= $meetingMidweek->ministry_item3_study->name ?></small></span></td>
    </tr>
    <tr><td style="padding-left:4px;"><i>Next Counsel Point</i></td></tr>
    <tr>
        <td style="padding-left:4px;"><i>Exercises done?</i></td>
        <td style="padding:0;" class="text-right"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(1), 'h:mm') ?></td>
    </tr>
    
    <?php endif; ?>
    
    
    <!-- Living as Christians -->
    <tr>
        <td colspan="3"><div class="alert alert-living text-uppercase" style="text-align:left;margin:6px 0 0 0;background-color:#82031D !important;color:#fff !important;"><b style="color:#fff !important;">Living as Christians</b></div></td>
    </tr>
    
    <!-- Song -->
    <tr>
        <td colspan="2"><div style="padding-bottom:2px;margin-bottom:2px;">Song <?= $meetingMidweek->middle_song_number ?></div></td>
        <td><div class="text-right" style="padding-bottom:2px;margin-bottom:2px;"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(5), 'h:mm') ?></div></td>
    </tr>
    
    <!-- item 1 -->
    <tr>
        <td><div style="padding-bottom:2px;margin-bottom:2px;"><?= $meetingMidweek->meeting->week->week_midweek->living_item1_theme ?> (<?= $meetingMidweek->meeting->week->week_midweek->living_item1_minutes ?> min.)</div></td>
        <td><div style="padding-bottom:2px;margin-bottom:2px;"><b><?= ($meetingMidweek->has('living_item1_individual')) ? $meetingMidweek->living_item1_individual->full_name : '' ?></b></div></td>
        <td><div class="text-right" style="padding-bottom:2px;margin-bottom:2px;"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes($meetingMidweek->meeting->week->week_midweek->living_item1_minutes), 'h:mm') ?></div></td>
    </tr>
    
    <!-- item 2 -->
    <?php if($meetingMidweek->living_item2_theme): ?>
    <tr>
        <td><div style="padding-bottom:2px;border-bottom:1px solid #82031D;margin-bottom:2px;height:42px;"><?= $meetingMidweek->meeting->week->week_midweek->living_item2_theme ?> (<?= $meetingMidweek->meeting->week->week_midweek->living_item2_minutes ?> min.)</div></td>
        <td><div style="padding-bottom:2px;border-bottom:1px solid #82031D;margin-bottom:2px;height:42px;"><b><?= ($meetingMidweek->has('living_item2_individual')) ? $meetingMidweek->living_item2_individual->full_name : '&nbsp;' ?></b></div></td>
        <td><div class="text-right" style="padding-bottom:2px;border-bottom:1px solid #82031D;margin-bottom:2px;height:42px;"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes($meetingMidweek->meeting->week->week_midweek->living_item2_minutes), 'h:mm') ?></div></td>
    </tr>
    
    <?php endif; ?>
    
    
    <!-- cbs -->
    <?php if($meetingMidweek->meeting->meeting_week_type_id === 1): ?>
    <tr>
        <td style="border-bottom:1px solid #82031D;"><div style="padding-bottom:2px;margin-bottom:2px;">Congregation Bible Study (30 min.)</div></td>
        <td style="border-bottom:1px solid #82031D;"><div style="padding-bottom:2px;margin-bottom:2px;"><b><?= ($meetingMidweek->has('living_cbs_conductor')) ? $meetingMidweek->living_cbs_conductor->full_name : '' ?></b><br><?= ($meetingMidweek->has('living_cbs_reader')) ? '<b>'.$meetingMidweek->living_cbs_reader->full_name.'</b>&nbsp;&nbsp;(Reader)' : '&nbsp;' ?></div></td>
        <td style="border-bottom:1px solid #82031D;"><div class="text-right" style="padding-bottom:2px;margin-bottom:2px;"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(30), 'h:mm') ?></div></td>
    </tr>    
    <?php endif; ?>
    
    
    <!-- closing comments -->
    <tr>
        <td colspan="2"><div style="padding-bottom:2px;border-bottom:1px solid #82031D;margin-bottom:2px;height:100px;font-size:11px;">Review followed by Preview of Next Week (3 min.)</div></td>
        <td><div class="text-right" style="padding-bottom:2px;border-bottom:1px solid #82031D;margin-bottom:2px;height:100px;"><?= $this->Time->format($meetingMidweek->meeting->installation->time_midweek->addMinutes(3), 'h:mm') ?></div></td>
    </tr>
    
    <?php if($meetingMidweek->meeting->meeting_week_type_id === 2): ?>
    <!-- CO -->
    <tr>
        <td><?= $meetingMidweek->co_talk_theme ?></td>
        <td><b><?= $meetingMidweek->meeting->installation->circuit_overseer_name ?></b></td>
    </tr>
    
    <?php endif; ?>
    
    <!-- song & prayer -->
    <tr>
        <td>Song <?= $meetingMidweek->closing_song_number ?> and Prayer</td>
        <td><?= ($meetingMidweek->has('closing_prayer')) ? '<b>'.$meetingMidweek->closing_prayer->full_name.'</b>&nbsp;&nbsp;(Prayer)' : '&nbsp;' ?></td>
    </tr>
        
    
</table>


<?php
if($debug):
    debug(json_encode($meetingMidweek, JSON_PRETTY_PRINT));
endif;
?>