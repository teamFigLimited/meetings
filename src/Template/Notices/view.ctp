<div class="page-header">
    <h2><?= $notice->name ?></h2>
</div>

<div class="well well-lg">
    <p><?= $notice->description ?></p>
</div>

<?php
if($debug):
    debug(json_encode($notice, JSON_PRETTY_PRINT));
endif;
?>

