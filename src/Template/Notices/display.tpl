<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">Notices</h4>
            </div>
            <div class="list-group">
                {% for notice in notices %}
                <li class="list-group-item">
                  <h4 class="list-group-item-heading">{{ notice.name }}<small class="r">{{ notice.created }}</small></h4>
                  <p class="list-group-item-text">{{ notice.description }}</p>
                </li>
                {% endfor %}
            </div>
        </div>
    </div>
</div>
<div class="paginator">
    <ul class="pagination">
        {{ Paginator.prev('< previous')|raw }}
        {{ Paginator.numbers()|raw }}
        {{ Paginator.next('next >')|raw }}
    </ul>
    <p>{{ Paginator.counter()|raw }}</p>
</div>
