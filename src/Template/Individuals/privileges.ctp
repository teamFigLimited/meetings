<div class="page-header">
    <h2 class="h2">Edit Individual Privileges <small class="text-uppercase"><?= $individual->full_name ?></small></h2>
</div>

<?= $this->Form->create($individual) ?>
<?= $this->Form->button(__('Save Changes'), ['class' => 'btn btn-success pull-right']) ?>

<div class="row">

    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Platform - Teaching</h4>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <?= $this->Form->input('privileges1._ids', ['options' => $privileges[1], 'class' => 'form-control', 'label' => false, 'size' => 10, 'default' => array_keys($individualPrivileges)]) ?>
                </li>
            </ul>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Platform - Reading</h4>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <?= $this->Form->input('privileges2._ids', ['options' => $privileges[2], 'class' => 'form-control', 'label' => false, 'size' => 10, 'default' => array_keys($individualPrivileges)]) ?>
                </li>
            </ul>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Platform - Other</h4>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <?= $this->Form->input('privileges3._ids', ['options' => $privileges[3], 'class' => 'form-control', 'label' => false, 'size' => 10, 'default' => array_keys($individualPrivileges)]) ?>
                </li>
            </ul>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Duties</h4>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <?= $this->Form->input('privileges4._ids', ['options' => $privileges[4], 'class' => 'form-control', 'label' => false, 'size' => 10, 'default' => array_keys($individualPrivileges)]) ?>
                </li>
            </ul>
        </div>
    </div>

</div>

<?= $this->Form->end() ?>


<?php debug(json_encode($individual, JSON_PRETTY_PRINT)); ?>
<?php debug($individualPrivileges); ?>