<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Individual'), ['action' => 'edit', $individual->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Individual'), ['action' => 'delete', $individual->id], ['confirm' => __('Are you sure you want to delete # {0}?', $individual->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Individuals'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Individual'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Congregation'), ['controller' => 'Congregations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Congregation'), ['controller' => 'Congregations', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Gender'), ['controller' => 'Gender', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Gender'), ['controller' => 'Gender', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Appointment'), ['controller' => 'Appointments', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Appointment'), ['controller' => 'Appointments', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Family Head'), ['controller' => 'Individuals', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Family Head'), ['controller' => 'Individuals', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Meeting Livings'), ['controller' => 'MeetingLivings', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Meeting Living'), ['controller' => 'MeetingLivings', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Meeting Prayers'), ['controller' => 'MeetingPrayers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Meeting Prayer'), ['controller' => 'MeetingPrayers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Privileges'), ['controller' => 'Privileges', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Privilege'), ['controller' => 'Privileges', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="individuals view large-9 medium-8 columns content">
    <h3><?= h($individual->full_name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Congregation') ?></th>
            <td><?= $individual->has('congregation') ? $this->Html->link($individual->congregation->name, ['controller' => 'Congregations', 'action' => 'view', $individual->congregation->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('First Name') ?></th>
            <td><?= h($individual->first_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Last Name') ?></th>
            <td><?= h($individual->last_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Gender') ?></th>
            <td><?= $individual->has('gender') ? $this->Html->link($individual->gender->name, ['controller' => 'Gender', 'action' => 'view', $individual->gender->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Appointment') ?></th>
            <td><?= $individual->has('appointment') ? $this->Html->link($individual->appointment->name, ['controller' => 'Appointments', 'action' => 'view', $individual->appointment->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Family Head') ?></th>
            <td><?= $individual->has('family_head') ? $this->Html->link($individual->family_head->full_name, ['controller' => 'Individuals', 'action' => 'view', $individual->family_head->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($individual->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Unbaptised Publisher') ?></th>
            <td><?= $this->Number->format($individual->is_unbaptised_publisher) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Baptised') ?></th>
            <td><?= $this->Number->format($individual->is_baptised) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Bro Sis') ?></th>
            <td><?= $this->Number->format($individual->is_bro_sis) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Clm Enrolled') ?></th>
            <td><?= $this->Number->format($individual->is_clm_enrolled) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($individual->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($individual->modified) ?></tr>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Meeting Livings') ?></h4>
        <?php if (!empty($individual->meeting_livings)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Meeting Id') ?></th>
                <th><?= __('Sort') ?></th>
                <th><?= __('Theme') ?></th>
                <th><?= __('Material') ?></th>
                <th><?= __('Minutes') ?></th>
                <th><?= __('Individual Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($individual->meeting_livings as $meetingLivings): ?>
            <tr>
                <td><?= h($meetingLivings->id) ?></td>
                <td><?= h($meetingLivings->meeting_id) ?></td>
                <td><?= h($meetingLivings->sort) ?></td>
                <td><?= h($meetingLivings->theme) ?></td>
                <td><?= h($meetingLivings->material) ?></td>
                <td><?= h($meetingLivings->minutes) ?></td>
                <td><?= h($meetingLivings->individual_id) ?></td>
                <td><?= h($meetingLivings->created) ?></td>
                <td><?= h($meetingLivings->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'MeetingLivings', 'action' => 'view', $meetingLivings->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'MeetingLivings', 'action' => 'edit', $meetingLivings->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'MeetingLivings', 'action' => 'delete', $meetingLivings->id], ['confirm' => __('Are you sure you want to delete # {0}?', $meetingLivings->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Meeting Prayers') ?></h4>
        <?php if (!empty($individual->meeting_prayers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Meeting Id') ?></th>
                <th><?= __('Individual Id') ?></th>
                <th><?= __('Position') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($individual->meeting_prayers as $meetingPrayers): ?>
            <tr>
                <td><?= h($meetingPrayers->id) ?></td>
                <td><?= h($meetingPrayers->meeting_id) ?></td>
                <td><?= h($meetingPrayers->individual_id) ?></td>
                <td><?= h($meetingPrayers->position) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'MeetingPrayers', 'action' => 'view', $meetingPrayers->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'MeetingPrayers', 'action' => 'edit', $meetingPrayers->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'MeetingPrayers', 'action' => 'delete', $meetingPrayers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $meetingPrayers->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Individuals') ?></h4>
        <?php if (!empty($individual->children)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Congregation Id') ?></th>
                <th><?= __('Is Unbaptised Publisher') ?></th>
                <th><?= __('Is Baptised') ?></th>
                <th><?= __('Is Bro Sis') ?></th>
                <th><?= __('First Name') ?></th>
                <th><?= __('Last Name') ?></th>
                <th><?= __('Gender Id') ?></th>
                <th><?= __('Appointment Id') ?></th>
                <th><?= __('Family Head Id') ?></th>
                <th><?= __('Is Clm Enrolled') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($individual->children as $children): ?>
            <tr>
                <td><?= h($children->id) ?></td>
                <td><?= h($children->congregation_id) ?></td>
                <td><?= h($children->is_unbaptised_publisher) ?></td>
                <td><?= h($children->is_baptised) ?></td>
                <td><?= h($children->is_bro_sis) ?></td>
                <td><?= h($children->first_name) ?></td>
                <td><?= h($children->last_name) ?></td>
                <td><?= h($children->gender_id) ?></td>
                <td><?= h($children->appointment_id) ?></td>
                <td><?= h($children->family_head_id) ?></td>
                <td><?= h($children->is_clm_enrolled) ?></td>
                <td><?= h($children->created) ?></td>
                <td><?= h($children->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Individuals', 'action' => 'view', $children->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Individuals', 'action' => 'edit', $children->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Individuals', 'action' => 'delete', $children->id], ['confirm' => __('Are you sure you want to delete # {0}?', $children->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Privileges') ?></h4>
        <?php if (!empty($individual->privileges)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Privilege Type Id') ?></th>
                <th><?= __('Is Sister') ?></th>
                <th><?= __('Is Brother') ?></th>
                <th><?= __('Is Qms') ?></th>
                <th><?= __('Is Elder') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($individual->privileges as $privileges): ?>
            <tr>
                <td><?= h($privileges->id) ?></td>
                <td><?= h($privileges->name) ?></td>
                <td><?= h($privileges->privilege_type_id) ?></td>
                <td><?= h($privileges->is_sister) ?></td>
                <td><?= h($privileges->is_brother) ?></td>
                <td><?= h($privileges->is_qms) ?></td>
                <td><?= h($privileges->is_elder) ?></td>
                <td><?= h($privileges->created) ?></td>
                <td><?= h($privileges->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Privileges', 'action' => 'view', $privileges->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Privileges', 'action' => 'edit', $privileges->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Privileges', 'action' => 'delete', $privileges->id], ['confirm' => __('Are you sure you want to delete # {0}?', $privileges->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
