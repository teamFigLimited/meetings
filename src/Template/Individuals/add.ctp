<div class="page-header">
    <h1>Add an Individual</h1>
</div>

<?= $this->Form->create($individual) ?>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Basic Information</h4>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <?= $this->Form->input('first_name', ['class' => 'form-control']) ?>
                </li>
                <li class="list-group-item">
                    <?= $this->Form->input('last_name', ['class' => 'form-control']) ?>
                </li>
                <li class="list-group-item">
                    <?= $this->Form->input('gender_id', ['class' => 'form-control']) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Standing</h4>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <?= $this->Form->input('is_publisher', ['class' => 'form-control', 'options' => [0 => 'No', 1 => 'Yes'], 'empty' => true]) ?>
                </li>
                <li class="list-group-item">
                    <?= $this->Form->input('is_baptised', ['class' => 'form-control', 'options' => [0 => 'No', 1 => 'Yes'], 'empty' => true]) ?>
                </li>
                <li class="list-group-item">
                    <?= $this->Form->input('is_bro_sis', ['label' => 'Salutation (Bro. / Sis.)', 'class' => 'form-control', 'options' => [0 => 'No', 1 => 'Yes']]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Family</h4>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <?= $this->Form->input('congregation_id', ['class' => 'form-control']) ?>
                </li>
                <li class="list-group-item">
                    <?= $this->Form->input('is_family_head', ['class' => 'form-control', 'options' => [0 => 'No', 1 => 'Yes'], 'default' => ($individual->family_head_id === 0) ? 1 : 0 ]) ?>
                </li>
                <li class="list-group-item">
                    <?= $this->Form->input('family_head_id', ['empty' => true, 'class' => 'form-control']) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Privileges</h4>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <?= $this->Form->input('appointment_id', ['class' => 'form-control']) ?>
                </li>
            </ul>
        </div>
    </div>
</div>
<?= $this->Form->button(__('Add Individual'), ['class' => 'btn btn-success']) ?>
<?= $this->Form->end() ?>

<?php
if($debug):
    debug(json_encode($individual, JSON_PRETTY_PRINT));
endif;
?>

