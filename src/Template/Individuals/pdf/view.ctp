<?= $this->Html->css('spacelab.min', ['fullBase' => true]) ?>
<?= $this->Html->css('css', ['fullBase' => true]) ?>
<?= $this->fetch('css') ?>
<style>
    @page { margin: 0;}
    body { margin: 10mm 5mm 0 5mm; font-size:12px;}
    h2 {padding: 0 8px;margin: 0;}
    h4 { border-bottom-color: rgb(238, 238, 238); border-bottom-style: solid; border-bottom-width: 1px; }
    h6 { padding: 4px 8px 2px 8px;margin:0;}
    p {margin:0;padding:0;}
    .page-header {margin: 5px 0 5px 0;}
    .page-header p{padding:0 8px;}
    .alert {font-size:10px;margin:0;padding:0;text-align:center;}
    .alert-treasures, .alert-ministry, .alert-living{padding:0;margin:0;}
    .text-success{margin:0;padding:0;}
    table {width:100%;border-bottom:1px solid #dddddd;padding:0 8px;margin:0;}
    tr, td {margin:0;padding:0;vertical-align:top;}
    
</style>

<div class="page-header">
    <h2>Our Christian Life and Ministry</h2>
    <p>Chairman's Worksheet</p>
</div>


<?= $individual->full_name ?>