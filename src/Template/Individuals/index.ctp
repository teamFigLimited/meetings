<div class="page-header">
    <h3><?= $this->Html->link('<span class="glyphicon glyphicon-plus-sign pull-right" aria-hidden="true"></span>', ['controller' => 'individuals', 'action' => 'add'], ['escape' => false]) ?></h3>
    <h1>Individuals</h1>
</div>

<div class="btn-group page-options" role="group" aria-label="page options">
    
    <div class="btn-group" role="group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Congregation
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li<?= ($congregationId == 0) ? ' class="active"' : '' ?>><?= $this->Html->link('-All Congregations-', ['controller' => 'individuals', 'action' => 'index', '?' => ['congregation_id' => 0]]) ?></li>
            <?php foreach($congregations as $congregation): ?>
            <li<?= ($congregation->id == $congregationId) ? ' class="active"' : '' ?>><?= $this->Html->link($congregation->name, ['controller' => 'individuals', 'action' => 'index', '?' => ['congregation_id' => $congregation->id]]) ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
    
</div>


<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Congregation</th>
                <th>Appointment</th>
                <th>Family Head</th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($individuals as $individual): ?>
            <tr>
                <td><?= $this->Number->format($individual->id) ?></td>
                <td><?= h($individual->reverse_name) ?></td>
                <td><?= $individual->has('congregation') ? $this->Html->link($individual->congregation->name, ['controller' => 'Congregations', 'action' => 'view', $individual->congregation->id]) : '' ?></td>
                <td><?= $individual->appointment->name ?></td>
                <td><?= ((isset($individual->family_head->id)) ? $individual->family_head->full_name : (($individual->family_head_id === 0) ? $individual->full_name : '-' )) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $individual->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $individual->id], ['confirm' => __('Are you sure you want to delete # {0}?', $individual->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php if($debug): ?>
<?php debug(json_encode($individuals, JSON_PRETTY_PRINT)); ?>
<?php endif; ?>