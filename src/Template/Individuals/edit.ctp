<div class="page-header">
    <h2 class="h2">Edit Individual <small class="text-uppercase"><?= $individual->full_name ?></small></h2>
</div>


<?= $this->Form->create($individual) ?>
<?= $this->Form->button(__('Save Changes'), ['class' => 'btn btn-success pull-right']) ?>

<div class="row">
    <div class="col-xs-12">
        <ul class="nav nav-tabs tablist" role="tablist">
            <li id="tab1" class="tab active" role="tab" aria-controls="panel1" aria-selected="true"><a>Individual</a></li>
            <li id="tab2" class="tab" role="tab" aria-controls="panel2" aria-selected="false"><a>Privileges</a></li>
            <?php if(in_array(15, array_keys($individualPrivileges)) || in_array(16, array_keys($individualPrivileges))): ?> <!-- public talks in / out -->
            <li id="tab3" class="tab" role="tab" aria-controls="panel3" aria-selected="false"><a>Outlines</a></li>
            <?php endif; ?>
            <?php if(in_array(23, array_keys($individualPrivileges))): ?> <!-- CLM Enrolled -->
            <li id="tab4" class="tab" role="tab" aria-controls="panel4" aria-selected="false"><a>Counsel</a></li>
            <?php endif; ?>
        </ul>
    </div>
</div>
<br>

<div id="panel1" class="panel" aria-labelledby="tab1" role="tabpanel" aria-hidden="false">       
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Basic Information</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">
                        <?= $this->Form->input('first_name', ['class' => 'form-control']) ?>
                    </li>
                    <li class="list-group-item">
                        <?= $this->Form->input('last_name', ['class' => 'form-control']) ?>
                    </li>
                    <li class="list-group-item">
                        <?= $this->Form->input('gender_id', ['class' => 'form-control']) ?>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Standing</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">
                        <?= $this->Form->input('is_publisher', ['class' => 'form-control', 'options' => [0 => 'No', 1 => 'Yes'], 'empty' => true]) ?>
                    </li>
                    <li class="list-group-item">
                        <?= $this->Form->input('is_baptised', ['class' => 'form-control', 'options' => [0 => 'No', 1 => 'Yes'], 'empty' => true]) ?>
                    </li>
                    <li class="list-group-item">
                        <?= $this->Form->input('is_bro_sis', ['label' => 'Salutation (Bro. / Sis.)', 'class' => 'form-control', 'options' => [0 => 'No', 1 => 'Yes']]) ?>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Family</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">
                        <?= $this->Form->input('is_family_head', ['class' => 'form-control', 'options' => [0 => 'No', 1 => 'Yes'], 'default' => ($individual->family_head_id === 0) ? 1 : 0 ]) ?>
                    </li>
                    <li class="list-group-item">
                        <?= $this->Form->input('family_head_id', ['empty' => true, 'class' => 'form-control']) ?>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Privileges</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">
                        <?= $this->Form->input('appointment_id', ['class' => 'form-control']) ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>


<div id="panel2" class="panel" aria-labelledby="tab2" role="tabpanel" aria-hidden="true">
    
    <div class="well">
        <h3>Important!</h3>
        <p>Make sure you have the Command Key (Mac) or Control Key (Windows, Linux) pressed down before clicking your selections. Failure will result in all other selections being lost. If this happens simply reload the page before saving.</p>
        <p>Mobile devices have their own method of multi-selecting.</p>
    </div>
    <div class="row">

        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Platform - Teaching</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">
                        <?= $this->Form->input('privileges1._ids', ['options' => $privileges[1], 'class' => 'form-control', 'label' => false, 'size' => 10, 'default' => array_keys($individualPrivileges)]) ?>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">CLM Assignments</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">
                        <?= $this->Form->input('privileges2._ids', ['options' => $privileges[2], 'class' => 'form-control', 'label' => false, 'size' => 10, 'default' => array_keys($individualPrivileges)]) ?>
                    </li>
                </ul>
                <div class="panel-body">
                    <p class="text-info"><i>CLM Enrolled</i> has to be selected for any other selection to work.</p>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Platform - Other</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">
                        <?= $this->Form->input('privileges3._ids', ['options' => $privileges[3], 'class' => 'form-control', 'label' => false, 'size' => 10, 'default' => array_keys($individualPrivileges)]) ?>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Duties</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">
                        <?= $this->Form->input('privileges4._ids', ['options' => $privileges[4], 'class' => 'form-control', 'label' => false, 'size' => 10, 'default' => array_keys($individualPrivileges)]) ?>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</div>


<div id="panel3" class="panel" aria-labelledby="tab3" role="tabpanel" aria-hidden="false">      
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Public Talk Outlines</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">
                        <?= $this->Form->input('outlines._ids', ['class' => 'form-control', 'label' => false, 'size' => 10]) ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>


<div id="panel4" class="panel" aria-labelledby="tab4" role="tabpanel" aria-hidden="false">      
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Student Counsel</h4>
                </div>
                
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Counsel</th>
                            <th>Assigned</th>
                            <th>Completed</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($individual->studies as $study): ?>
                        <tr>
                            <td><?= $study->name ?></td>
                            <td><?= $study->_joinData->date_assigned ?></td>
                            <td><?= $study->_joinData->date_completed ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                
                
                <ul class="list-group">
                    <li class="list-group-item">
                        
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<?= $this->Form->end() ?>

<?php
if($debug):
    debug(json_encode($individual, JSON_PRETTY_PRINT));
    debug($individualPrivileges);
endif;
?>