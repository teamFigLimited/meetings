<div class="page-header">
    <h1>Add a Counsel Study</h1>
</div>

<?= $this->Form->create($study) ?>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Study</h4>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <?= $this->Form->input('number', ['class' => 'form-control']) ?>
                </li>
                <li class="list-group-item">
                    <?= $this->Form->input('name', ['class' => 'form-control']) ?>
                </li>
                <li class="list-group-item">
                    <?= $this->Form->input('is_reading', ['class' => 'form-control', 'label' => 'Reading', 'options' => [0 => 'No', 1 => 'Yes']]) ?>
                </li>
                <li class="list-group-item">
                    <?= $this->Form->input('is_demonstration', ['class' => 'form-control', 'label' => 'Demonstration', 'options' => [0 => 'No', 1 => 'Yes']]) ?>
                </li>
                <li class="list-group-item">
                    <?= $this->Form->input('is_discourse', ['class' => 'form-control', 'label' => 'Discourse', 'options' => [0 => 'No', 1 => 'Yes']]) ?>
                </li>
                <li class="list-group-item">
                    <?= $this->Form->button(__('Add'), ['class' => ['form-control btn btn-success']]) ?>
                </li>
            </ul>
        </div>
    </div>
</div>

<?= $this->Form->end() ?>

