<div class="page-header">
    <h3><?= $this->Html->link('<span class="glyphicon glyphicon-plus-sign pull-right" aria-hidden="true"></span>', ['controller' => 'studies', 'action' => 'add'], ['escape' => false]) ?></h3>
    <h1>Counsel Study List</h1>
</div>

<table class="table table-striped">
    <thead>
        <tr>
            <th>Number</th>
            <th>Name</th>
            <th>Reading</th>
            <th>Demonstration</th>
            <th>Discourse</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($studies as $study): ?>
        <tr>
            <td><?= $this->Number->format($study->id) ?></td>
            <td><?= h($study->name) ?></td>
            <td><?= ($study->is_reading == 1) ? 'Y' : '' ?></td>
            <td><?= ($study->is_demonstration == 1) ? 'Y' : '' ?></td>
            <td><?= ($study->is_discourse == 1) ? 'Y' : '' ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>


<?php
if($debug):
    debug(json_encode($studies, JSON_PRETTY_PRINT));
endif;
?>

