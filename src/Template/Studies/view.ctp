<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Study'), ['action' => 'edit', $study->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Study'), ['action' => 'delete', $study->id], ['confirm' => __('Are you sure you want to delete # {0}?', $study->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Studies'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Study'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="studies view large-9 medium-8 columns content">
    <h3><?= h($study->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($study->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($study->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Number') ?></th>
            <td><?= $this->Number->format($study->number) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Reading') ?></th>
            <td><?= $this->Number->format($study->is_reading) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Demonstration') ?></th>
            <td><?= $this->Number->format($study->is_demonstration) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Discourse') ?></th>
            <td><?= $this->Number->format($study->is_discourse) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($study->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($study->modified) ?></td>
        </tr>
    </table>
</div>
