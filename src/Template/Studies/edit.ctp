<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $study->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $study->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Studies'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="studies form large-9 medium-8 columns content">
    <?= $this->Form->create($study) ?>
    <fieldset>
        <legend><?= __('Edit Study') ?></legend>
        <?php
            echo $this->Form->input('number');
            echo $this->Form->input('name');
            echo $this->Form->input('is_reading');
            echo $this->Form->input('is_demonstration');
            echo $this->Form->input('is_discourse');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
