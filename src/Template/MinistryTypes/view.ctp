<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ministry Type'), ['action' => 'edit', $ministryType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ministry Type'), ['action' => 'delete', $ministryType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ministryType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ministry Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ministry Type'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ministryTypes view large-9 medium-8 columns content">
    <h3><?= h($ministryType->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($ministryType->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($ministryType->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($ministryType->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($ministryType->modified) ?></td>
        </tr>
    </table>
</div>
