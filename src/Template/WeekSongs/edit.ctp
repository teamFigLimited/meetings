<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $weekSong->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $weekSong->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Week Songs'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Weeks'), ['controller' => 'Weeks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Week'), ['controller' => 'Weeks', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Songs'), ['controller' => 'Songs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Song'), ['controller' => 'Songs', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="weekSongs form large-9 medium-8 columns content">
    <?= $this->Form->create($weekSong) ?>
    <fieldset>
        <legend><?= __('Edit Week Song') ?></legend>
        <?php
            echo $this->Form->input('week_id', ['options' => $weeks, 'empty' => true]);
            echo $this->Form->input('song_id', ['options' => $songs, 'empty' => true]);
            echo $this->Form->input('position');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
