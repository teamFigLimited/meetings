<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Week Song'), ['action' => 'edit', $weekSong->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Week Song'), ['action' => 'delete', $weekSong->id], ['confirm' => __('Are you sure you want to delete # {0}?', $weekSong->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Week Songs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Week Song'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Weeks'), ['controller' => 'Weeks', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Week'), ['controller' => 'Weeks', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Songs'), ['controller' => 'Songs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Song'), ['controller' => 'Songs', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="weekSongs view large-9 medium-8 columns content">
    <h3><?= h($weekSong->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Week') ?></th>
            <td><?= $weekSong->has('week') ? $this->Html->link($weekSong->week->id, ['controller' => 'Weeks', 'action' => 'view', $weekSong->week->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Song') ?></th>
            <td><?= $weekSong->has('song') ? $this->Html->link($weekSong->song->title, ['controller' => 'Songs', 'action' => 'view', $weekSong->song->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($weekSong->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Position') ?></th>
            <td><?= $this->Number->format($weekSong->position) ?></td>
        </tr>
    </table>
</div>
