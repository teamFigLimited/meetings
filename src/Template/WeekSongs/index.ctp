<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Week Song'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Weeks'), ['controller' => 'Weeks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Week'), ['controller' => 'Weeks', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Songs'), ['controller' => 'Songs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Song'), ['controller' => 'Songs', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="weekSongs index large-9 medium-8 columns content">
    <h3><?= __('Week Songs') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('week_id') ?></th>
                <th><?= $this->Paginator->sort('song_id') ?></th>
                <th><?= $this->Paginator->sort('position') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($weekSongs as $weekSong): ?>
            <tr>
                <td><?= $this->Number->format($weekSong->id) ?></td>
                <td><?= $weekSong->has('week') ? $this->Html->link($weekSong->week->id, ['controller' => 'Weeks', 'action' => 'view', $weekSong->week->id]) : '' ?></td>
                <td><?= $weekSong->has('song') ? $this->Html->link($weekSong->song->title, ['controller' => 'Songs', 'action' => 'view', $weekSong->song->id]) : '' ?></td>
                <td><?= $this->Number->format($weekSong->position) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $weekSong->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $weekSong->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $weekSong->id], ['confirm' => __('Are you sure you want to delete # {0}?', $weekSong->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
