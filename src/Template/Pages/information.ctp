<div class="page-header">
    <h1>
        <kbd>fig</kbd>meetings <small class="text-uppercase">Information</small>
    </h1>
</div>

<ul class="nav nav-pills">
    <li role="presentation" class="active"><?= $this->Html->link('Information', ['controller' => 'pages', 'action' => 'display', 'information']) ?></li>
    <li role="presentation"><?= $this->Html->link('FAQ\'s', ['controller' => 'pages', 'action' => 'display', 'faqs']) ?></li>
    <li role="presentation"><?= $this->Html->link('Tutorials', ['controller' => 'pages', 'action' => 'display', 'tutorials']) ?></li>
    <li role="presentation"><?= $this->Html->link('Tickets', ['controller' => 'pages', 'action' => 'display', 'tickets']) ?></li>
    <li role="presentation" class="pull-right"><?= $this->Html->link('Home', ['controller' => 'pages', 'action' => 'display', 'home']) ?></li>
</ul>

<h3>Information</h3>
<p><kbd>fig</kbd> meetings has been created to help with scheduling congregation meetings at the Kingdom Hall. Other features will be added as time permits such as Microphone Rota, Cleaning Rota, and so on.</p>
<p>For more specific information see the relevant <?= $this->Html->link('FAQ', ['controller' => 'pages', 'action' => 'display', 'faqs']) ?> or <?= $this->Html->link('tutorial', ['controller' => 'pages', 'action' => 'display', 'tutorials']) ?>. If you still can't find what you are looking for please create a <?= $this->Html->link('Support Ticket', ['controller' => 'pages', 'action' => 'display', 'tickets']) ?>.</p>