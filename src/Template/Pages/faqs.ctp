<div class="page-header">
    <h1>
        <kbd>fig</kbd>meetings <small class="text-uppercase">Information</small>
    </h1>
</div>

<ul class="nav nav-pills">
    <li role="presentation"><?= $this->Html->link('Information', ['controller' => 'pages', 'action' => 'display', 'information']) ?></li>
    <li role="presentation" class="active"><?= $this->Html->link('FAQ\'s', ['controller' => 'pages', 'action' => 'display', 'faqs']) ?></li>
    <li role="presentation"><?= $this->Html->link('Tutorials', ['controller' => 'pages', 'action' => 'display', 'tutorials']) ?></li>
    <li role="presentation"><?= $this->Html->link('Tickets', ['controller' => 'pages', 'action' => 'display', 'tickets']) ?></li>
    <li role="presentation" class="pull-right"><?= $this->Html->link('Home', ['controller' => 'pages', 'action' => 'display', 'home']) ?></li>
</ul>

<h3>FAQ's</h3>
<ol>
    <li>
        <h5><b>What is <kbd>fig</kbd> meetings?</b></h5>
        <p><kbd>fig</kbd> meetings is a web-based application that helps with the meeting scheduling for congregations that attend Kingdom Halls.</p>
    </li>
    <li>
        <h5><b>Who can use this application?</b></h5>
        <p>Anyone. But it is really for anyone who is involved in any way to organise meetings at Kingdom Halls. There is no restriction as each registered user has their own 'space' to use which doesn't affect any other users space.</p>
    </li>
    <li>
        <h5><b>How much does it cost to use?</b></h5>
        <p>Nothing. You are free to use the application within the terms without any charge or other financial obligations.</p>
        <p>The <kbd>fig</kbd> suite of theocratic applications is funded entirely by optional donations. You can pay a modest monthly charge of £1 or a yearly charge of £10 to help towards the cost of developing this application, but it is entirely voluntary and any who choose not to pay still get full access to the application.</p>
    </li>
</ol>
