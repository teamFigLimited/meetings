<?php
    $this->layout = false;
?>
<!DOCTYPE html>
<html>
	<head>
		<?= $this->Html->charset() ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>
			Fig Meetings | Theocratic Software for Kingdom Hall Christian Life and Ministry Meeting Scheduling for Jehovah's Witnesses
		</title>
        <meta name="description" content="kingdom hall congregation meeting scheduling software for Jehovahs Witnesses using browser based services available on any device with an internet connection"
		<?= $this->Html->meta('icon') ?>

		<?= $this->Html->css('spacelab.min') ?>
		<?= $this->Html->css('sticky-footer') ?>
		<?= $this->Html->css('css') ?>

		<?= $this->fetch('meta') ?>
		<?= $this->fetch('css') ?>
		<?= $this->fetch('script') ?>

        <style>
            body {margin-top: 10px;}
        </style>

	</head>
    <body>

        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-70241209-2', 'auto');
        ga('send', 'pageview');
        </script>

        <div id="container" class="container">

            <div class="page-header">
                <h1 style="min-height:80px;color:#3f175f;">
                    meetings
                    <span class="pull-right"><img src="/img/fig-300-purple.png" alt="" style="width:65px;"/></span>
                </h1>
            </div>

            <div class="row" style="margin-top:30px;">

                <div class="col-xs-12 col-sm-8 col-md-9">

                    <p class="text-info" style="margin-bottom:30px;">Software to help schedule Our Christian Life and Ministry Meetings.</p>

                    <img src="/img/fig-meetings.png" alt="" style="width:100%;"/>

                </div>

                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">Options</div>
                        </div>
                        <div class="list-group">
                            <?= $this->Html->link('Register', ['controller' => 'users', 'action' => 'add'], ['class' => 'list-group-item']) ?>
                            <?php if($currentUser): ?>
                            <?= $this->Html->link('My Installation', ['controller' => 'installations', 'action' => 'index'], ['class' => 'list-group-item list-group-item-success']) ?>
                            <?= $this->Html->link('Log Out', ['controller' => 'users', 'action' => 'logout'], ['class' => 'list-group-item']) ?>
                            <?php else: ?>
                            <?= $this->Html->link('Log In', ['controller' => 'installations', 'action' => 'index'], ['class' => 'list-group-item list-group-item-success']) ?>
                            <?php endif; ?>
                            <?= $this->Html->link('More Info', ['controller' => 'pages', 'action' => 'display', 'information'], ['class' => 'list-group-item']) ?>
                            <?= $this->Html->link('Notices', ['controller' => 'notices', 'action' => 'display'], ['class' => 'list-group-item']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--


        <div class="home">
            <h1 class="text-center h1"><kbd>fig</kbd></h1>
            <p class="text-center">meetings</p>
        </div>

        <div id="container" class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-4 hidden-xs"></div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">Options</div>
                        </div>
                        <div class="list-group">
                            <?= $this->Html->link('Register', ['controller' => 'users', 'action' => 'add'], ['class' => 'list-group-item']) ?>
                            <?php if($currentUser): ?>
                            <?= $this->Html->link('My Installation', ['controller' => 'installations', 'action' => 'index'], ['class' => 'list-group-item list-group-item-success']) ?>
                            <?= $this->Html->link('Log Out', ['controller' => 'users', 'action' => 'logout'], ['class' => 'list-group-item']) ?>
                            <?php else: ?>
                            <?= $this->Html->link('Log In', ['controller' => 'installations', 'action' => 'index'], ['class' => 'list-group-item list-group-item-success']) ?>
                            <?php endif; ?>
                            <?= $this->Html->link('More Info', ['controller' => 'pages', 'action' => 'display', 'information'], ['class' => 'list-group-item']) ?>
                        </div>
                        <div class="panel-body">
                            <p class="text-info">If you have registered an account with any <kbd>fig</kbd> website, you can use that login here.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        -->

    </body>
</html>
