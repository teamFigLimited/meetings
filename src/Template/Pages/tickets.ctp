<div class="page-header">
    <h1>
        <kbd>fig</kbd>meetings <small class="text-uppercase">Support Tickets</small>
    </h1>
</div>

<ul class="nav nav-pills">
    <li role="presentation"><?= $this->Html->link('Information', ['controller' => 'pages', 'action' => 'display', 'information']) ?></li>
    <li role="presentation"><?= $this->Html->link('FAQ\'s', ['controller' => 'pages', 'action' => 'display', 'faqs']) ?></li>
    <li role="presentation"><?= $this->Html->link('Tutorials', ['controller' => 'pages', 'action' => 'display', 'tutorials']) ?></li>
    <li role="presentation" class="active"><?= $this->Html->link('Tickets', ['controller' => 'pages', 'action' => 'display', 'tickets']) ?></li>
    <li role="presentation" class="pull-right"><?= $this->Html->link('Home', ['controller' => 'pages', 'action' => 'display', 'home']) ?></li>
</ul>

<h3>Support Tickets</h3>
<p>Support Tickets allow users to contact the development team with specific requests, questions or observations. [In development]</p>
