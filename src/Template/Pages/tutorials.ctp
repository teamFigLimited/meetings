<div class="page-header">
    <h1>
        <kbd>fig</kbd>meetings <small class="text-uppercase">Tutorials</small>
    </h1>
</div>

<ul class="nav nav-pills">
    <li role="presentation"><?= $this->Html->link('Information', ['controller' => 'pages', 'action' => 'display', 'information']) ?></li>
    <li role="presentation"><?= $this->Html->link('FAQ\'s', ['controller' => 'pages', 'action' => 'display', 'faqs']) ?></li>
    <li role="presentation" class="active"><?= $this->Html->link('Tutorials', ['controller' => 'pages', 'action' => 'display', 'tutorials']) ?></li>
    <li role="presentation"><?= $this->Html->link('Tickets', ['controller' => 'pages', 'action' => 'display', 'tickets']) ?></li>
    <li role="presentation" class="pull-right"><?= $this->Html->link('Home', ['controller' => 'pages', 'action' => 'display', 'home']) ?></li>
</ul>

<h3>Tutorials</h3>
<ol>
    <li>
        <h5><b>Setting Up</b></h5>
        <p>This tutorial will show you how to get started with <kbd>fig</kbd> meetings. [TBA]</p>
    </li>
</ol>
