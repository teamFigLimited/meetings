<div class="page-header">
    <h2 class="h2">Create Installation Data <small>Logged In As: <?= $currentUser['username'] ?></small></h2>
</div>

<?= $this->Form->create($installation) ?>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Installation</h4>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <?= $this->Form->input('congregation_name', ['class' => 'form-control']) ?>
                </li>
                <li class="list-group-item">
                    <?= $this->Form->submit('Begin', ['class' => 'btn btn-success form-control']) ?>
                </li>
            </ul>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>