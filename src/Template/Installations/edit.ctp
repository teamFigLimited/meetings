<div class="page-header">
    <h2>Installation Edit <small class="text-uppercase"><?= $installation->congregation_name ?></small></h2>
</div>

<?= $this->Form->create($installation) ?>
<?= $this->Form->button(__('Save Changes'), ['class' => 'btn btn-success pull-right']) ?>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Settings</h4>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <?= $this->Form->input('congregation_id', ['label' => 'Local Congregation', 'class' => 'form-control']) ?>
                </li>
                <li class="list-group-item">
                    <?= $this->Form->input('circuit_overseer_name', ['class' => 'form-control']) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Service Committee</h4>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <?= $this->Form->input('coordinator_id', ['class' => 'form-control', 'empty' => true]) ?>
                </li>
                <li class="list-group-item">
                    <?= $this->Form->input('secretary_id', ['class' => 'form-control', 'empty' => true]) ?>
                </li>
                <li class="list-group-item">
                    <?= $this->Form->input('service_overseer_id', ['class' => 'form-control', 'empty' => true]) ?>
                </li>
            </ul>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>

