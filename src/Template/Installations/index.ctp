<div class="page-header">
    <h3><?= $this->Html->link('<span class="glyphicon glyphicon-plus-sign pull-right" aria-hidden="true"></span>', ['controller' => 'installations', 'action' => 'add'], ['escape' => false]) ?></h3>
	<h2 class="h2">Installations <small class="text-uppercase">List</small></h2>
</div>

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('congregation_id') ?></th>
                <th><?= $this->Paginator->sort('circuit_overseer_name', 'Circuit Overseer') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($installations as $installation): ?>
            <tr>
                <td><?= $this->Number->format($installation->id) ?></td>
                <td><?= $installation->congregation->name ?></td>
                <td><?= h($installation->circuit_overseer_name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $installation->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>

<?php if($debug): ?>
<?php debug(json_encode($installations, JSON_PRETTY_PRINT)); ?>
<?php endif; ?>