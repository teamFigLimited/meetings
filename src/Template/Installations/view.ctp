<div class="page-header">
	<span class="pull-right"><?= $currentUser['username'] ?> | <?= $this->Html->link($currentInstallation->congregation->name, ['controller' => 'installations', 'action' => 'view', $installation->id]) ?></span>
    <h3>Installation <small class="text-uppercase"><?= $currentInstallation->congregation->name ?></small></h3>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">Notices</h4>
            </div>
            <div class="list-group">
                <?php foreach($notices as $notice): ?>
                <?= $this->Html->link($notice->name.'<span class="pull-right"><span class="glyphicon glyphicon-expand" aria-hidden="true"></span></span>', ['controller' => 'notices', 'action' => 'view', $notice->id], ['class' => 'list-group-item', 'escape' => false]) ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>


<?php
if($debug):
    debug(json_encode($installation, JSON_PRETTY_PRINT));
endif;
?>
