<div class="page-header">
    <h1>Reports</h1>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">CLM</h4>
            </div>
            <div class="list-group">
                <?= $this->Html->link('Noticeboard, '.$currentMonth, ['controller' => 'months', 'action' => 'clmReport', $currentInstallation->id, 1, '_ext' => 'pdf'], ['class' => 'list-group-item']) ?>
                <?= $this->Html->link('Noticeboard, '.$nextMonth, ['controller' => 'months', 'action' => 'clmReport', $currentInstallation->id, 1, '_ext' => 'pdf'], ['class' => 'list-group-item']) ?>
            </div>
        </div>
    </div>
</div>

<?php
if($debug):
    debug($month);
endif;
?>