<div class="page-header">
    <h1>Add Weekend Section <small>Week <?= $week->full_date ?></small></h1>
</div>

<?= $this->Form->create($weekWeekend) ?>
<div class="row">
    <div class="col-xs-12 col-sm-6">
        <ul class="list-group">
            <li class="list-group-item">
                <?= $this->Form->input('middle_song_number', ['class' => 'form-control']) ?>
            </li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-6">
        <ul class="list-group">
            <li class="list-group-item">
                <?= $this->Form->input('closing_song_number', ['class' => 'form-control']) ?>
            </li>
        </ul>
    </div>
</div

<div class="row">
    <div class="col-xs-12 col-sm-6"></div>
    <div class="col-xs-12 col-sm-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">Watchtower<sub>&reg;</sub></div>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <?= $this->Form->input('wt_theme', ['class' => 'form-control', 'label' => 'Theme']) ?>
                </li>
            </ul>
        </div>
    </div>
</div>
<?= $this->Form->button(__('Submit')) ?>
<?= $this->Form->end() ?>




<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Week Weekends'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Weeks'), ['controller' => 'Weeks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Week'), ['controller' => 'Weeks', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="weekWeekends form large-9 medium-8 columns content">
    <?= $this->Form->create($weekWeekend) ?>
    <fieldset>
        <legend><?= __('Add Week Weekend') ?></legend>
        <?php
            echo $this->Form->input('week_id', ['type' => 'hidden', 'value' => $this->request->query['week_id']]);
            echo $this->Form->input('middle_song_number');
            echo $this->Form->input('wt_theme');
            echo $this->Form->input('closing_song_number');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>


<?php
if($debug):
    debug(json_encode($week, JSON_PRETTY_PRINT));
endif;
?>
