<div class="page-header">
    <h1>Edit Weekend Section <small>Week <?= $weekWeekend->week->full_date ?></small></h1>
</div>

<?= $this->Form->create($weekWeekend) ?>
<div class="row">
    <div class="col-xs-12 col-sm-6">
        <ul class="list-group">
            <li class="list-group-item">
                <?= $this->Form->input('middle_song_number', ['class' => 'form-control']) ?>
            </li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-6">
        <ul class="list-group">
            <li class="list-group-item">
                <?= $this->Form->input('closing_song_number', ['class' => 'form-control']) ?>
            </li>
        </ul>
    </div>
</div
<div class="row">
    <div class="col-xs-12 col-sm-6"></div>
    <div class="col-xs-12 col-sm-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">Watchtower<sub>&reg;</sub></div>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <?= $this->Form->input('wt_theme', ['class' => 'form-control', 'label' => 'Theme']) ?>
                </li>
            </ul>
        </div>
    </div>
</div>
<?= $this->Form->button(__('Submit')) ?>
<?= $this->Form->end() ?>


<?php 
if($debug): 
    debug(json_encode($weekWeekend, JSON_PRETTY_PRINT));
endif;
?>