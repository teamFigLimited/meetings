<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Week Weekend'), ['action' => 'edit', $weekWeekend->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Week Weekend'), ['action' => 'delete', $weekWeekend->id], ['confirm' => __('Are you sure you want to delete # {0}?', $weekWeekend->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Week Weekends'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Week Weekend'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Weeks'), ['controller' => 'Weeks', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Week'), ['controller' => 'Weeks', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="weekWeekends view large-9 medium-8 columns content">
    <h3><?= h($weekWeekend->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Week') ?></th>
            <td><?= $weekWeekend->has('week') ? $this->Html->link($weekWeekend->week->id, ['controller' => 'Weeks', 'action' => 'view', $weekWeekend->week->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Wt Theme') ?></th>
            <td><?= h($weekWeekend->wt_theme) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($weekWeekend->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Middle Song Number') ?></th>
            <td><?= $this->Number->format($weekWeekend->middle_song_number) ?></td>
        </tr>
        <tr>
            <th><?= __('Closing Song Number') ?></th>
            <td><?= $this->Number->format($weekWeekend->closing_song_number) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($weekWeekend->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($weekWeekend->modified) ?></td>
        </tr>
    </table>
</div>
