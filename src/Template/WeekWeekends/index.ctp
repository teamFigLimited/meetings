<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Week Weekend'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Weeks'), ['controller' => 'Weeks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Week'), ['controller' => 'Weeks', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="weekWeekends index large-9 medium-8 columns content">
    <h3><?= __('Week Weekends') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('week_id') ?></th>
                <th><?= $this->Paginator->sort('middle_song_number') ?></th>
                <th><?= $this->Paginator->sort('wt_theme') ?></th>
                <th><?= $this->Paginator->sort('closing_song_number') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($weekWeekends as $weekWeekend): ?>
            <tr>
                <td><?= $this->Number->format($weekWeekend->id) ?></td>
                <td><?= $weekWeekend->has('week') ? $this->Html->link($weekWeekend->week->id, ['controller' => 'Weeks', 'action' => 'view', $weekWeekend->week->id]) : '' ?></td>
                <td><?= $this->Number->format($weekWeekend->middle_song_number) ?></td>
                <td><?= h($weekWeekend->wt_theme) ?></td>
                <td><?= $this->Number->format($weekWeekend->closing_song_number) ?></td>
                <td><?= h($weekWeekend->created) ?></td>
                <td><?= h($weekWeekend->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $weekWeekend->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $weekWeekend->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $weekWeekend->id], ['confirm' => __('Are you sure you want to delete # {0}?', $weekWeekend->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
