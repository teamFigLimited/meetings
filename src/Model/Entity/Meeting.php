<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Meeting Entity.
 *
 * @property int $id
 * @property int $installation_id
 * @property \App\Model\Entity\Installation $installation
 * @property int $week_id
 * @property \App\Model\Entity\Week $week
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\MeetingMidweek[] $meeting_midweeks
 * @property \App\Model\Entity\MeetingPrayer[] $meeting_prayers
 * @property \App\Model\Entity\MeetingPt[] $meeting_pts
 * @property \App\Model\Entity\MeetingWt[] $meeting_wts
 */
class Meeting extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
