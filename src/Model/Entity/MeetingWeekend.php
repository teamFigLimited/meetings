<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MeetingWeekend Entity.
 *
 * @property int $id
 * @property int $meeting_id
 * @property \App\Model\Entity\Meeting $meeting
 * @property \Cake\I18n\Time $time_meeting
 * @property int $chairman_id
 * @property \App\Model\Entity\Chairman $chairman
 * @property int $speaker_id
 * @property \App\Model\Entity\Speaker $speaker
 * @property string $speaker_name
 * @property int $congregation_id
 * @property \App\Model\Entity\Congregation $congregation
 * @property string $congregation_name
 * @property int $middle_song_number
 * @property int $wt_reader_id
 * @property \App\Model\Entity\WtReader $wt_reader
 * @property int $closing_song_number
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class MeetingWeekend extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
