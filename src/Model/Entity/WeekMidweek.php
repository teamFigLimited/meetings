<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WeekMidweek Entity.
 *
 * @property int $id
 * @property int $week_id
 * @property \App\Model\Entity\Week $week
 * @property string $treasures_talk_theme
 * @property string $treasures_talk_material
 * @property string $treasures_gems_material
 * @property string $treasures_reading_material
 * @property string $ministry_presentation_material
 * @property string $ministry_initial_material
 * @property string $ministry_return_material
 * @property string $ministry_study_material
 * @property string $cbs_material
 */
class WeekMidweek extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
