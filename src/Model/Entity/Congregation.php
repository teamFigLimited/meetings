<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Congregation Entity.
 *
 * @property int $id
 * @property string $name
 * @property int $installation_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Installation[] $installations
 * @property \App\Model\Entity\Individual[] $individuals
 */
class Congregation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
