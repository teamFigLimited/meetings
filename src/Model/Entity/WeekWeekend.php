<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WeekWeekend Entity.
 *
 * @property int $id
 * @property int $week_id
 * @property \App\Model\Entity\Week $week
 * @property int $middle_song_number
 * @property string $wt_theme
 * @property int $closing_song_number
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class WeekWeekend extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
