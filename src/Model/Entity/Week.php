<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\I18n\Time;

/**
 * Week Entity.
 *
 * @property int $id
 * @property int $month_id
 * @property \App\Model\Entity\Month $month
 * @property \Cake\I18n\Time $date_monday
 * @property \App\Model\Entity\Meeting[] $meetings
 * @property \App\Model\Entity\WeekLiving[] $week_livings
 * @property \App\Model\Entity\WeekMidweek[] $week_midweeks
 * @property \App\Model\Entity\WeekSong[] $week_songs
 * @property \App\Model\Entity\WeekWeekend[] $week_weekends
 * @property \App\Model\Entity\WeekWt[] $week_wts
 */
class Week extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
    
    
	protected function _getFullDate()
	{
		
		$date1 = $time = Time::createFromFormat(
			'Y-m-d H:i:s',
			$this->_properties['date_monday']->format('Y-m-d H:i:s'),
			'Europe/London'
		);
		$month1 = $date1->format('F');
		$day1 = $date1->format('j');
		
		$date2 = $date1->modify('+6 days');
		$month2 = $date2->format('F');
		$day2 = $date2->format('j');
		
		return $month1.' '.$day1.(($month2 != $month1) ? ' - '.$month2 : ' -').' '.$day2;
	}
    	
}
