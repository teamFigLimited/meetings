<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Individual Entity.
 *
 * @property int $id
 * @property int $congregation_id
 * @property \App\Model\Entity\Congregation $congregation
 * @property string $first_name
 * @property string $last_name
 * @property int $gender_id
 * @property \App\Model\Entity\Gender $gender
 * @property int $appointment_id
 * @property \App\Model\Entity\Appointment $appointment
 * @property int $family_head_id
 * @property \App\Model\Entity\FamilyHead $family_head
 * @property int $is_clm_enrolled
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\MeetingLiving[] $meeting_livings
 * @property \App\Model\Entity\MeetingPrayer[] $meeting_prayers
 * @property \App\Model\Entity\Privilege[] $privileges
 */
class Individual extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'privileges' => true
    ];
    
    
    protected function _getFullName()
    {
        return $this->_properties['first_name'].' '.$this->_properties['last_name'];
    }
	
    
    protected function _getReverseName()
    {
        return $this->_properties['last_name'].', '.$this->_properties['first_name'];
    }
	
    
}
