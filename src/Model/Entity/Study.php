<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Study Entity.
 *
 * @property int $id
 * @property string $name
 * @property int $is_reading
 * @property int $is_demonstration
 * @property int $is_discourse
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Study extends Entity
{

    public function initialize(array $config)
    {
        $this->displayField('full_name');
    }
	
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
	
    protected function _getFullName()
    {
        return $this->_properties['id'].': '.$this->_properties['name'];
    }
	
}
