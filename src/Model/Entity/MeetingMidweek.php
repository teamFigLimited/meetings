<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MeetingMidweek Entity.
 *
 * @property int $id
 * @property int $meeting_id
 * @property \App\Model\Entity\Meeting $meeting
 * @property \Cake\I18n\Time $time_meeting
 * @property int $chairman_id
 * @property \App\Model\Entity\Chairman $chairman
 * @property int $opening_song_number
 * @property int $opening_prayer_id
 * @property \App\Model\Entity\OpeningPrayer $opening_prayer
 * @property string $treasures_material
 * @property string $treasures_talk_theme
 * @property string $treasures_talk_material
 * @property int $treasures_talk_individual_id
 * @property \App\Model\Entity\TreasuresTalkIndividual $treasures_talk_individual
 * @property string $treasures_gems_material
 * @property int $treasures_gems_individual_id
 * @property \App\Model\Entity\TreasuresGemsIndividual $treasures_gems_individual
 * @property string $treasures_reading_material
 * @property int $treasures_reading_individual_id
 * @property \App\Model\Entity\TreasuresReadingIndividual $treasures_reading_individual
 * @property string $ministry_presentation_material
 * @property int $ministry_presentation_individual_id
 * @property \App\Model\Entity\MinistryPresentationIndividual $ministry_presentation_individual
 * @property string $ministry_initial_material
 * @property int $ministry_initial_student_id
 * @property \App\Model\Entity\MinistryInitialStudent $ministry_initial_student
 * @property int $ministry_initial_assistant_id
 * @property \App\Model\Entity\MinistryInitialAssistant $ministry_initial_assistant
 * @property string $ministry_return_material
 * @property int $ministry_return_student_id
 * @property \App\Model\Entity\MinistryReturnStudent $ministry_return_student
 * @property int $ministry_return_assistant_id
 * @property \App\Model\Entity\MinistryReturnAssistant $ministry_return_assistant
 * @property string $ministry_study_material
 * @property int $ministry_study_student_id
 * @property \App\Model\Entity\MinistryStudyStudent $ministry_study_student
 * @property int $ministry_study_assistant_id
 * @property \App\Model\Entity\MinistryStudyAssistant $ministry_study_assistant
 * @property int $middle_song_number
 * @property string $living_item1_theme
 * @property string $living_item1_material
 * @property int $living_item1_minutes
 * @property int $living_item1_individual_id
 * @property \App\Model\Entity\LivingItem1Individual $living_item1_individual
 * @property string $living_item2_theme
 * @property string $living_item2_material
 * @property int $living_item2_minutes
 * @property int $living_item2_individual_id
 * @property \App\Model\Entity\LivingItem2Individual $living_item2_individual
 * @property string $living_cbs_material
 * @property int $living_cbs_conductor_id
 * @property \App\Model\Entity\LivingCbsConductor $living_cbs_conductor
 * @property int $living_cbs_reader_id
 * @property \App\Model\Entity\LivingCbsReader $living_cbs_reader
 * @property int $closing_song_number
 * @property int $closing_prayer_id
 * @property \App\Model\Entity\ClosingPrayer $closing_prayer
 */
class MeetingMidweek extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
