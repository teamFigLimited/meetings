<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MeetingOutgoingTalk Entity.
 *
 * @property int $id
 * @property int $meeting_id
 * @property \App\Model\Entity\Meeting $meeting
 * @property int $speaker_id
 * @property \App\Model\Entity\Speaker $speaker
 * @property int $congregation_id
 * @property \App\Model\Entity\Congregation $congregation
 * @property int $outline_id
 * @property \App\Model\Entity\Outline $outline
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class MeetingOutgoingTalk extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
