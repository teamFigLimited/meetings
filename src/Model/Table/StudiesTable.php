<?php
namespace App\Model\Table;

use App\Model\Entity\Study;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Studies Model
 *
 */
class StudiesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('studies');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Individuals', [
            'through' => 'IndividualsStudies'
        ]);
        
        $this->hasMany('MeetingMidweekReading', [
            'className' => 'MeetingMidweeks',
            'foreignKey' => 'treasures_reading_study_id'
        ]);
        $this->hasMany('MeetingMidweekItem1', [
            'className' => 'MeetingMidweeks',
            'foreignKey' => 'ministry_item1_study_id'
        ]);
        $this->hasMany('MeetingMidweekItem2', [
            'className' => 'MeetingMidweeks',
            'foreignKey' => 'ministry_item2_study_id'
        ]);
        $this->hasMany('MeetingMidweekItem3', [
            'className' => 'MeetingMidweeks',
            'foreignKey' => 'ministry_item3_study_id'
        ]);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('number', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('number');

        $validator
            ->allowEmpty('name');

        $validator
            ->add('is_reading', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('is_reading');

        $validator
            ->add('is_demonstration', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('is_demonstration');

        $validator
            ->add('is_discourse', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('is_discourse');

        return $validator;
    }
}
