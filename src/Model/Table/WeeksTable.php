<?php
namespace App\Model\Table;

use App\Model\Entity\Week;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Weeks Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Months
 * @property \Cake\ORM\Association\HasMany $Meetings
 * @property \Cake\ORM\Association\HasMany $WeekLivings
 * @property \Cake\ORM\Association\HasMany $WeekMidweeks
 * @property \Cake\ORM\Association\HasMany $WeekSongs
 * @property \Cake\ORM\Association\HasMany $WeekWeekends
 * @property \Cake\ORM\Association\HasMany $WeekWts
 */
class WeeksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('weeks');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Month', [
            'className' => 'Months',
            'foreignKey' => 'month_id'
        ]);        
        
        $this->hasMany('Meetings', [
            'foreignKey' => 'week_id'
        ]);
        $this->hasOne('WeekMidweek', [
            'className' => 'WeekMidweeks',
            'foreignKey' => 'week_id'
        ]);
        $this->hasOne('WeekWeekend', [
            'className' => 'WeekWeekends',
            'foreignKey' => 'week_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('date_monday', 'valid', ['rule' => 'date'])
            ->allowEmpty('date_monday');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['month_id'], 'Month'));
        return $rules;
    }
}
