<?php
namespace App\Model\Table;

use App\Model\Entity\WeekLiving;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WeekLivings Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Weeks
 */
class WeekLivingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('week_livings');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Weeks', [
            'foreignKey' => 'week_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('theme');

        $validator
            ->allowEmpty('material');

        $validator
            ->add('minutes', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('minutes');

        $validator
            ->add('sort', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('sort');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['week_id'], 'Weeks'));
        return $rules;
    }
}
