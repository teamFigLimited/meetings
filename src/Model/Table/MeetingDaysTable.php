<?php
namespace App\Model\Table;

use App\Model\Entity\MeetingDay;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MeetingDays Model
 *
 */
class MeetingDaysTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('meeting_days');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        $validator
            ->add('is_midweek', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('is_midweek');

        $validator
            ->add('is_weekend', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('is_weekend');

        return $validator;
    }
}
