<?php
namespace App\Model\Table;

use App\Model\Entity\MeetingMidweek;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MeetingMidweeks Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Meetings
 * @property \Cake\ORM\Association\BelongsTo $Chairmen
 * @property \Cake\ORM\Association\BelongsTo $OpeningPrayers
 * @property \Cake\ORM\Association\BelongsTo $TreasuresTalkIndividuals
 * @property \Cake\ORM\Association\BelongsTo $TreasuresGemsIndividuals
 * @property \Cake\ORM\Association\BelongsTo $TreasuresReadingIndividuals
 * @property \Cake\ORM\Association\BelongsTo $MinistryPresentationIndividuals
 * @property \Cake\ORM\Association\BelongsTo $MinistryInitialStudents
 * @property \Cake\ORM\Association\BelongsTo $MinistryInitialAssistants
 * @property \Cake\ORM\Association\BelongsTo $MinistryReturnStudents
 * @property \Cake\ORM\Association\BelongsTo $MinistryReturnAssistants
 * @property \Cake\ORM\Association\BelongsTo $MinistryStudyStudents
 * @property \Cake\ORM\Association\BelongsTo $MinistryStudyAssistants
 * @property \Cake\ORM\Association\BelongsTo $LivingItem1Individuals
 * @property \Cake\ORM\Association\BelongsTo $LivingItem2Individuals
 * @property \Cake\ORM\Association\BelongsTo $LivingCbsConductors
 * @property \Cake\ORM\Association\BelongsTo $LivingCbsReaders
 * @property \Cake\ORM\Association\BelongsTo $ClosingPrayers
 */
class MeetingMidweeksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('meeting_midweeks');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Week', [
            'className' => 'Weeks',
            'foreignKey' => 'week_id'
        ]);
        $this->belongsTo('Meeting', [
            'className' => 'Meetings',
            'foreignKey' => 'meeting_id'
        ]);
        $this->belongsTo('Chairman', [
            'className' => 'Individuals',
            'foreignKey' => 'chairman_id'
        ]);
        $this->belongsTo('OpeningPrayer', [
            'className' => 'Individuals',
            'foreignKey' => 'opening_prayer_id'
        ]);
        $this->belongsTo('TreasuresTalkIndividual', [
            'className' => 'Individuals',
            'foreignKey' => 'treasures_talk_individual_id'
        ]);
        $this->belongsTo('TreasuresGemsIndividual', [
            'className' => 'Individuals',
            'foreignKey' => 'treasures_gems_individual_id'
        ]);
        $this->belongsTo('TreasuresReadingIndividual', [
            'className' => 'Individuals',
            'foreignKey' => 'treasures_reading_individual_id'
        ]);
        $this->belongsTo('TreasuresReadingStudy', [
            'className' => 'Studies',
            'foreignKey' => 'treasures_reading_study_id'
        ]);
        $this->belongsTo('MinistryPresentationIndividual', [
            'className' => 'Individuals',
            'foreignKey' => 'ministry_presentation_individual_id'
        ]);
        $this->belongsTo('MinistryItem1Type', [
            'className' => 'MinistryTypes',
            'foreignKey' => 'ministry_item1_type_id'
        ]);
        $this->belongsTo('MinistryItem1Student', [
            'className' => 'Individuals',
            'foreignKey' => 'ministry_item1_student_id'
        ]);
        $this->belongsTo('MinistryItem1Study', [
            'className' => 'Studies',
            'foreignKey' => 'ministry_item1_study_id'
        ]);
        $this->belongsTo('MinistryItem1Assistant', [
            'className' => 'Individuals',
            'foreignKey' => 'ministry_item1_assistant_id'
        ]);
        $this->belongsTo('MinistryItem2Type', [
            'className' => 'MinistryTypes',
            'foreignKey' => 'ministry_item2_type_id'
        ]);
        $this->belongsTo('MinistryItem2Student', [
            'className' => 'Individuals',
            'foreignKey' => 'ministry_item2_student_id'
        ]);
        $this->belongsTo('MinistryItem2Study', [
            'className' => 'Studies',
            'foreignKey' => 'ministry_item2_study_id'
        ]);
        $this->belongsTo('MinistryItem2Assistant', [
            'className' => 'Individuals',
            'foreignKey' => 'ministry_item2_assistant_id'
        ]);
        $this->belongsTo('MinistryItem3Type', [
            'className' => 'MinistryTypes',
            'foreignKey' => 'ministry_item3_type_id'
        ]);
        $this->belongsTo('MinistryItem3Student', [
            'className' => 'Individuals',
            'foreignKey' => 'ministry_item3_student_id'
        ]);
        $this->belongsTo('MinistryItem3Study', [
            'className' => 'Studies',
            'foreignKey' => 'ministry_item3_study_id'
        ]);
        $this->belongsTo('MinistryItem3Assistant', [
            'className' => 'Individuals',
            'foreignKey' => 'ministry_item3_assistant_id'
        ]);
        $this->belongsTo('LivingItem1Individual', [
            'className' => 'Individuals',
            'foreignKey' => 'living_item1_individual_id'
        ]);
        $this->belongsTo('LivingItem2Individual', [
            'className' => 'Individuals',
            'foreignKey' => 'living_item2_individual_id'
        ]);
        $this->belongsTo('LivingCbsConductor', [
            'className' => 'Individuals',
            'foreignKey' => 'living_cbs_conductor_id'
        ]);
        $this->belongsTo('LivingCbsReader', [
            'className' => 'Individuals',
            'foreignKey' => 'living_cbs_reader_id'
        ]);
        $this->belongsTo('ClosingPrayer', [
            'className' => 'Individuals',
            'foreignKey' => 'closing_prayer_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('time_meeting', 'valid', ['rule' => 'datetime'])
            ->allowEmpty('time_meeting');

        $validator
            ->add('opening_song_number', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('opening_song_number');

        $validator
            ->allowEmpty('treasures_material');

        $validator
            ->allowEmpty('treasures_talk_theme');

        $validator
            ->allowEmpty('treasures_talk_material');

        $validator
            ->allowEmpty('treasures_gems_material');

        $validator
            ->allowEmpty('treasures_reading_material');

        $validator
            ->allowEmpty('ministry_presentation_material');

        $validator
            ->allowEmpty('ministry_initial_material');

        $validator
            ->allowEmpty('ministry_return_material');

        $validator
            ->allowEmpty('ministry_study_material');

        $validator
            ->add('middle_song_number', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('middle_song_number');

        $validator
            ->allowEmpty('living_item1_theme');

        $validator
            ->allowEmpty('living_item1_material');

        $validator
            ->add('living_item1_minutes', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('living_item1_minutes');

        $validator
            ->allowEmpty('living_item2_theme');

        $validator
            ->allowEmpty('living_item2_material');

        $validator
            ->add('living_item2_minutes', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('living_item2_minutes');

        $validator
            ->allowEmpty('living_cbs_material');

        $validator
            ->add('closing_song_number', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('closing_song_number');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['meeting_id'], 'Meeting'));
        $rules->add($rules->existsIn(['chairman_id'], 'Chairman'));
        $rules->add($rules->existsIn(['opening_prayer_id'], 'OpeningPrayer'));
        $rules->add($rules->existsIn(['treasures_talk_individual_id'], 'TreasuresTalkIndividual'));
        $rules->add($rules->existsIn(['treasures_gems_individual_id'], 'TreasuresGemsIndividual'));
        $rules->add($rules->existsIn(['treasures_reading_individual_id'], 'TreasuresReadingIndividual'));
        $rules->add($rules->existsIn(['ministry_presentation_individual_id'], 'MinistryPresentationIndividual'));
        $rules->add($rules->existsIn(['ministry_item1_student_id'], 'MinistryItem1Student'));
        $rules->add($rules->existsIn(['ministry_item1_assistant_id'], 'MinistryItem1Assistant'));
        $rules->add($rules->existsIn(['ministry_item2_student_id'], 'MinistryItem2Student'));
        $rules->add($rules->existsIn(['ministry_item2_assistant_id'], 'MinistryItem2Assistant'));
        $rules->add($rules->existsIn(['ministry_item3_student_id'], 'MinistryItem3Student'));
        $rules->add($rules->existsIn(['ministry_item3_assistant_id'], 'MinistryItem3Assistant'));
        $rules->add($rules->existsIn(['living_item1_individual_id'], 'LivingItem1Individual'));
        $rules->add($rules->existsIn(['living_item2_individual_id'], 'LivingItem2Individual'));
        $rules->add($rules->existsIn(['living_cbs_conductor_id'], 'LivingCbsConductor'));
        $rules->add($rules->existsIn(['living_cbs_reader_id'], 'LivingCbsReader'));
        $rules->add($rules->existsIn(['closing_prayer_id'], 'ClosingPrayer'));
        return $rules;
    }
    
    
}
