<?php
namespace App\Model\Table;

use App\Model\Entity\WeekSong;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WeekSongs Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Weeks
 * @property \Cake\ORM\Association\BelongsTo $Songs
 */
class WeekSongsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('week_songs');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Weeks', [
            'foreignKey' => 'week_id'
        ]);
        $this->belongsTo('Songs', [
            'foreignKey' => 'song_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('position', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('position');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['week_id'], 'Weeks'));
        $rules->add($rules->existsIn(['song_id'], 'Songs'));
        return $rules;
    }
}
