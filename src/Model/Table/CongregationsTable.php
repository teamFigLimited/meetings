<?php
namespace App\Model\Table;

use App\Model\Entity\Congregation;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Congregations Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Installations
 * @property \Cake\ORM\Association\HasMany $Individuals
 * @property \Cake\ORM\Association\HasMany $Installations
 */
class CongregationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('congregations');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Installation', [
            'className' => 'Installations',
            'foreignKey' => 'installation_id'
        ]);
        $this->hasMany('Individuals', [
            'foreignKey' => 'congregation_id',
            'sort' => ['Individuals.last_name' => 'ASC', 'Individuals.first_name' => 'ASC']
        ]);
        
        /*
        $this->hasMany('Installations', [
            'foreignKey' => 'congregation_id'
        ]);
         */
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['installation_id'], 'Installation'));
        return $rules;
    }
    
    
	public function isOwnedBy($congregationId, $installationId)
	{
		return $this->exists(['id' => $congregationId, 'installation_id' => $installationId]);
	}

    
}
