<?php
namespace App\Model\Table;

use App\Model\Entity\Installation;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Installations Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Congregations
 * @property \Cake\ORM\Association\HasMany $Meetings
 */
class InstallationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('installations');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('User', [
            'className' => 'Users',
            'foreignKey' => 'user_id'
        ]);
        
        $this->hasOne('Congregation', [
            'className' => 'Congregations',
            'foreignKey' => 'installation_id'
        ]);
        $this->hasOne('Coordinator', [
            'className' => 'Individuals',
            'foreignKey' => 'coordinator_id'
        ]);
        $this->hasOne('Secretary', [
            'className' => 'Individuals',
            'foreignKey' => 'secretary_id'
        ]);
        $this->hasOne('ServiceOverseer', [
            'className' => 'Individuals',
            'foreignKey' => 'service_overseer_id'
        ]);
        $this->hasOne('ClmOverseer', [
            'className' => 'Individuals',
            'foreignKey' => 'clm_overseer_id'
        ]);
        
        $this->hasMany('Meetings', [
            'foreignKey' => 'installation_id'
        ]);
        $this->hasOne('Congregations', [
            'foreignKey' => 'installation_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->notEmpty('congregation_name');

        $validator
            ->allowEmpty('circuit_overseer_name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'User'));
        $rules->add($rules->existsIn(['congregation_id'], 'Congregation'));
        return $rules;
    }
    
    
	public function isOwnedBy($installationId, $userId)
	{
		return $this->exists(['id' => $installationId, 'user_id' => $userId]);
	}

    
}
