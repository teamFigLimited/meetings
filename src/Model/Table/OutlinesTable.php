<?php
namespace App\Model\Table;

use App\Model\Entity\Outline;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Outlines Model
 *
 */
class OutlinesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('outlines');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        
        $this->belongsToMany('Individuals', [
            'foreignKey' => 'outline_id',
            'targetForeignKey' => 'individual_id',
            'joinTable' => 'individuals_outlines'
        ]);
        

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('number', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('number');

        $validator
            ->allowEmpty('name');

        $validator
            ->add('date_revision', 'valid', ['rule' => 'date'])
            ->allowEmpty('date_revision');

        return $validator;
    }
}
