<?php
namespace App\Model\Table;

use App\Model\Entity\MeetingWeekend;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MeetingWeekends Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Meetings
 * @property \Cake\ORM\Association\BelongsTo $Chairmen
 * @property \Cake\ORM\Association\BelongsTo $Speakers
 * @property \Cake\ORM\Association\BelongsTo $Congregations
 * @property \Cake\ORM\Association\BelongsTo $WtReaders
 */
class MeetingWeekendsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('meeting_weekends');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Meeting', [
            'className' => 'Meetings',
            'foreignKey' => 'meeting_id'
        ]);
        $this->belongsTo('Chairman', [
            'className' => 'Individuals',
            'foreignKey' => 'chairman_id'
        ]);
        $this->belongsTo('Outline', [
            'className' => 'Outlines',
            'foreignKey' => 'outline_id'
        ]);
        $this->belongsTo('Speaker', [
            'className' => 'Individuals',
            'foreignKey' => 'speaker_id'
        ]);
        $this->belongsTo('SpeakerCongregation', [
            'className' => 'Congregations',
            'foreignKey' => 'congregation_id'
        ]);
        $this->belongsTo('Reader', [
            'className' => 'Individuals',
            'foreignKey' => 'reader_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('time_meeting', 'valid', ['rule' => 'datetime'])
            ->allowEmpty('time_meeting');

        $validator
            ->allowEmpty('speaker_name');

        $validator
            ->allowEmpty('congregation_name');

        $validator
            ->add('middle_song_number', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('middle_song_number');

        $validator
            ->add('closing_song_number', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('closing_song_number');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['meeting_id'], 'Meeting'));
        $rules->add($rules->existsIn(['chairman_id'], 'Chairman'));
        $rules->add($rules->existsIn(['speaker_id'], 'Speaker'));
        $rules->add($rules->existsIn(['congregation_id'], 'SpeakerCongregation'));
        $rules->add($rules->existsIn(['wt_reader_id'], 'Reader'));
        return $rules;
    }
}
