<?php
namespace App\Model\Table;

use App\Model\Entity\WeekWeekend;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WeekWeekends Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Weeks
 */
class WeekWeekendsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('week_weekends');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Week', [
            'className' => 'Weeks',
            'foreignKey' => 'week_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('middle_song_number', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('middle_song_number');

        $validator
            ->allowEmpty('wt_theme');

        $validator
            ->add('closing_song_number', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('closing_song_number');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['week_id'], 'Week'));
        return $rules;
    }
}
