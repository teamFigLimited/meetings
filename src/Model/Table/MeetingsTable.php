<?php
namespace App\Model\Table;

use App\Model\Entity\Meeting;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Meetings Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Installations
 * @property \Cake\ORM\Association\BelongsTo $Weeks
 * @property \Cake\ORM\Association\HasMany $MeetingMidweeks
 * @property \Cake\ORM\Association\HasMany $MeetingPrayers
 * @property \Cake\ORM\Association\HasMany $MeetingPts
 * @property \Cake\ORM\Association\HasMany $MeetingWts
 */
class MeetingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('meetings');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Installation', [
            'className' => 'Installations',
            'foreignKey' => 'installation_id'
        ]);
        $this->belongsTo('Week', [
            'className' => 'Weeks',
            'foreignKey' => 'week_id'
        ]);
        $this->belongsTo('MeetingWeekType', [
            'className' => 'MeetingWeekTypes',
            'foreignKey' => 'meeting_week_type_id'
        ]);
        
        $this->hasMany('MeetingOutgoingTalks', [
            'foreignKey' => 'meeting_id'
        ]);
        
        
        $this->hasOne('MeetingMidweek', [
            'className' => 'MeetingMidweeks',
            'foreignKey' => 'meeting_id'
        ]);
        $this->hasOne('MeetingWeekend', [
            'className' => 'MeetingWeekends',
            'foreignKey' => 'meeting_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['installation_id'], 'Installation'));
        $rules->add($rules->existsIn(['week_id'], 'Week'));
        return $rules;
    }
    
    
    public function updateMeetings($weeks = null, $installationId = null) {
        
        $meetings = [];
        foreach($weeks as $week) {
            $meetings[] = [
                'installation_id' => $installationId, 
                'week_id' => $week->id,
                'meeting_week_type_id' => 1
            ];
        }
        
        $entities = $this->newEntities($meetings);
        
        foreach($entities as $entity) {
            if($this->save($entity)) {
                //  its OK!
            } else {
                echo $entity->week_id." not saved";
                
                $x = $entity->errors();
                if($x) {
                    debug($entity);
                    debug($x);
                    if ($entity->errors()) {
                        return false;
                    }
                }

            }
        }
        return true;

    }
    
    
    public function updateMeetingMidweeksByMeeting($meetings = null)
    {
        
        $meetingMidweeks = [];
        foreach($meetings as $meeting) {
            $meetingMidweeks[] = [
                'week_id' => $meeting->week->id,
                'meeting_id' => $meeting->id, 
                'is_week1' => $meeting->week->week_midweek->is_week1,
                'opening_song_number' => $meeting->week->week_midweek->opening_song_number,
                'treasures_talk_material' => $meeting->week->week_midweek->treasures_talk_material,
                'treasures_gems_material' => $meeting->week->week_midweek->treasures_gems_material,
                'treasures_reading_material' => $meeting->week->week_midweek->treasures_reading_material,
                'ministry_presentation_material' => $meeting->week->week_midweek->ministry_presentation_material,
                'ministry_item1_type_id'  => $meeting->week->week_midweek->ministry_item1_type_id,
                'ministry_item1_material' => $meeting->week->week_midweek->ministry_item1_material,
                'ministry_item2_type_id'  => $meeting->week->week_midweek->ministry_item2_type_id,
                'ministry_item2_material' => $meeting->week->week_midweek->ministry_item2_material,
                'ministry_item3_type_id'  => $meeting->week->week_midweek->ministry_item3_type_id,
                'ministry_item3_material' => $meeting->week->week_midweek->ministry_item3_material,
                'middle_song_number' => $meeting->week->week_midweek->middle_song_number,
                'living_item1_theme' => $meeting->week->week_midweek->living_item1_theme,
                'living_item1_material' => $meeting->week->week_midweek->living_item1_material,
                'living_item1_minutes' => $meeting->week->week_midweek->living_item1_minutes,
                'living_item2_theme' => $meeting->week->week_midweek->living_item2_theme,
                'living_item2_material' => $meeting->week->week_midweek->living_item2_material,
                'living_item2_minutes' => $meeting->week->week_midweek->living_item2_minutes,
                'living_cbs_material' => $meeting->week->week_midweek->living_cbs_material,
                'closing_song_number' => $meeting->week->week_midweek->closing_song_number
            ];
        }
        
        $entities = $this->MeetingMidweek->newEntities($meetingMidweeks);
        
        foreach($entities as $entity) {
            if($this->MeetingMidweek->save($entity, ['checkRules' => false])) {
                //  its OK!
            } else {
                
                //  debugging
                echo $entity->id." not saved";
                
                $x = $entity->errors();
                if($x) {
                    debug($entity);
                    debug($x);
                    if ($entity->errors()) {
                        return false;
                    }
                }

            }
        }
        return true;        
        
    }
    
    
    public function updateMeetingWeekendsByMeeting($meetings = null)
    {
        
        $meetingWeekends = [];
        foreach($meetings as $meeting) {
            
            //  find meeting_id based on week_id
            
            $meetingWeekends[] = [
                'meeting_id' => $meeting->id, 
                'wt_theme' => $meeting->week->week_weekend->wt_theme,
                'middle_song_number' => $meeting->week->week_weekend->middle_song_number,
                'closing_song_number' => $meeting->week->week_weekend->closing_song_number
            ];
        }
        
        $entities = $this->MeetingWeekend->newEntities($meetingWeekends);
        
        foreach($entities as $entity) {
            if($this->MeetingWeekend->save($entity, ['checkRules' => false])) {
                //  its OK!
            } else {
                
                //  debugging
                echo $entity->id." not saved";
                
                $x = $entity->errors();
                if($x) {
                    debug($entity);
                    debug($x);
                    if ($entity->errors()) {
                        return false;
                    }
                }

            }
        }
        return true;        
        
    }
    
    
}
