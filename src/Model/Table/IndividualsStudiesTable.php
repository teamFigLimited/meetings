<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class IndividualsStudiesTable extends Table
{
    public function initialize(array $config)
    {
        $this->belongsTo('Individuals');
        $this->belongsTo('Studies');
    }
}