<?php
namespace App\Model\Table;

use App\Model\Entity\MeetingOutgoingTalk;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MeetingOutgoingTalks Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Meetings
 * @property \Cake\ORM\Association\BelongsTo $Speakers
 * @property \Cake\ORM\Association\BelongsTo $Congregations
 * @property \Cake\ORM\Association\BelongsTo $Outlines
 */
class MeetingOutgoingTalksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('meeting_outgoing_talks');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Meeting', [
            'className' => 'Meetings',
            'foreignKey' => 'meeting_id'
        ]);
        $this->belongsTo('Speaker', [
            'className' => 'Individuals',
            'foreignKey' => 'speaker_id'
        ]);
        $this->belongsTo('Congregation', [
            'className' => 'Congregations',
            'foreignKey' => 'congregation_id'
        ]);
        $this->belongsTo('Outline', [
            'className' => 'Outlines',
            'foreignKey' => 'outline_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['meeting_id'], 'Meeting'));
        $rules->add($rules->existsIn(['speaker_id'], 'Speaker'));
        $rules->add($rules->existsIn(['congregation_id'], 'Congregation'));
        $rules->add($rules->existsIn(['outline_id'], 'Outline'));
        return $rules;
    }
}
