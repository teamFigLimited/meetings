<?php
namespace App\Model\Table;

use App\Model\Entity\Individual;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Individuals Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Congregations
 * @property \Cake\ORM\Association\BelongsTo $Genders
 * @property \Cake\ORM\Association\BelongsTo $Appointments
 * @property \Cake\ORM\Association\BelongsTo $FamilyHeads
 * @property \Cake\ORM\Association\HasMany $MeetingLivings
 * @property \Cake\ORM\Association\HasMany $MeetingPrayers
 * @property \Cake\ORM\Association\BelongsToMany $Privileges
 */
class IndividualsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('individuals');
        $this->displayField('full_name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Congregation', [
            'className' => 'Congregations',
            'foreignKey' => 'congregation_id'
        ]);
        $this->belongsTo('Gender', [
            'className' => 'Genders',
            'foreignKey' => 'gender_id'
        ]);
        $this->belongsTo('Appointment', [
            'className' => 'Appointments',
            'foreignKey' => 'appointment_id'
        ]);
        $this->belongsTo('FamilyHead', [
            'className' => 'Individuals',
            'foreignKey' => 'family_head_id'
        ]);
        
        
        $this->belongsToMany('Outlines', [
            'foreignKey' => 'individual_id',
            'targetForeignKey' => 'outline_id',
            'joinTable' => 'individuals_outlines'
        ]);
        $this->belongsToMany('Privileges', [
            'foreignKey' => 'individual_id',
            'targetForeignKey' => 'privilege_id',
            'joinTable' => 'individuals_privileges'
        ]);
        $this->belongsToMany('Studies', [
            'through' => 'IndividualsStudies'
        ]);
        
        
        $this->hasMany('TreasuresReading', [
            'className' => 'MeetingMidweeks',
            'foreignKey' => 'treasures_reading_individual_id'
        ]);
        
        $this->hasOne('LastTreasuresReading', [
            'className' => 'MeetingMidweeks',
            'foreignKey' => 'treasures_reading_individual_id',
        ]);

        $this->hasMany('Children', [
            'className' => 'Individuals',
            'foreignKey' => 'family_head_id'
        ]);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('family_head_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['congregation_id'], 'Congregation'));
        $rules->add($rules->existsIn(['gender_id'], 'Gender'));
        $rules->add($rules->existsIn(['appointment_id'], 'Appointment'));
        return $rules;
    }
    
}
