<?php
namespace App\Model\Table;

use App\Model\Entity\MeetingEvent;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MeetingEvents Model
 *
 */
class MeetingEventsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('meeting_events');
        $this->displayField('name');
        $this->primaryKey('id');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        $validator
            ->add('is_co', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('is_co');

        $validator
            ->add('is_assembly', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('is_assembly');

        $validator
            ->add('is_convention', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('is_convention');

        return $validator;
    }
}
