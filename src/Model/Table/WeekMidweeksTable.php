<?php
namespace App\Model\Table;

use App\Model\Entity\WeekMidweek;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;


/**
 * WeekMidweeks Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Weeks
 */
class WeekMidweeksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('week_midweeks');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Week', [
            'className' => 'Weeks',
            'foreignKey' => 'week_id'
        ]);
        $this->belongsTo('MinistryItem1Type', [
            'className' => 'MinistryTypes',
            'foreignKey' => 'ministry_item1_type_id'
        ]);
        $this->belongsTo('MinistryItem2Type', [
            'className' => 'MinistryTypes',
            'foreignKey' => 'ministry_item2_type_id'
        ]);
        $this->belongsTo('MinistryItem3Type', [
            'className' => 'MinistryTypes',
            'foreignKey' => 'ministry_item3_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('treasures_talk_theme');

        $validator
            ->allowEmpty('treasures_talk_material');

        $validator
            ->allowEmpty('treasures_gems_material');

        $validator
            ->allowEmpty('treasures_reading_material');

        $validator
            ->allowEmpty('ministry_presentation_material');

        $validator
            ->allowEmpty('ministry_initial_material');

        $validator
            ->allowEmpty('ministry_return_material');

        $validator
            ->allowEmpty('ministry_study_material');

        $validator
            ->allowEmpty('cbs_material');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['week_id'], 'Week'));
        return $rules;
    }


  // In a table or behavior class
  public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
  {
   if (isset($data['treasures_talk_theme'])) {
     $data['treasures_talk_theme'] = iconv('UTF-8', 'ASCII//TRANSLIT', $data['treasures_talk_theme']);
   }
   if (isset($data['treasures_talk_material'])) {
     $data['treasures_talk_material'] = iconv('UTF-8', 'ASCII//TRANSLIT', $data['treasures_talk_material']);
   }
   if (isset($data['treasures_gems_material'])) {
     $data['treasures_gems_material'] = iconv('UTF-8', 'ASCII//TRANSLIT', $data['treasures_gems_material']);
   }
   if (isset($data['living_item1_material'])) {
     $data['living_item1_material'] = iconv('UTF-8', 'ASCII//TRANSLIT', $data['living_item1_material']);
   }
   if (isset($data['living_item2_material'])) {
     $data['living_item2_material'] = iconv('UTF-8', 'ASCII//TRANSLIT', $data['living_item2_material']);
   }
 }
}
