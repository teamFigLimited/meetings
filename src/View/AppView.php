<?php
namespace App\View;

use WyriHaximus\TwigView\View\TwigView;

class AppView extends TwigView
{

    public function initialize()
    {
        parent::initialize();
        $this->loadHelper('Paginator');
        //$this->loadHelper('CakeDC/Users.User');
    }

}
